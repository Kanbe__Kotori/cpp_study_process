#include <vector>
#include <list>
#include <iostream>
#include "stack.h"

using namespace simulation;

int main()
{
	stack<int, std::list<int>> st;
	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);

	while (!st.empty())
	{
		std::cout << st.top() << " ";
		st.pop();
	}
	std::cout << std::endl;
	return 0;
}