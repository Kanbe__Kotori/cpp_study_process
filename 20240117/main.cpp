//#include <iostream>
//#include <string>
//using namespace std;
//
//int main()
//{
//	string s("abcd");
//	auto p1 = &s[s.size()] - 1;
//	auto p2 = &s[s.size()];
//	auto p3 = &s[s.size()] + 1;
//
//	return 0;
//}

//#include "string.h"

//#include <iostream>
//#include <string>
//using namespace std;
//
//int main()
//{
//	string str("Hello World!");
//	string::iterator bit = str.begin();
//	string::iterator eit = str.end();
//
//	//如果觉得迭代器的类型太长 可以用auto让编译器自己推导
//	auto prevEit = str.end() - 1;
//
//	//迭代器的使用很像指针 但迭代器不一定是指针
//	cout << *bit << endl;
//	cout << *prevEit << endl;
//
//	return 0;
//}
//#include <iostream>
//#include <string>
//using std::cin;
//using std::endl;
//using std::cout;
//using std::string;
//
//int main()
//{
//	string s("Hello");
//	cin >> s;
//	cout << s << endl;
//	return 0;
//}
