//LeetCode541.翻转字符串Ⅱ
// 
//解法一：
//class Solution {
//public:
//    string reverseStr(string s, int k) {
//        int len = s.size();
//        int indexBegin = 0;
//        int indexEnd = k - 1;
//        bool reverseFlag = true;
//        while (indexBegin < len)
//        {
//            if (indexEnd >= len)
//            {
//                indexEnd = len - 1;
//            }
//
//            if (reverseFlag)
//            {
//                //迭代器的使用是左闭右开的 所以右边的下标要加1才能达到想要的效果
//                reverse(s.begin() + indexBegin, s.begin() + indexEnd + 1);
//                reverseFlag = false;
//            }
//            else
//            {
//                reverseFlag = true;
//            }
//
//            indexBegin += k;
//            indexEnd += k;
//        }
//
//        return s;
//    }
//};

//解法二：
//class Solution {
//public:
//    string reverseStr(string s, int k) {
//        int len = s.size();
//
//        for (int i = 0; i < len; i += 2 * k)
//        {
//            reverse(s.begin() + i, s.begin() + min(i + k, len));
//        }
//
//        return s;
//    }
//};