#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)


//#include <stdio.h>

#include <iostream>
#include <cmath>
using namespace std;

//这个类并不完美 因为很多函数都缺少const修饰
class Date
{
public:
	explicit Date(int year = 0, int month = 1, int day = 1) // 初始化列表 用于const 引用 和没有默认构造函数（不需要传任何参数）的类
		:_year(year)
		,_month(month)
		,_day(day)
	{}
	//{
	//	if (year >= 0 && (month >= 1 && month <= 12) && (day >= 1 && day <= getMonthDay(year, month)))
	//	{
	//		_year = year;
	//		_month = month;
	//		_day = day;
	//	}
	//	else
	//	{
	//		cout << "输入日期不合法！" << endl;
	//	}
	//}

	void getDate()
	{
		cout << "date is " << _year << "/" << _month << "/" << _day << endl;
	}

	int getMonthDay(int year, int month) const //这个const是修饰this的
	{
		static int monthDay[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };

		if (isLeapYear(year) && _month == 2)
		{
			return 29;
		}

		return monthDay[month];

	}

	bool isLeapYear(int year) const
	{
		if (year % 400 == 0 || ((year % 4 == 0) && (year % 100 != 0)))
		{
			return true;
		}

		return false;
	}

	bool operator>(const Date& date)
	{
		if (_year > date._year)
		{
			return true;
		}
		else if (_year == date._year && _month > date._month)
		{
			return true;
		}
		else if (_year == date._year && _month == date._month && _day > date._day)
		{
			return true;
		}

		return false;
	}

	bool operator==(const Date& date)
	{
		return _year == date._year
			&& _month == date._month
			&& _day == date._day;
	}

	bool operator>=(const Date& date)
	{
		return *this > date || *this == date;
	}

	bool operator<(const Date& date)
	{
		return !(*this >= date);
	}

	bool operator<=(const Date& date)
	{
		return !(*this > date);
	}

	bool operator!=(const Date& date)
	{
		return !(*this == date);
	}

	Date& operator+=(int days)
	{
		if (days < 0)
		{
			*this -= abs(days);
			return *this;
		}

		_day += days;
		while (_day > getMonthDay(_year, _month))
		{
			_day -= getMonthDay(_year, _month);
			++_month; 

			if (_month == 13)
			{
				_month = 1;
				++_year;
			}
		}

		return *this;
	}

	Date& operator=(const Date& date)
	{
		if (this != &date)
		{
			_year = date._year;
			_month = date._month;
			_day = date._day;

			return *this;
		}
	}

	Date operator+(int days)
	{
		Date result = *this;
		result += days;

		return result;
	}

	//后置加加
	//后置加加需要有一个占位符
	Date operator++(int)
	{
		Date result(*this);
		*this += 1;

		return result;
	}

	Date& operator++()
	{
		*this += 1;
		return *this;
	}

	Date operator--(int)
	{
		//Date result(*this);
		//_day -= 1;
		//if (_day == 0)
		//{
		//	if (_month == 1)
		//	{
		//		--_year;
		//		_month = 12;
		//		_day = 31;
		//	}

		//	--_month;
		//	_day = getMonthDay(_year, _month);
		//}

		//return result;

		Date result(*this);
		*this -= 1;

		return *this;
	}

	Date& operator--()
	{
		*this -= 1;
		return *this;
	}

	Date& operator-=(int days)
	{

		if (days < 0)
		{
			*this += abs(days);
			return *this;
		}

		_day -= days;

		while (_day <= 0)
		{
			--_month;
			if (_month == 0)
			{
				--_year;
				_month = 12;
			}

			_day += getMonthDay(_year, _month);
		}

		return *this;
	}

	Date operator-(int days)
	{
		Date result(*this);

		result -= days;

		return result;
	}

	int operator-(const Date& date) const
	{
		int flag = 1;
		Date max(*this);
		Date min(date);

		if (max < min)
		{
			max = date;
			min = *this;
			flag = -1;
		}

		int count = 0;
		while (min < max)
		{
			++count;
			++min;
		}

		return count * flag;
	}

	Date* operator&()
	{
		return this;
	}

	const Date* operator&() const
	{
		return this;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	//Date d1(2024, 1, 7);
	//Date d2(2024, 4, 22);
	////int gap = d2 - d1;
	////cout << gap << endl;
	//d2++;

	//d1 = d2;
	//d1.getDate();

	//Date d1(2024, 1, 8);
	//d1.getDate();
	//Date d2 = d1 - 10;
	//d1 -= 7;
	//d1.getDate();
	//d2.getDate();

	//C语言可以通过指针强行对const修饰的变量进行修改
	//但是C++不行

	//const int a = 10;
	//int* pa = &a;
	//(++*pa);
	//printf("%d\n", a);

	Date d1(2024, 1, 9);
	Date d2(2024, 4, 22);

	//C++只允许单参数的隐式类型转换

	//C++11允许的多参数隐式类型转换 可以通过在构造函数前加explicit关键词来规避这种写法 
	//Date d3 = { 2024, 6, 23 };


	cout << (d2 - d1) << endl;
	cout << (d1 - d2) << endl;

	return 0;
}