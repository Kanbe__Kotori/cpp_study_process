﻿//#include <iostream>
//#include <chrono>
//#include <span>
//#include <string>
//#include <algorithm>
//#include <string_view>
//#include <vector>
//#include <list>
//#include <thread>
//#include <mutex>
//#include <ranges>
//#include <unordered_map>
//using namespace std;

//#include <iostream>
//#include <string>
//using namespace std;
//
//int main() {
//	string str1 = "D:\code\test\main.cc";
//	string str2 = "D:\\code\\test\\main.cc";
//	string str3 = R"nomore15(D:\code\test\main.cc)nomore15";
//
//	cout << str1 << endl;
//	cout << str2 << endl;
//	cout << str3 << endl;
//
//	return 0;
//}

//#include<iostream>
//#include<string>
//using namespace std;

//#include<iostream>
//#include<string>
//using namespace std;
//int main()
//{
//    string str1 = "\
//    <html>\
//        <head>\
//            <title>\
//                C++学习\
//            </title>\
//        </head>\
//            <body>\
//                <p>\
//                    原始字面量\
//                </p>\
//            </body>\
//    </html>";
//
//    string str2 = R"(
//    <html>
//        <head>
//            <title>
//                C++学习
//            </title>
//        </head>
//            <body>
//                <p>
//                    原始字面量
//                </p>
//            </body>
//    </html>)";
//
//    cout << str1 << "\n" << endl;
//    cout << str2 << endl;
//
//
//    return 0;
//}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//#include <iostream>
//#include <utility>
//using namespace std;
//
//class Container
//{
//public:
//	Container()
//		: data_(new int[10])
//	{
//		cout << "Container()" << endl;
//	}
//
//	~Container()
//	{
//		cout << "~Container()" << endl;
//		delete[] data_;
//	}
//
//	//Container(const Container& container)
//	//	: data_(new int[10])
//	//{
//	//	cout << "deep copy" << endl;
//	//}
//
//	//Container(Container&& container) noexcept
//	//	: data_(nullptr)
//	//{
//	//	std::swap(container.data_, data_);
//	//	cout << "move copy" << endl;
//	//}
//
//	//Container& operator=(const Container& container)
//	//{
//	//	if (this == &container) return *this;
//
//	//	delete[] data_;
//	//	data_ = new int[10];
//
//	//	cout << "Container& operator=(const Container& container)" << endl;
//
//	//	return *this;
//	//}
//
//	//Container& operator=(Container&& container) noexcept
//	//{
//	//	if (this == &container) return *this;
//
//	//	data_ = nullptr;
//
//	//	std::swap(data_, container.data_);
//
//	//	cout << "Container& operator(Container&& container)" << endl;
//
//	//	return *this;
//	//}
//
//	int& operator[](int index)
//	{
//		return data_[index];
//	}
//
//	void show()
//	{
//		cout << data_ << endl;
//	}
//
//private:
//	int* data_;
//};
//
//Container getObject()
//{
//	Container c;
//	//c[0] = 10;
//	//c.show();
//
//	return c;
//}
//
//template<class T>
//void func(const T&& param)
//{
//	cout << "func" << endl;
//}

//int main()
//{
//	//Container cc = getObject();
//	//c.show();
//	//Container cc = std::move(c);
//
//	//int x = 10;
//	////func(x);
//	//func(10); // 纯右值
//	//func(int()); // 将亡值
//	//func(std::move(x)); // 将亡值
//
//	//const int&& a = 5;
//	//int&& b = 10;
//	////++a;
//	////++b;
//
//	////const int&& c = std::move(a);
//	//const int& c = a;
//	//int& d = b;
//	//++d;
//	//cout << d << endl;
//
//	//auto&& abc = 10;
//	//auto&& bbb = abc;
//	//auto& gdj = bbb;
//
//	//cout << typeid(abc).name() << endl;
//	//cout << typeid(bbb).name() << endl;
//	//cout << typeid(gdj).name() << endl;
//
//
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//template<class T>
//void func(T& t)
//{
//	cout << "lvalue" << endl;
//}
//
//template<class T>
//void func(T&& t)
//{
//	cout << "rvalue" << endl;
//}
//
//template<class T>
//void forward(T&& t)
//{
//	func(t);
//	func(std::forward<T>(t));
//	func(move(t));
//}
//
//int main()
//{
//	int num = 10;
//	forward(num);
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//template<typename T>
//void printValue(T& t)
//{
//    cout << "l-value: " << t << endl;
//}
//
//template<typename T>
//void printValue(T&& t)
//{
//    cout << "r-value: " << t << endl;
//}
//
//template<typename T>
//void testForward(T&& v)
//{
//    printValue(v);
//    printValue(move(v));
//    printValue(forward<T>(v));
//    cout << endl;
//}
//
//int main()
//{
//    //testForward(520);
//    int num = 1314;
//    //testForward(num);
//    testForward(forward<int>(num));
//    testForward(forward<int&>(num));
//    testForward(forward<int&&>(num));
//
//    return 0;
//}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if 0

#include <iostream>
#include <string>
#include <vector>
#include <array>
using namespace std;

//const struct A
//{
//	int a;
//};

//constexpr struct B
//{
//	int b;
//};

//int main()
//{
//	const int num = 20;
//	int array[num];
//	return 0;
//}

//int main()
//{
//	A a{1};
//	a.a = 10;
//}

//const int size_c = 1024;
//constexpr int size_cpr = 2048;
//
//int main()
//{
//	int array1[size_c] = { 1,2,3 };
//	int array2[size_cpr] = { 3,4,5 };
//	return 0;
//}

// class Test
//struct Test1
//{
//	int i;
//	double d;
//};
//
//struct Test2
//{
//	string str;
//	vector<int> v;
//};
//
//template<class T>
//struct Data
//{
//	T data;
//};
//
//template<class T, class Alloc = std::allocator<T>>
//struct Test3
//{
//	T data;
//	Data<T> test;
//	Alloc a;
//};
//
//int main()
//{
//	constexpr Test1 t1{ 1,1.2 }; // 必须要在定义时就初始化
//	cout << t1.i << " " << t1.d << endl;
//	//t.1 = 2; // error constexpr定义的对象的成员函数也是常量，不能修改
//
//	//constexpr Test2 t2{ "12345", {1,2,3} }; // 如果类中含有自定义类型，则不能用constexpr来
//	const Test2 t2{"12345", {1,2,3}};
//
//	constexpr Test3<int> t3{ 1, 1 };
//
//	//constexpr vector<int> v;
//	constexpr array<int, 10> array{ 1,2,3 };
//	return 0;
//}

//template<class T>
//struct Test1
//{
//	T t;
//	int i;
//	array<double, 3> arr;
//};
//
//struct Test2
//{
//	string str;
//	vector<int> v;
//};

//int main()
//{
//	constexpr Test1<char> t1{ 'a', 10, {1,2,3} }; // 必须要在定义时就赋值
//	cout << t1.t << " " << t1.i << endl;
//	int arr[t1.i] = {}; // 证实t1.i具有常量属性
//	//t1.arr[0] = 10; //error constexpr修饰的对象的成员也是常量，不能进行修改
//	//constexpr array<double, 3> a{ 5, 6, 7 };
//	//t1.arr = a;
//
//	//error 因为constexpr修饰的常量是在编译时确定的，而string、vector等容器的数据是存在堆上的，而程序对堆空间的管理是在运行时才开始的，所以会出现编译错误
//	//constexpr Test2 t2{ "123", {1,2,3} };
//
//	const Test2 t2{ "123", {1,2,3} }; // 这里的const不是声明t2为编译时常量（真常量），而是声明为运行时常量（只读属性）
//	//t2.str = "000"; //error 只读对象的成员也是只读的
//	return 0;
//}

//constexpr void test()
//{
//
//}

//#include <iostream>
//using namespace std;
//
//constexpr int test(int i)
//{
//	return i > 0 ? i : 0;
//}
//
//int main()
//{
//	int i;
//	cin >> i;
//	cout << test(i);
//	return 0;
//}

//struct Test
//{
//public:
//	constexpr Test() : i(0), d(0.0), c('0')
//	{}
//	~Test() = default;
//
//	int i;
//	double d;
//	char c;
//};
//
//int main()
//{
//	// 错误用法，这样使用构建出的对象不是常量
//	Test t1;
//	cout << t1.i << " " << t1.d << " " << t1.c << endl;
//	t1.i = 100;
//	cout << t1.i << " " << t1.d << " " << t1.c << endl;
//	Test t2;
//	t1 = t2;
//	cout << t1.i << " " << t1.d << " " << t1.c << endl;
//
//	// 正确用法
//	constexpr Test t3;
//	//t3.i = 100; // error
//
//	return 0;
//}

struct Person
{
	const char* name_;
	uint16_t age_;
};

template<class T>
constexpr T dispatch(const T& t)
{
	return t;
}

int main()
{
	// 因为12345是字面量即常量，所以此时dispatch是常量表达式函数
	constexpr int ret = dispatch(12345);

	Person p1{ "zhangsan", 15 };
	// 因为p1是变量，不是常量表达式，所以dispacth函数的constexpr失效
	Person p2 = dispatch(p1);
	cout << p2.name_ << " " << p2.age_ << endl;

	constexpr Person p3{ "zhangsan", 15 };
	// 因为p3为常量，所以此时dispacth函数的constexpr有效
	constexpr Person p4 = dispatch(p3);
	cout << p4.name_ << " " << p4.age_ << endl;

	return 0;
}

#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if 0
#include <iostream>
using namespace std;

//enum {test = 8};
//enum {_ALIGN = 8};
//
//int main()
//{
//	//cout << sizeof(test) << endl;
//	//cout << typeid(test).name() << endl;
//
//	//cout << ~((size_t)_ALIGN - 1) << endl;
//	//cout << ~(_ALIGN - 1) << endl;
//
//	//cout << 0B0111 << endl;
//
//	return 0;
//}

//union obj
//{
//	obj* next_;
//	char data[128];
//};
//
//int main()
//{
//	cout << sizeof(obj) << endl;
//	return 0;
//}

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if 0
#include <iostream>
using namespace std;

void test(int& i)
{
	i = 100;
}

int main()
{
	volatile const int i = 20;
	test(const_cast<int&>(i));
	cout << i << endl;
	return 0;
}

#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////'

//  /* __n must be > 0      */
//static void* allocate(size_t __n)
//{
//    void* __ret = 0;
//
//    if (__n > (size_t)_MAX_BYTES) {
//        __ret = malloc_alloc::allocate(__n);
//    }
//    else {
//        _Obj* __STL_VOLATILE* __my_free_list
//            = _S_free_list + _S_freelist_index(__n);
//        // Acquire the lock here with a constructor call.
//        // This ensures that it is released in exit or during stack
//        // unwinding.
//#     ifndef _NOTHREADS
//      /*REFERENCED*/
//        _Lock __lock_instance;
//#     endif
//        _Obj* __RESTRICT __result = *__my_free_list;
//        if (__result == 0)
//            __ret = _S_refill(_S_round_up(__n));
//        else {
//            *__my_free_list = __result->_M_free_list_link;
//            __ret = __result;
//        }
//    }
//
//    return __ret;
//};

//class _Lock {
//public:
//	_Lock() { __NODE_ALLOCATOR_LOCK; }
//	~_Lock() { __NODE_ALLOCATOR_UNLOCK; }
//};

//for (__i = 1; ; __i++) { // 死循环
//	// 更新__current_obj和__next_obj
//	__current_obj = __next_obj;
//	__next_obj = (_Obj*)((char*)__next_obj + __n);
//	if (__nobjs - 1 == __i) { // 处理最后一个chunk块
//		__current_obj->_M_free_list_link = 0;
//		break;
//	}
//	else {
//		// 与普通链表一样，连接节点
//		__current_obj->_M_free_list_link = __next_obj;
//	}
//}

//if (__bytes_left >= __total_bytes) {
//	__result = _S_start_free;
//	_S_start_free += __total_bytes;
//	return(__result);
//}

//if (__bytes_left >= __total_bytes) { // 之前剩下的内存块的大小大于本次总共需要申请的内存大小
//	__result = _S_start_free;
//	_S_start_free += __total_bytes;
//	return(__result);
//}
//else if (__bytes_left >= __size) { // 之前剩下的内存块不满足上述情况，但是至少能分配一个__size大小的chunk块
//	__nobjs = (int)(__bytes_left / __size);
//	__total_bytes = __size * __nobjs;
//	__result = _S_start_free;
//	_S_start_free += __total_bytes;
//	return(__result);
//}
//else{}

//if (__bytes_left >= __total_bytes) { // 之前剩下的内存块的大小大于本次总共需要申请的内存大小
//    __result = _S_start_free;
//    _S_start_free += __total_bytes;
//    return(__result);
//}
//else if (__bytes_left >= __size) { // 之前剩下的内存块不满足上述情况，但是至少能分配一个__size大小的chunk块
//    __nobjs = (int)(__bytes_left / __size);
//    __total_bytes = __size * __nobjs;
//    __result = _S_start_free;
//    _S_start_free += __total_bytes;
//    return(__result);
//}
//else { // 之前剩下的内存块一个__size大小的chunk块都分配不出来
//    size_t __bytes_to_get =
//        2 * __total_bytes + _S_round_up(_S_heap_size >> 4);
//     尽量利用剩下的残羹剩饭内存
//    if (__bytes_left > 0) {
//        _Obj* __STL_VOLATILE* __my_free_list =
//            _S_free_list + _S_freelist_index(__bytes_left);
//
//        ((_Obj*)_S_start_free)->_M_free_list_link = *__my_free_list;
//        *__my_free_list = (_Obj*)_S_start_free;
//    }
//    _S_start_free = (char*)malloc(__bytes_to_get);
//    if (0 == _S_start_free) {
//        size_t __i;
//        _Obj* __STL_VOLATILE* __my_free_list;
//        _Obj* __p;
//         尽量利用我们现有的资源。这不会有什么坏处。
//         我们不会尝试更小的请求，因为这往往会在多进程机器上造成灾难.
//        for (__i = __size;
//            __i <= (size_t)_MAX_BYTES;
//            __i += (size_t)_ALIGN) {
//            __my_free_list = _S_free_list + _S_freelist_index(__i);
//            __p = *__my_free_list;
//            if (0 != __p) {
//                *__my_free_list = __p->_M_free_list_link;
//                _S_start_free = (char*)__p;
//                _S_end_free = _S_start_free + __i;
//                return(_S_chunk_alloc(__size, __nobjs));
//                 Any leftover piece will eventually make it to the
//                 right free list.
//            }
//        }
//        _S_end_free = 0;	// 异常情况
//        _S_start_free = (char*)malloc_alloc::allocate(__bytes_to_get);
//         这样做要么会出现异常，要么会纠正这种情况。因此，我们假设它成功了。
//    }
//    _S_heap_size += __bytes_to_get;
//    _S_end_free = _S_start_free + __bytes_to_get;
//    return(_S_chunk_alloc(__size, __nobjs));
//}

//#ifndef __THROW_BAD_ALLOC
//#  if defined(__STL_NO_BAD_ALLOC) || !defined(__STL_USE_EXCEPTIONS)
//#    include <stdio.h>
//#    include <stdlib.h>
//#    define __THROW_BAD_ALLOC fprintf(stderr, "out of memory\n"); exit(1)
//#  else /* Standard conforming out-of-memory handling */
//#    include <new>
//#    define __THROW_BAD_ALLOC throw std::bad_alloc()
//#  endif
//#endif

//  /* __p may not be 0 */
//static void deallocate(void* __p, size_t __n)
//{
//	if (__n > (size_t)_MAX_BYTES)
//		malloc_alloc::deallocate(__p, __n);
//	else {
//		_Obj* __STL_VOLATILE* __my_free_listchunk
//			= _S_free_list + _S_freelist_index(__n);
//		_Obj* __q = (_Obj*)__p;
//
//		// acquire lock
//#       ifndef _NOTHREADS
//	  /*REFERENCED*/
//		_Lock __lock_instance;
//#       endif /* _NOTHREADS */
//		__q->_M_free_list_link = *__my_free_list;
//		*__my_free_list = __q;
//		// lock is released here
//	}
//}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if 0

#include <iostream>
using namespace std;

//int main()
//{
//	cout << sizeof(unsigned long) << endl;
//
//	return 0;
//}

//using u_char = unsigned char;
//#define ngx_align(d, a)     (((d) + (a - 1)) & ~(a - 1))
//#define ngx_align_ptr(p, a)                                                   \
//    (u_char *) (((uintptr_t) (p) + ((uintptr_t) a - 1)) & ~((uintptr_t) a - 1))
//
//
//int main()
//{
//	cout << ngx_align(5, 4) << endl;
//	char* pc = (char*)malloc(sizeof(char));
//	//cout << pc << endl;
//	//cout << ngx_align_ptr(pc,4) << endl;
//
//	printf("%p\n", pc);
//	printf("%p\n", ngx_align_ptr((char*)17, 8));
//
//	return 0;
//}

int main()
{
	cout << static_cast<size_t>(-1) << endl;
	return 0;
}

#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////4

#if 0

//#include <iostream>
//#include <cstdlib>
//using namespace std;
//
//struct Data
//{
//	FILE* file_; //  = open();
//	int* array_; // = new int[n];
//};
//
//
//
//
//int main()
//{
//	return 0;
//}

#include <iostream>
#include <cstdlib>
using namespace std;

//struct Data {
//	FILE* file_;
//	int* array_;
//};
//
//void fileHandler(void* fp) {
//	fclose((FILE*)fp);
//	fp = nullptr;
//}
//
//void arrayHandler(void* p) {
//	delete[] p;
//	p = nullptr;
//}
//
//int main()
//{
//	// ......
//
//	ngnix_pool_t* pool = ngnix_create_pool(1024, log);
//	Data* p_data = (Data*)ngnix_palloc(pool, sizeof(Data));
//	p_data->file_ = fopen(".\\readme.txt", "r");
//	p_data->array_ = new int[20];
//
//	ngx_pool_cleanup_t* pclean1 = ngx_pool_cleanup_add(pool, sizeof(FILE*));
//	pclean->handler = &fileHandler;
//	pclean->data = p - data->file_;
//
//	ngx_pool_cleanup_t* pclean2 = ngx_pool_cleanup_add(pool, sizeof(int*));
//	pclean->handler = &arrayHandler;
//	pclean->data = p - data->array_;
//
//	// ......
//
//	return 0;
//}

//int main()
//{
//	auto i = 10;
//	return 0;
//}

void print(int i) {
	cout << i << endl;
}

void print(char) = delete;

int main() {
	print(1);
	print('a');
	return 0;
}

#endif
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if 0

#include <string>
#include <iostream>
using namespace std;

union People {
	string str;
	int id;
	double d;

	People() {
		new (&str) string();
	}

	~People() {}
};

int main() {
	People p;
	p.str = "hh";
	cout << p.str << endl;
	p.id = 1;
	cout << p.str << endl;
	p.d = 3.14;
	cout << p.id << endl;
	return 0;
}
#endif

#if 1

//#include <iostream>
//using namespace std;
//
//template<class T, size_t N = 0>
//struct Test {
//	T data_;
//	size_t size_ = N;
//};
//
//
//int main() {
//	Test<int, 1234> t;
//	cout << t.size_ << endl;
//	cout << Test<int>().size_ << endl;
//	return 0;
//}

#include <iostream>
#include <chrono>
#include <ctime>
#include <functional>
#include <thread>
#include <atomic>
using namespace std;

int main() {
	//cout << typeid(1E5).name() << endl;
	//cout << 1E5 << endl;
	//cout << typeid(static_cast<int>(1E5)).name() << endl;
	//cout << static_cast<int>(1E5) << endl;
	// En = 10^n

	//auto rep = chrono::duration<int, ratio<1,2>>(2);
	//cout << rep.count() << endl;

	//auto gap = chrono::minutes(1) - chrono::seconds(61);
	//cout << gap.count() << endl;
	//return 0;

	//auto now = chrono::high_resolution_clock::now();
	//cout << now.time_since_epoch().count() << endl;

	//chrono::system_clock::time_point epoch;
	//time_t t = chrono::system_clock::to_time_t(epoch);
	//cout << ctime(&t) << endl;

	//chrono::system_clock::time_point now = chrono::system_clock::now();
	//time_t time = chrono::system_clock::to_time_t(now);
	//cout << ctime(&time) << endl;

	//auto interval = chrono::duration_cast<chrono::years>(chrono::system_clock::now() - chrono::system_clock::time_point()).count();
	//cout << interval << endl;

	//function<void(int, int, atomic<long long>&)> handler = [](int begin, int end, atomic<long long>& sum) {
	//	for (; begin <= end; ++begin) sum += begin;
	//};

	//atomic<long long> sum1{ 0 };
	//auto begin1 = chrono::high_resolution_clock::now();
	//thread t1(handler, 1, 100000000, sum1);
	//thread t2(handler, 100000001, 200000000, sum1);
	//t1.join();
	//t2.join();
	//auto end1 = chrono::high_resolution_clock::now();

	//long long sum2 = 0;
	//auto begin2 = chrono::high_resolution_clock::now();
	//for (int i = 1; i <= 200000000; ++i) sum2 += i;
	//auto end2 = chrono::high_resolution_clock::now();

	//cout << (end1 - begin1).count() << " " << sum1 << " " << endl;
	//cout << (end2 - begin2).count() << " " << sum2 << " " << endl;

	//return 0;
}

#endif