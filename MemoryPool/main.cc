﻿#include "nginx_memory_pool.h"
#include <iostream>
using namespace std;

struct Inner {
	~Inner()
	{
		cout << "~Inner" << endl;
	}
};

struct Data{
	int* arr;
	Inner* i;
};

void arrayHandler(void* ptr) {
	delete[] ptr;
}

void innerHandler(void* ptr) {
	Inner* p = (Inner*)ptr;
	p->~Inner();
}

int main()
{
	MemoryPool pool(1024);
	Data* dt = (Data*)pool.ngx_palloc(sizeof(Data));
	dt->arr = new int[1024];
	dt->i = new Inner();

	auto res1 = pool.ngx_pool_cleanup_add(sizeof(int*));
	auto res2 = pool.ngx_pool_cleanup_add(sizeof(Inner*));

	res1->data = dt->arr;
	res1->handler = arrayHandler;

	res2->data = dt->i;
	res2->handler = innerHandler;

	return 0;
}

//struct Test {
//	std::function<void(void)> handler_;
//};
//
//int main()
//{
//	Test t1;
//	Test* t2 = new Test();
//	Test* t3 = (Test*)malloc(sizeof(Test));
//
//	t1.handler_ = nullptr;
//	t2->handler_ = nullptr;
//	//t3->handler_ = nullptr;
//
//
//	t1.handler_ = []() -> void {std::cout << "t1"; };
//	t2->handler_ = []() -> void {std::cout << "t2"; };
//	t3->handler_ = []() -> void {std::cout << "t3"; };
//
//	return 0;
//}
