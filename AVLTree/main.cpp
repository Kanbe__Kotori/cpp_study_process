#include "AVLTree.hpp"
#include <iostream>
using namespace simulation;

int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
	AVLTree<int, int> avl;
	for (const auto& k : arr)
	{
		avl.insert(std::make_pair(k, k));
	}

	std::cout << avl.isBalance() << std::endl;
	avl.inOrder();
	return 0;
}