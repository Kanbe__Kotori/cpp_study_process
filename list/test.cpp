#include <iostream>
#include "list.h"
//#include <list>
using namespace simulation;
using std::endl;
using std::cout; 

//int main()
//{
//	list<int> li;
//	li.push_back(1);
//	li.push_back(1);
//	li.push_back(1);
//	li.push_back(1);
//
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	return 0;
//}

void test1()
{
	list<int> li;
	li.push_front(1);
	li.push_front(1);
	li.push_front(1);
	li.push_front(1);

	list<int>::iterator it = li.begin();
	while (it != li.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}

void test2()
{
	list<int> li;
	li.push_back(1);
	li.push_back(2);
	li.push_back(3);
	li.push_back(4);
	li.push_back(5);

	list<int>::reverse_iterator rbit = li.rbegin();
	while (rbit != li.rend())
	{
		cout << *rbit << " ";
		++rbit;
	}
	cout << endl;
}

//void test3()
//{
//	std::list<int> li{ 1,2,3,4,5,6 };
//	std::list<int>::reverse_iterator rbit = li.rbegin();
//}

void test4()
{
	list<int> li(3, 50);
	list<int> li2(li);
	for (const auto& k : li)
	{
		cout << k << " ";
	}
	cout << endl;

	for (const auto& k : li2)
	{
		cout << k << " ";
	}
	cout << endl;

	li2.push_back(2);
	li2.push_back(3);
	li2.push_back(4);
	li2.push_back(5);
	li = li2;
	for (const auto& k : li)
	{
		cout << k << " ";
	}
	cout << endl;

	for (const auto& k : li2)
	{
		cout << k << " ";
	}
	cout << endl;
}

void test5()
{
	list<int> li;
	li.push_back(1);
	li.push_back(1);
	li.push_back(1);
	li.push_back(1);
	li.push_back(1);
	li.push_back(1);

	cout << li.size() << endl;
}

void test6()
{
	list<int> li;
	li.insert(li.begin(), 4);
	li.insert(li.begin(), 3);
	li.insert(li.begin(), 2);
	li.insert(li.begin(), 1);
	for (const auto& k : li)
	{
		cout << k << " ";
	}
	cout << endl;

	li.erase(--li.end());
	li.erase(--li.end());
	li.erase(--li.end());
	for (const auto& k : li)
	{
		cout << k << " ";
	}
	cout << endl;
}

void test7()
{
	list<int> li;
	li.push_back(1);
	li.push_back(1);
	li.push_back(1);
	li.push_back(1);
	for (const auto& k : li)
	{
		cout << k << " ";
	}
	cout << endl;

	li.clear();
	for (const auto& k : li)
	{
		cout << k << " ";
	}
	cout << endl;
}
int main()
{
	test7();
	return 0;
}