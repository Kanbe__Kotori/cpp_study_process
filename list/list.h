#pragma once
#include <algorithm>
#include <cassert>

namespace simulation {

	//链表节点
	template<class T>
	struct ListNode {
		ListNode(const T& val = T())
			: _prev(nullptr)
			, _next(nullptr)
			, _val(val)
		{}

		ListNode<T>* _prev;
		ListNode<T>* _next;
		T _val;
	};

	
	//迭代器有两种实现方式，具体应根据容器底层数据结构实现：
	// 1. 原生态指针，比如：vector
	// 2. 将原生态指针进行封装，因迭代器使用形式与指针完全相同，因此在自定义的类中必须实现以下方法：
	// ① 指针可以解引用，迭代器的类中必须重载operator*()
	// ② 指针可以通过->访问其所指空间成员，迭代器类中必须重载oprator->()
	// ③ 指针可以++向后移动，迭代器类中必须重载operator++()与operator++(int)
	//    至于operator--() / operator--(int)是否需要重载，应该根据具体的结构来抉择：
	//		双向链表可以向前移动，所以需要重载，如果是forward_list就不需要重载
	// ④ 迭代器需要进行是否相等的比较，因此还需要重载operator == ()与operator != ()
	
	//迭代器 是一个封装了很多函数的节点指针
	//加模板是为了同时实现iterator和const_iterator
	//typedef ListIterator<T, T&, T*> iterator;
	//typedef ListIterator<T, const T&, const T*> const_iterator;
	template<class T, class Ref, class Ptr>
	class ListIterator {
	private:
		//把迭代器重命名 简化代码
		typedef ListIterator<T, Ref, Ptr> Self;
		typedef ListNode<T> Node;

	public:
		//将Ref和Ptr重新定义一次 为实现反向迭代器做准备
		//如果不重定义 在实现反向迭代器时使用Ref和Ptr时会报错
		typedef Ref Ref;
		typedef Ptr Ptr;

		//指向节点的指针
		Node* _pNode;

		ListIterator(Node* pNode = nullptr)
			: _pNode(pNode)
		{}

		Ref operator*()
		{
			return _pNode->_val;
		}

		Ptr operator->()
		{
			//return &(_pNode->_val);
			return &(operator*());
		}

		//前置++
		Self& operator++()
		{
			_pNode = _pNode->_next;
			return *this;
		}

		//后置++
		Self operator++(int)
		{
			Self temp(*this);
			_pNode = _pNode->_next;
			return temp;
		}

		Self& operator--()
		{
			_pNode = _pNode->_prev;

			return *this;
		}

		Self operator--(int)
		{
			Self temp(*this);
			_pNode = _pNode->_prev;

			return temp;
		}

		bool operator==(const Self& s) const
		{
			return _pNode == s._pNode;
		}

		bool operator!=(const Self& s) const
		{
			return _pNode != s._pNode;
		}
	};

	//加模板可以同时实现iterator和const_iterator的反向版本
	template<class Iterator>
	class ReverseIterator {
	private:
		typedef ReverseIterator<Iterator> Self;

		// 此处typename的作用是明确告诉编译器，Ref是Iterator类中的一个类型，而不是静态成员变量
		// 因为静态成员变量也是按照 类名::静态成员变量名 的方式访问的
		// 否则编译器编译时就不知道Ref是Iterator中的类型还是静态成员变量
		typedef typename Iterator::Ref Ref;
		typedef typename Iterator::Ptr Ptr;

	public:
		Iterator _it;

		ReverseIterator(Iterator it)
			: _it(it)
		{}

		//调用了ListIterator类中操作符来实现

		Ref operator*()
		{
			Iterator temp(_it);
			--temp;
			return *temp;
		}

		Ptr operator->()
		{
			return &(operator*());
		}

		bool operator==(const Self& s) const
		{
			return _it == s._it;
		}

		bool operator!=(const Self& s) const
		{
			return _it != s._it;
		}

		Self& operator++()
		{
			--_it;
			return *this;
		}

		Self operator++(int)
		{
			Self temp(*this);
			--_it;
			return temp;
		}

		Self& operator--()
		{
			++_it;
			return *this;
		}

		Self operator--(int)
		{
			Self temp(*this);
			++_it;
			return temp;
		}
	};

	//链表
	//在类模板中可以用类名来代替类型 如用list来代替list<T>
	//但是不建议这么做 因为这样就分不清 你在一个地方到底想写类型还是类名还是写错了 可读性不高
	template<class T>
	class list {
	private:
		typedef ListNode<T> Node;
		//指向头节点的指针
		Node* _pHead;

		//创建头节点 只会被构造函数调用 因此一个list对象只会调用一次该函数
		void creatHead()
		{
			_pHead = new Node;

			//因为list是带头双向循环链表
			//所以在链表中没有有效节点时 头节点的next和prev都指向自己
			_pHead->_next = _pHead;
			_pHead->_prev = _pHead;
		}

	public:
		typedef ListIterator<T, T&, T*> iterator;
		typedef ListIterator<T, const T&, const T*> const_iterator;
		typedef ReverseIterator<iterator> reverse_iterator;
		typedef ReverseIterator<const_iterator> const_reverse_iterator;

		//构造函数等
		//默认构造
		list()
		{
			creatHead();
		}

		//避免在T为int类型时会误调用迭代器构造 所以这里是 int n 而不是 size_t n
		list(int n, const T& val = T())
		{
			assert(n >= 0);
			creatHead();
			while (n--)
			{
				push_back(val);
			}
		}

		template<class InputIterator>
		list(InputIterator first, InputIterator last)//左闭右开 -》[first, last)
		{
			creatHead();
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		list(const simulation::list<T>& l)
		{
			creatHead();
			for (auto& k : l)
			{
				push_back(k);
			}
		}

		~list()
		{
			clear();
			delete _pHead;
			_pHead = nullptr;
		}

		list<T>& operator=(const list<T>& l)
		{
			if (this != &l)
			{
				list<T> temp(l);
				swap(temp);
			}

			return *this;
		}


		//迭代器
		iterator begin()
		{
			//可以写成return _pHead->_next
			//因为在返回时_pHead->_next会作为参数调用iterator的构造函数 而形成一个临时的iterator对象作为返回值
			return iterator(_pHead->_next);
		}

		const_iterator begin() const
		{
			return const_iterator(_pHead->_next);
		}

		iterator end()
		{
			return iterator(_pHead);
		}

		const_iterator end() const
		{
			return const_iterator(_pHead);
		}


		//begin、end与rbegin、rend是镜像对称的
		//即begin和rend一起指向第一个元素 end与rbegin一起指向最后一个数据再后面的一个位置
		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}

		const_reverse_iterator rbegin() const
		{
			return const_reverse_iterator(end());
		}

		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		const_reverse_iterator rend() const
		{
			return const_reverse_iterator(begin());
		}

		//容量相关
		size_t size() const
		{
			size_t count = 0;
			const_iterator cit = begin();
			while (cit != end())
			{
				++count;
				++cit;
			}
			return count;
		}

		bool empty() const
		{
			//return size() == 0; 时间复杂度为O(N)
			return _pHead->_next == _pHead; //时间复杂度为O(1)
		}

		//元素访问
		T& front()
		{
			return _pHead->_next->_val;
		}

		const T& front() const
		{
			return _pHead->_next->_val;
		}

		T& back()
		{
			return _pHead->_prev->_val;
		}

		const T& back() const
		{
			return _pHead->_prev->_val;
		}

		iterator insert(iterator pos, const T& val)
		{
			Node* cur = pos._pNode;
			Node* prev = cur->_prev;

			Node* newNode = new Node(val);
			newNode->_next = cur;
			cur->_prev = newNode;

			newNode->_prev = prev;
			prev->_next = newNode;

			return iterator(newNode);
		}

		iterator erase(iterator pos)
		{
			//不能删掉头节点
			assert(pos != end());

			Node* cur = pos._pNode;
			Node* next = cur->_next;
			Node* prve = cur->_prev;

			delete cur;
			next->_prev = prve;
			prve->_next = next;

			return iterator(next);
		}

		void push_front(const T& val)
		{
			insert(begin(), val);
		}

		void push_back(const T& val)
		{
			insert(end(), val);
		}

		void pop_front()
		{
			erase(begin());
		}

		void pop_back()
		{
			erase(--end());
		}

		void swap(simulation::list<T>& l)
		{
			std::swap(_pHead, l._pHead);
		}

		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}
	};
}