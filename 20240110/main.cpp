#include <iostream>
#include <iomanip>
using namespace std;

//剑指offer 牛客 JZ64 第三种方法 类和对象
// class Sum {
// public:
//     Sum()
//     {
//         _sum += _i;
//         ++_i;
//     }

//     static int getSum()
//     {
//         return _sum;
//     }

//     static void reSet()
//     {
//         _sum = 0;
//         _i = 1;
//     }

// private:
//     static int _sum;
//     static int _i;
// };

// int Sum :: _sum = 0;
// int Sum :: _i = 1;

// class Solution {
// public:
//     int Sum_Solution(int n) {
//         //创建一个n长度的Sum数组 每个对象在被创建时都会调用构造函数 一共调用了n次
//         //利用这个方法 再加上一个数字i随着调用次数而变化 就能达到题目要求
//         //每次使用前 都要将_sum和_i的值重置 避免多次调用（多线程）使结果出错
//         Sum::reSet();
//         Sum s[n];  
//         return Sum ::getSum();
//     }
// };

// int main()
// {
//     // cout << sizeof(long) << endl;
//     // cout << sizeof(long long) <<endl;

//     Solution s;
//     cout << s.Sum_Solution(5) << endl;
//     return 0;
// }

// class Test {
// public:
//     // Test(int test)
//     //     //静态变量无法通过初始化列表进行初始化
//     //     // : _test(test)
//     // {
//     //     // _test = 100;
//     // }

//     void getData()
//     {
//         cout << _test << endl;
//     }

// private:
//     static int _test;
// };

// int Test :: _test = 10;

// int main()
// {
//     // Test t(10);
//     //C++的static和Java的static完全不一样
//     //C++的太不方便了
//     Test t;
//     t.getData();
//     return 0;
// }
class Date {
    //友元函数
    friend ostream& operator<<(ostream& out, const Date& date);
    friend istream& operator>>(istream& in, Date& date);
public:
    Date(int year, int month, int day)
        : _year(year)
        , _month(month)
        , _day(day)
    {}
private:
    int _year;
    int _month;
    int _day;
};

//必须定义在类的外面
//因为成员函数中this一定作为第一个参数
//如果定义在类里面就会变成 d1 << cout
//这样能达成效果 但是不符合cout的使用方法
ostream& operator<<(ostream& out, const Date& date)
{
    out << date._year << " " << date._month << " " << date._day << endl;
    return out;
}

istream& operator>>(istream& in, Date& date)
{
    in >> date._year >> date._month >> date._day;
    return in;
}

int main()
{
    Date d1(1111, 22, 33);
    Date d2(2024, 1, 10);
    cout << d1 << d2;
    cin >> d1;
    cout << d1;
    return 0;
}