#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <iostream>
using namespace std;

//牛客  HJ73 计算日期到天数转换
//#include <iostream>
//using namespace std;
//
//int main() {
//    int monthDays[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//    int year, month, day;
//    cin >> year >> month >> day;
//    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
//    {
//        monthDays[2] += 1;
//    }
//
//    int count = 0;
//    for (int i = 1; i < month; ++i)
//    {
//        count += monthDays[i];
//    }
//
//    count += day;
//    cout << count << endl;
//    return 0;
//}

//class Test {
//public:
//	void print()
//	{
//		cout << "test" << endl;
//	}
//};
//
//int main()
//{
//	//如果构造对象什么参数也不传 要像t2这样写 因为编译器会把t1这钟写法看作函数的声明 t1则是函数名 t1的类型为  Test (*)()
//	//Test t1();
//	//t1.print();
//	Test t2;
//	t2.print();
//
//	//匿名类 生命周期只有1行 用于调用类里面的非静态成员函数
//	Test().print();
//
//	return 0;
//}

//void test()
//{
//	cout << &a << endl;
//	static int a = 0;
//}
//
//int main()
//{
//	return 0;
//}