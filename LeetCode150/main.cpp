//TODO: LeetCode150 逆波兰表达式求值

class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<int> opNumStack;
        for(const auto& str : tokens) {
            //如果是操作数就入栈 
            //是操作符就取出两个数计算结果后再入栈

            if(str == "+" || str == "-" ||str == "*" || str == "/") {
                //在栈中 右操作数在左操作数的上边 所以先取出的是右操作数
                int right = opNumStack.top();
                opNumStack.pop();
                int left = opNumStack.top();
                opNumStack.pop();

                //C++中的switch语句只能作用于内置类型 不能作作用于自定义类型
                //因为操作符字符串的长度只有1 所以能够通过0下标得到相应的字符
                switch(str[0]) {
                    case '+' :
                    opNumStack.push(left + right);
                    break;
                    case '-' :
                    opNumStack.push(left - right);
                    break;
                    case '*' :
                    opNumStack.push(left * right);
                    break;
                    case '/' :
                    opNumStack.push(left / right);
                    break;
                    default :
                    cout << "switch err" << endl;
                    break;
                }
            }
            else {
                opNumStack.push(stoi(str));
            }
        }

        return opNumStack.top();
    }
};