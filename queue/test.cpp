#include "queue.h"
#include <list>
#include <iostream>
using namespace std;
using namespace simulation;

int main(int argc, char* argv, char* env)
{
	queue<int, list<int>> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	q.push(5);
	cout << q.front() << endl;
	cout << q.back() << endl;

	while (!q.empty())
	{
		cout << q.front() << " ";
		q.pop();
	}
	cout << endl;
	return 0;
}

//int main()
//{
//	list<int> li{ 1 };
//	li.erase(li.end());
//	for (auto& k : li)
//	{
//		cout << k << endl;
//	}
//	return 0;
//}