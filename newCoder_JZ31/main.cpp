//TODO-> newCoder_JZ31 栈的压入、弹出序列

// class Solution {
// public:
//     bool IsPopOrder(const vector<int>& pushV, const vector<int>& popV) {
//         stack<int> temp;
//         int i = 0;
//         for(const auto& k : pushV)
//         {
//             temp.push(k);
//             while(!temp.empty() && temp.top() == popV[i])
//             {
//                 temp.pop();
//                 ++i;
//             }
//         }
//         return temp.empty();
//     }
// };