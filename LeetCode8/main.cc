//TODO -> LeetCode8 字符串转换整数 (atoi)

class Solution {
public:
    int myAtoi(string s) {
        //如果为空字符串 就直接返回0
        if (s.empty()) {return 0;}

        int index = 0, len = s.size(), flag = 1, ans = 0;
        //处理前导空格
        while (index < len && s[index] == ' ') {++index;}

        //确定符号 有人会脑子抽到在数字前加'+'号 所以也要进行判断
        if (index < len && (s[index] == '-' || s[index] == '+'))
        {
            flag = s[index++] == '-' ? -1 : 1;
        }

        //转换 不仅要保证index < len 还因为有可能数字的后面会跟字母 所以还要保证读完数字就结束
        while (index < len && isdigit(s[index]))
        {
            //string类存储的数据类型为char类型 所以取出来的数字都是char类型的 想把其转成int类型就要减'0'
            int digit = s[index] - '0';

            //判断是否溢出
            if (ans > (INT_MAX - digit) / 10)
            {
                return flag == 1 ? INT_MAX : INT_MIN;
            }

            ans = ans * 10 + digit;

            ++index;
        }

        //如果传入的字符串s是以字母开头的话 前面的if全都要不没有作用 要不就没进去 所以最后返回的答案就是ans的初值0
        return ans * flag;
    }
};