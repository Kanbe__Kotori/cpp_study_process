#include <iostream>
#include <list>
#include <string>
using namespace std;

//int main()
//{
//	//默认拷贝拷贝构造 list<T> lt;(T为list存储的数据类型)
//	list<string> ls;
//
//	return 0;
//}

//int main()
//{
//	//填充构造    list (size_type n, const value_type& val = value_type())
//
//	list<int> li(5, 20);
//
//	//如果不指定val的值就会初始化为相应类型的默认值
//	//int为0 double为0.0 char为'\0'……
//	list<double> ld(3);
//
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	for (const auto& k : ld)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	//迭代器构造 list (InputIterator first, InputIterator last) 迭代器区间为左闭右开 -》 [first, last)
//
//	//用数组构造
//	int array[] = { 1,2,3,4,5,6 };
//	list<int> li(array, array + sizeof(array) / sizeof(array[0]));
//
//	//这个其实是先用数组构造了一个list<int> temp 然后用拷贝构造将temp拷贝给了li_another -》 list<int> li_another(temp)
//	list<int> li_another{ 1,2,3,4,5 };
//
//	//可以用别的容器的迭代器来构造list
//	string s("Hello list!");
//	list<char> lc(s.begin(), s.end());
//
//	//可以用list的迭代器来构造list
//	list<char> lc_copy(++lc.begin(), --lc.end());
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	for (const auto& k : li_another)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	for (const auto& k : lc)
//	{
//		cout << k;
//	}
//	cout << endl;
//
//	for (const auto& k : lc_copy)
//	{
//		cout << k;
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	//拷贝构造 list (const list& x)
//
//	list<int> li{ 1,2,3,4,5 };
//
//	//拷贝构造
//	list<int> li_copy(li);
//
//	//赋值运算符重载
//	list<int> li_another = li;
//
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;	
//
//	for (const auto& k : li_copy)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	for (const auto& k : li_another)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	list<int> li{ 1,2,3,4,5 };
//	cout << *li.begin() << endl;
//	cout << *(--li.end()) << endl;
//
//	return 0;
//}

//int main()
//{
//	list<int> li{ 1,2,3,4,5,6,7 };
//	list<int>::reverse_iterator rbit = li.rbegin();
//	while (rbit != li.rend())
//	{
//		//从begin到end 从rbegin到rend都是++
//		cout << *rbit << " ";
//		++rbit;
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	list<string> ls{ "Hello", "lisr", "and", "string", "!" };
//	cout << ls.size() << endl;
//	return 0;
//}

//int main()
//{
//	list<int> li;
//	cout << li.empty() << endl;
//
//	//尾插一个1
//	li.push_back(1);
//
//	cout << li.empty() << endl;
//	return 0;
//}

//int main()
//{
//	list<int> li{ 1,2,3,4,5,6 };
//	cout << li.front() << endl;
//
//	return 0;
//}

//int main()
//{
//	list<string> ls{ "Hello", "list!" };
//	cout << ls.back() << endl;
//
//	return 0;
//}

//int main()
//{
//	list<int> li{ 2,3,4,5 };
//
//	//头插1
//	li.push_front(1);
//
//	//尾插6
//	li.push_back(6);
//
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	list<int> li{ 1,2,3,4,5,6 };
//	//头删
//	li.pop_front();
//
//	//尾删
//	li.pop_back();
//
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	list<int> li{ 1,2,3,5,6,7 };
//
//	list<int>::iterator pos = li.begin();
//
//	//list的迭代器不支持直接加减一个值。比如我们要在5的前面插入一个4，就必须让pos++3次，而不能pos + 3
//	//循环的次数可以看成目标位置下标的数值，但不是真正的下标，因为list节点并不分布在一块连续的空间
//	for (int i = 0; i < 3; ++i)
//	{
//		++pos;
//	}
//
//	li.insert(pos, 4);
//
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	list<int> li{ 1,2,3,4,5,6,7,8 };
//	list<int>::iterator pos = li.begin();
//
//	//删除list中的所有偶数
//	while (pos != li.end())
//	{
//		if (*pos % 2 == 0)
//		{
//			pos = li.erase(pos);
//		}
//		else
//		{
//			++pos;
//		}
//	}
//
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	list<int> li1{ 1, 2, 3, 4 ,5 };
//	list<int> li2{ 6, 7, 8 ,9, 10 };
//
//	li1.swap(li2);
//
//	cout << "li1的数据为：";
//	for (const auto& k : li1)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	cout << "li2的数据为：";
//	for (const auto& k : li2)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	list<int> li{ 1,2,3,4,5 };
//	cout << "clear前的数据为：";
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	li.clear();
//	cout << "clear后的数据为：";
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	list<int> des1{ 1,2,3,4,5 };
//	list<int> des2(des1);
//	list<int> des3 = des2;
//
//	list<int> sour1{ 6,7,8,9,10 };
//	list<int> sour2(sour1);
//	list<int> sour3 = sour2;
//
//	//转移一整个链表
//	des1.splice(des1.end(), sour1);
//
//	//转移一个元素
//	des2.splice(des2.end(), sour2, sour2.begin());
//
//	//转移一个区间 -》左闭右开
//	des3.splice(des3.end(), sour3, ++sour3.begin(), --sour3.end());
//
//	cout << "des1的数据为：";
//	for (const auto& k : des1)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	cout << "sour1的数据为：";
//	for (const auto& k : sour1)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	cout << "des2的数据为：";
//	for (const auto& k : des2)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	cout << "sour2的数据为：";
//	for (const auto& k : sour2)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	cout << "des3的数据为：";
//	for (const auto& k : des3)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	cout << "sour3的数据为：";
//	for (const auto& k : sour3)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	list<int> li{ 1,2,3,1,1,5,8,5,4,3 };
//	cout << "remove前的数据为：";
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	li.remove(1);
//	cout << "remove后的数据为：";
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	list<int> li{ 32,5,86,12,6,2 };
//	li.sort();
//	for (auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	list<int> li{ 1,4,6,4,6,7,8,9,1,3,5 };
//
//	//在去重之前建议先对list进行排序 不然效率实在是太低了 不过排序的效率也很低就是了
//	li.sort();
//	li.unique();
//
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	list<int> li{ 1,2,3,4,5,6 };
//
//	li.reverse();
//	for (const auto& k : li)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

