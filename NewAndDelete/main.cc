﻿#include <iostream>
#include <cassert>
using namespace std;

//#include <iostream>
//using namespace std;
//
//template<class T>
//class ListNode 
//{
//public:
//	ListNode() 
//		: data_(T())
//		, prev_(nullptr)
//		, next_(nullptr)
//	{
//		cout << "ListNode()" << endl;
//	}
//
//	~ListNode() 
//	{
//		cout << "~ListNode()" << endl;
//	}
//private:
//	T data_;
//	ListNode<T>* prev_;
//	ListNode<T>* next_;
//};
//
//int main() 
//{
//	//int* array = new int[3] {1, 2, 3};
//	//for (int i = 0; i < 3; ++i) {
//	//	cout << array[i] << " ";
//	//}
//	//cout << endl;
//
//	//ListNode<int>* node = new ListNode<int>();
//	//delete node;
//
//	return 0;
//}

//#include <cstdlib> // cpp
//// #include <stdlib.h> // c
//
//int main()
//{
//	int* p1 = (int*)malloc(sizeof(int)); // 申请1个int类型的空间
//	int* p2 = (int*)malloc(sizeof(int) * 10); // 申请10个int类型的空间
//	free(p1); // 回收p1指向的空间
//	free(p2); // 回收p2指向的空间
//
//	int* p3 = (int*)calloc(4, sizeof(int)); // 申请4个int类型的空间，并把这些空间初始化为0
//	int* p4 = (int*)realloc(p3, sizeof(int) * 100); // 将p3指向的空间扩容成100个int类型
//
//	free(p4); // 这里不需要free(p3) 因为在realloc的时候就已经释放了p3
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//int main() 
//{
//	// 开单个空间
//	int* p1 = (int*)malloc(sizeof(int));
//	int* p2 = new int;
//
//	// 开数组（连续）空间
//	int* p3 = (int*)malloc(sizeof(int) * 5);
//	int* p4 = new int[5];
//
//
//	free(p1);
//	delete p2;
//	free(p3);
//	delete[] p4;
//}

//class Test
//{
//public:
//	~Test()
//	{
//		cout << "~Test()" << endl;
//	}
//};
//
//int main() 
//{
//	int* p1 = new int(10); // 申请1个int的空间，并初始化为10
//
//	int* p2 = new int[5] {1, 2, 3, 4, 5}; // 申请5个int的空间，分别初始化为1、2、3、4、5
//	int* p3 = (int*)calloc(5, sizeof(int)); // 申请5个int的空间，全部初始化为0
//	assert(p3);
//
//	cout << *p1 << endl;
//
//	cout << "p2: ";
//	for (int i = 0; i < 5; ++i) {
//		cout << p2[i] << " ";
//	}
//	cout << endl;
//
//	cout << "p3: ";
//	for (int i = 0; i < 5; ++i) {
//		cout << p3[i] << " ";
//	}
//	cout << endl;
//
//	delete p1;
//	delete[] p2;
//	free(p3);
//
//	Test* t1 = new Test();
//	Test* t2 = new Test();
//
//	delete t1, t2;
//
//	return 0;
//}
//

//struct Test
//{
//	Test() : data_(520)
//	{
//		cout << "Test()" << endl;
//	}
//
//	~Test()
//	{
//		cout << "~Test()" << endl;
//	}
//
//	int data_;
//};

//int main()
//{
//	cout << "malloc/free:" << endl;
//	Test* test1 = (Test*)malloc(sizeof(Test));
//	assert(test1);
//	cout << test1->data_ << endl;
//	free(test1);
//
//	cout << endl;
//	cout << "---------------------------";
//	cout << endl;
//
//	cout << "new/delete:" << endl;
//	Test* test2 = new Test();
//	cout << test2->data_ << endl;
//	delete test2;
//
//	return 0;
//}

//class Test
//{
//public:
//	Test() = default;
//	~Test() = default;
//
//	void* operator new(size_t size)
//	{
//
//	}
//};

//int main()
//{
//	size_t size = 128; // 防止整形溢出
//	void* p1 = malloc(size * 1024 * 1024 * 1024); // 申请32G的内存空间
//	cout << p1 << endl; // 如果没申请到p1就为空指针
//	if (p1) free(p1); // 如果申请到了则释放空间
//
//	try
//	{
//		void* p2 = operator new(size * 1024 * 1024 * 1024); // 申请32G的内存空间
//		cout << p2 << endl; // 如果没申请到就会抛异常，就不会执行该语句x
//		operator delete(p2);
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}

//struct ListNode
//{
//	ListNode* prev_;
//	ListNode* next_;
//	int data_;
//
//	void* operator new(size_t size)
//	{
//		void* p = nullptr;
//
//		// 假装这里是在内存池中申请空间的逻辑...
//		p = allocator<ListNode>().allocate(1);
//		cout << "内存池分配空间" << endl;
//
//		return p;
//	}
//
//	void operator delete(void* p)
//	{
//		// 假装这里是在内存池中回收空间的逻辑...
//		allocator<ListNode>().deallocate((ListNode*)p, 1);
//
//		cout << "内存池回收空间" << endl;
//	}
//};

//int main()
//{
//	ListNode* node = new ListNode();
//	delete node;
//	return 0;
//}

struct Test
{
	Test(int data) :data_(data)
	{
		cout << "Test()" << endl;
	}
	~Test()
	{
		cout << "~Test() -> " << data_ << endl;
	}

	int data_;
};

//int main()
//{
//	cout << "new/delete:" << endl;
//	Test* t1 = new Test(1234);
//	cout << t1->data_ << endl;
//	delete t1;
//
//	cout << endl;
//	cout << "-----------------------" << endl;
//	cout << endl;
//
//	cout << "my new/delete:" << endl;
//	Test* t2 = (Test*)operator new(sizeof(Test));
//	cout << "operator new end" << endl;
//	new (t2) Test(5678);
//	cout << "placement-new end" << endl;
//	cout << t2->data_ << endl;
//	t2->~Test();
//	operator delete(t2);
//
//	return 0;
//}

int main()
{
	Test* t1 = new Test(1);
	Test* t2 = new Test(2);

	delete t1, t2; // error! 只会delete t1

	Test* t3 = new Test(3);
	Test* t4 = new Test(4);

	delete (t3, t4); // error! 只会delete t4

	return 0;
}