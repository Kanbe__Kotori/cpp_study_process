#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <iostream>
using namespace std;

//int main()
//{
//	int* arr1 = new int[5] {1, 2, 3, 4, 5};
//	int* arr2 = new int[5];
//	int arr3[10] = { 0 };
//	int* arr4 = new int[5]();
//	int* arr5 = new int[10] {1, 2, 3, 4, 5};
//	//for (int& k : arr3)
//	//{
//	//	cout << k << " ";
//	//}
//	//cout << endl;
//
//	//这里的arr1是指针而不是数组名 所以sizeof(arr1) = 4或8
//	//并且因为加强版for只能用于数组 所以不能用加强for来遍历arr1
//	//for (int k = 0; k < sizeof(arr1) / sizeof(arr1[0]); ++k)
//	//{
//	//	cout << arr1[k] << " ";
//	//}
//	//cout << endl;
//
//	//delete[] arr1, arr2;
//	//cout << typeid(arr1).name() << endl;
//	//cout << typeid(arr3).name() << endl;
//
//	delete[] arr1;
//	delete[] arr2;
//	delete[] arr4;
//	delete[] arr5;
//
//	return 0;
//}

//#include <stdio.h>
//#include <stdlib.h>
//
//int main()
//{
//	int* arr1 = (int*)malloc(10 * sizeof(int));
//	int* arr2 = (int*)calloc(10, sizeof(int));
//
//	free(arr1);
//	free(arr2);
//	return 0;
//}

//class A
//{
//public:
//	A(int a = 0)
//		: _a(a)
//	{
//		cout << "A():" << this << endl;
//	}
//
//	~A()
//	{
//		cout << "~A():" << this << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	// 内置类型是几乎是一样的
//	int* p3 = (int*)malloc(sizeof(int)); // C
//	int* p4 = new int;
//	free(p3);
//	delete p4;
//	cout << "----------------" << endl;
//
//	// new/delete 和 malloc/free最大区别是 new/delete对于【自定义类型】除了开空间还会调用构造函数和析构函数
//	A* p1 = (A*)malloc(sizeof(A));
//	A* p2 = new A(1);
//	free(p1);
//	delete p2;
//	cout << "----------------" << endl;
//
//	A* p5 = (A*)malloc(sizeof(A) * 10);
//	A* p6 = new A[10];
//	free(p5);
//	delete[] p6;
//	cout << "----------------" << endl;
//
//	return 0;
//}

///*
// * operator new：该函数实际通过malloc来申请空间，当malloc申请空间成功时直接返回；
// * 申请空间失败，尝试执行空间不足应对措施，如果改应对措施用户设置了，则继续申请，否则抛异常。
//*/
//void* __CRTDECL operator new(size_t size) _THROW1(_STD bad_alloc)
//{
//    // try to allocate size bytes
//    void* p;
//    while ((p = malloc(size)) == 0)
//        if (_callnewh(size) == 0)
//        {
//            // report no memory
//            // 如果申请内存失败了，这里会抛出bad_alloc 类型异常
//            static const std::bad_alloc nomem;
//            _RAISE(nomem);
//        }
//    return (p);
//}
///*
// * operator delete: 该函数最终是通过free来释放空间的
//*/
//void operator delete(void* pUserData)
//{
//    _CrtMemBlockHeader* pHead;
//    RTCCALLBACK(_RTC_Free_hook, (pUserData, 0));
//    if (pUserData == NULL)
//        return;
//    _mlock(_HEAP_LOCK);  /* block other threads */
//    __TRY
//        /* get a pointer to memory block header */
//        pHead = pHdr(pUserData);
//    /* verify block type */
//    _ASSERTE(_BLOCK_TYPE_IS_VALID(pHead->nBlockUse));
//    _free_dbg(pUserData, pHead->nBlockUse);
//    __FINALLY
//        _munlock(_HEAP_LOCK);  /* release other threads */
//    __END_TRY_FINALLY
//        return;
//}
///*
//free的实现
//*/
//#define free(p) _free_dbg(p, _NORMAL_BLOCK)

//int main()
//{
//
//	size_t n = 128;
//	void* ptr = operator new(n * 1024 * 1024 * 1024);
//	cout << ptr << endl;
//
//	//try
//	//{
//	//	size_t n = 128;
//	//	void* ptr = operator new(n * 1024 * 1024 * 1024 * 1024);
//	//	cout << ptr << endl;
//	//}
//	//catch (exception& e)
//	//{
//	//	cout << e.what() << endl;
//	//}
//}

//class A
//{
//public:
//	A(int a = 0)
//		: _a(a)
//	{
//		cout << "A():" << this << endl;
//	}
//
//	~A()
//	{
//		cout << "~A():" << this << endl;
//	}
//
//	void* operator new(size_t szie)
//	{
//		cout << "operator new" << endl;
//		return nullptr;
//	}
//
//	void operator delete(void* p)
//	{
//		cout << "opereator delete" << endl;
//	}
//private:
//	int _a;
//};
//
//int main()
//{
//	A* i = new A;
//	delete i;
//	return 0;
//}

class A {
public:
	A(int a= 0 , int b = 0, double c = 0.0)
		: _a(a)
		, _b(b)
		, _c(c)
	{
		cout << "A(int a= 0 , int b = 0, double c = 0.0)" << endl;
	}

	~A()
	{
		cout << "~A" << endl;
	}

	void getData()
	{
		cout << "-a = " << _a << " _b = " << _b << " _c = " << _c << endl;
	}

private:
	int _a;
	int _b;
	double _c;
};

int main()
{
	A* pa = (A*)operator new(sizeof(A));
	new(pa)A(1, 2, 3.0);
	pa->getData();
	new(pa)A;
	pa->getData();
	pa->~A();
	operator delete(pa);
}