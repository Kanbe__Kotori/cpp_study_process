//#include "HashTable.hpp"
//
//using namespace closedHashTable;
//
//template<class K, class V>
//struct HashSetKeyOfData {
//	size_t operator() (const std::pair<K, V> kv) const
//	{
//		return (size_t)kv.first;
//	}
//};
//
//int main()
//{
//	HashTable<int, std::pair<int, int>, HashSetKeyOfData<int, int>> ht;
//	int array[]{ 1,2,3,4,5,6,7,8 };
//	for (const auto& i : array) {
//		ht.insert(std::make_pair(i, i));
//	}
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//int main() {
//	//auto是不会自带const或者是&的
//	const int a = 10;
//	int b = 5;
//	auto c = a / b;
//	auto& d = c;
//	const auto e = a / b;
//	auto f = d;
//
//	return 0;
//}

//#pragma execution_character_set("utf-8")

//#include <iostream>
//using namespace std;
//
//int main()
//{
//	const int i = 10;
//	int array[i] = {};
//	return 0;
//}

//#include <iostream>
//#include "unordered_map.hpp"
//using namespace simulation;
//
//int main()
//{
//	std::cout << typeid(1).name() << std::endl;
//	std::cout << typeid((bool)1).name() << std::endl;
//	return 0;
//}
