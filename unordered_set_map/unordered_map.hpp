#pragma once

#include "HashTable.hpp"

namespace simulation {
	template<class K, class V, class Hash = hash_bucket::HashKeyConvertToUI<K>>
	class unordered_map {
	private:
		struct Unordered_mapKeyOfT {
			const K& operator() (const std::pair<const K, V>& kv) const
			{
				return kv.first;
			}
		};

	private:
		hash_bucket::HashTable<K, const std::pair<const K, V>, Hash, Unordered_mapKeyOfT> _ht;

	public:
		typedef typename hash_bucket::HashTable<K, std::pair<const K, V>, Hash, Unordered_mapKeyOfT>::iterator iterator;

	public:
		iterator begin()
		{
			return _ht.begin();
		}

		iterator end()
		{
			return _ht.end();
		}

		std::pair<iterator, bool> insert(const std::pair<const K, V>& kv)
		{
			return _ht.insert(kv);
		}

		iterator find(const K& key)
		{
			return _ht.find(key);
		}

		bool erase(const K& key)
		{
			return _ht.erase(key);
		}

		V& operator[](const K& key)
		{
			return (*(this->insert(std::make_pair<key, V()>).first)).second;
		}
	};
}