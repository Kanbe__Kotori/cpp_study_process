#pragma once

#include "HashTable.hpp"

namespace simulation {
	template<class K, class Hash = hash_bucket::HashKeyConvertToUI<K>>
	class unordered_set {
	private:
		struct Unordered_setKeyOfT {
			const K& operator() (const K& key) const
			{
				return key;
			}
		};

	private:
		hash_bucket::HashTable<K, K, Hash, Unordered_setKeyOfT> _ht;

	public:
		typedef typename hash_bucket::HashTable<K, K, Hash, Unordered_setKeyOfT>::iterator iterator;

	public:
		iterator begin()
		{
			return _ht.begin();
		}

		iterator end()
		{
			return _ht.end();
		}

		std::pair<iterator, bool> insert(const K& key)
		{
			return _ht.insert(key);
		}

		iterator find(const K& key)
		{
			return _ht.find(key);
		}

		bool erase(const K& key)
		{
			return _ht.erase(key);
		}
	};
}