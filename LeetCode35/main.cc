//todo: LeetCode35. 搜索插入位置
//* ver1 暴力解法
class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
        int left = 0, right = nums.size() - 1;
        int mid = 0;
        bool flag = false;
        while (left <= right) {
            mid = left + (right - left) / 2;
            if (nums[mid] > target) {
                right = mid - 1;
                flag = false;
            }
            else if (nums[mid] < target) {
                left = mid + 1;
                flag = true;
            }
            else {
                return mid;
            }
        }

        return flag ? mid + 1 : mid;
    }
};

//* ver2 变聪明法
class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
        int left = 0, right = nums.size() - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] > target) {
                right = mid - 1;
            }
            else if (nums[mid] < target) {
                left = mid + 1;
            }
            else {
                return mid;
            }
        }

        return left; //! 如果没有找到，left恰好就是插入位置
    }
};