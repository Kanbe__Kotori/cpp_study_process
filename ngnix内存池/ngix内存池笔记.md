max 最大小块内存

单个内存块一次性可以分配出去的最大空间

内存对齐的好处：减少CPUio的次数，提高效率



~~~C++
// 第一块进行特殊处理
p = pool;
p -> d.last = (u_char*)p + sizeof(ngx_pool_t);
p -> d.failed = 0;

// 从第二块开始
for (p = p -> d.next; p; p = p -> d.next) {
    p -> d.next = (u_char*)p + sizeof(ngx_pool_data_t);
    p -> d.failed = 0;
}
~~~

nginx内存池的设计方案只适用于http服务器这种短连接，间歇性给客户端提供资源的应用场景。SGI-STL二级空间配置器可以适用于任何场景，如长连接