//#include <iostream>
//#include <SDL.h>
//using namespace std;
//
//int main(int argc, char** agrv) // int argc, char* argv[] || int, char**
//{
//	SDL_Log("Hello Visual Studio SDL2!\n");
//	return 0;
//}

//#include <iostream>
//#include <thread>
//#include <Windows.h>
//using namespace std;
//
//int main()
//{
//	int array[] = { 6,3,2,8,90,1,2,3,4,5,6,1,8 };
//	const size_t size = sizeof(array) / sizeof(array[0]);
//	thread threads[size];
//	size_t index = 0;
//	for (auto& i : array)
//	{
//		threads[index++] = thread([&i]() -> void {
//			Sleep(i);
//			cout << i << endl;
//			});
//	}
//
//	for (auto& th : threads)
//	{
//		th.join();
//	}
//	return 0;
//}

#include<cstdlib>
#include<chrono>
#include<iostream>
#include<thread>
#include<vector>
#include <algorithm>

//void SleepSort(const int i)
//{
//	std::this_thread::sleep_for(std::chrono::milliseconds(i));
//	std::cout << i << " ";
//}

//void SleepSort(const int i, const int offset)
//{
//	std::this_thread::sleep_for(std::chrono::milliseconds(i + offset));
//	std::cout << i << " ";
//}
//
//int main()
//{
//	std::cout << "Sorted array is:\n";
//	std::vector<std::thread> threads;
//	std::vector<int> num;
//	int x;
//	while (std::cin >> x)
//		num.push_back(x);
//	//for (int i = 0; i < num.size(); ++i)
//	//	threads.push_back(std::thread(SleepSort, num[i]));
//
//	//添加负值排序
//	std::vector<int> data;
//	data.reserve(num.size() - 1);
//	for (int i = 0; i < num.size(); ++i)
//		data.push_back(num[i]);
//	const int minElem = std::abs(*std::min_element(data.begin(), data.end()));  //min_element寻找范围 [first, last) 中的最小元素。
//	for (int i = 0; i < num.size(); ++i)
//		threads.push_back(std::thread(SleepSort, num[i], minElem));
//
//	for (auto& thread : threads)
//		thread.join();
//
//	return 0;
//}

//#include "SDL.h"
//#include <windows.h>
//
//int main(int, char**)
//{
//	ShowWindow(GetConsoleWindow()/*GetForegroundWindow()*/, SW_HIDE);
//	SDL_Init(SDL_INIT_EVERYTHING);
//
//	SDL_Window* window = SDL_CreateWindow("Hello SDL2", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 720, SDL_WINDOW_SHOWN);
//	SDL_Renderer* render = SDL_CreateRenderer(window, -1, 0);
//
//	bool is_Quit = false;
//	SDL_Event event;
//
//	SDL_SetRenderDrawColor(render, 0, 255, 0, 255);
//	while(!is_Quit) 
//	{
//		while (SDL_PollEvent(&event))
//		{
//			if (event.type == SDL_QUIT)
//			{
//				is_Quit = true;
//			}
//		}
//
//		SDL_Rect rect;
//		rect.x = 100;
//		rect.y = 100;
//		rect.w = 200;
//		rect.h = 100;
//		SDL_RenderDrawRect(render, &rect);
//
//		SDL_RenderPresent(render);
//	}
//
//	SDL_DestroyRenderer(render);
//	SDL_DestroyWindow(window);
//	SDL_Quit();
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//class test {
//public:
//	~test()
//	{
//		cout << "~test()" << endl;
//	}
//};
//
//int main()
//{
//	test t;
//	return 0;
//}

#include <iostream>
using namespace std;

int main(int , char**)
{
	volatile const int foo = 10; //const修饰的常量会放进寄存器里去，所以通过指针对其在内存上的数据进行更改，但是不会影响在寄存器上的数据，所以打印出来的值不相同
	int* pfoo = const_cast<int*>(&foo);
	*pfoo = 20;
	cout << *pfoo << endl;
	cout << foo << endl; //这里打印是在寄存器中拿出了数据，这是编译器对const变量进行优化导致的
	return 0;
}