//牛客KY258 日期累加

#include <iomanip>
#include <iostream>
using namespace std;

class Solution
{
public:
    Solution(int year, int month, int day)
        : _year(year), _month(month), _day(day)
    {
    }

    Solution(const Solution &s)
    {
        _year = s._year;
        _month = s._month;
        _day = s._day;
    }

    [[nodiscard]] bool isLeapYear(const int year) const
    {
        if (year % 400 == 0 || (year % 4 == 0) && (year % 100 != 0))
        {
            return true;
        }

        return false;
    }

    [[nodiscard]] int getMonthDay(const int year, const int month) const
    {
        int monthDays[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (isLeapYear(year) && month == 2)
        {
            return 29;
        }

        return monthDays[month];
    }

    bool operator>(const Solution &s) const
    {
        if (_year > s._year)
        {
            return true;
        }
        else if (_year == s._year && _month > s._month)
        {
            return true;
        }
        else if (_year == s._year && _month == s._month && _day > s._day)
        {
            return true;
        }

        return false;
    }

    bool operator==(const Solution &s) const
    {
        return _year == s._year && _month == s._month && _day == s._day;
    }

    bool operator>=(const Solution &s) const
    {
        return *this == s || *this > s;
    }

    bool operator<(const Solution &s) const
    {
        return !(*this >= s);
    }

    bool operator<=(const Solution &s) const
    {
        return !(*this > s);
    }

    bool operator!=(const Solution &s) const
    {
        return !(*this == s);
    }

    Solution &operator+=(const int days)
    {
        _day += days;
        while (_day > getMonthDay(_year, _month))
        {
            _day -= getMonthDay(_year, _month);
            ++_month;

            if (_month == 13)
            {
                ++_year;
                _month = 1;
            }
        }

        return *this;
    }

    Solution &operator++()
    {
        *this += 1;
        return *this;
    }

    int operator-(const Solution &s) const
    {
        Solution max(*this);
        Solution min(s);

        if (max < min)
        {
            max = s;
            min = *this;
        }

        int count = 0;
        while (min < max)
        {
            ++count;
            ++min;
        }

        return count + 1;
    }

    void datePrint(const int days) const
    {
        Solution s = *this;
        s += days;

        cout << s._year << "-" << setw(2) << setfill('0') << s._month << "-" 
        << setw(2) << setfill('0') << s._day <<endl;
    }
private:
    int _year;
    int _month;
    int _day;
};

int main()
{
    int count;
    int year, month, day, days;
    scanf("%d", &count);
    while(count--)
    {
        cin >> year >> month >> day >> days;
        Solution s(year, month, day);
        s.datePrint(days);
    }
    return 0;
}