#pragma once

#include <unordered_map>
#include <list>
#include <utility>

using namespace std;

//LeetCode 146.LRU缓存
namespace simulation {
    class LRUCache {
    public:
        LRUCache(int capacity)
            : _capacity(capacity)
        {}

        int get(int key) {
            unordered_map<int, list<pair<int, int>>::iterator>::iterator ret = _hash_map.find(key);

            if (ret == _hash_map.end()) { //没找到就说明key不存在
                return -1;
            }
            else { //找到了
                //更新位置
                list<pair<int, int>>::iterator pos = ret->second;
                pair<int, int> data = *pos;

                //先把旧数据删除，再尾插新数据，达到了更新数据并把数据放在尾部(最新数据)的位置
                _lru_list.erase(pos);
                _lru_list.push_back(data);

                _hash_map[key] = --_lru_list.end();

                return data.second;
            }
        }

        void put(int key, int value) {
            unordered_map<int, list<pair<int, int>>::iterator>::iterator ret = _hash_map.find(key);

            if (ret == _hash_map.end()) { // 没有找到，就是要插入
                // 满了就要删除删除数据
                if (isFull()) {
                    int oldest_key = _lru_list.front().first;
                    _lru_list.pop_front();
                    _hash_map.erase(oldest_key);
                }

                //新增数据
                _lru_list.push_back(make_pair(key, value));
                _hash_map[key] = --_lru_list.end();
            }
            else { // 找到了，就是要更新
                list<pair<int, int>>::iterator pos = ret->second;

                //先把旧数据删除，再尾插新数据，达到了更新数据并把数据放在尾部(最新数据)的位置
                _lru_list.erase(pos);
                _lru_list.push_back(make_pair(key, value));

                _hash_map[key] = --_lru_list.end();
            }
        }

        bool isFull() {
            return _lru_list.size() == _capacity;
        }

    private:
        unordered_map<int, list<pair<int, int>>::iterator> _hash_map; // 第二个存储list的迭代器，解决了list的查找不是O(1)的问题
        // 我们把最新的数据存在list的尾部，所以头部就是最少使用的数据
        list<pair<int, int>> _lru_list; // list的头插与头删都是O(1)，只要知道了位置，任意位置的插入删除也都是O(1)，但是查找是O(N)
        const int _capacity;
    };

    /**
     * Your LRUCache object will be instantiated and called as such:
     * LRUCache* obj = new LRUCache(capacity);
     * int param_1 = obj->get(key);
     * obj->put(key,value);
     */
}