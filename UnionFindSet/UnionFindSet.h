#pragma once

#include <stdexcept>
#include <utility>
#include <vector>
using namespace std;

namespace simulation {
	class UnionFindSet {
	private:
		vector<int> _ufs;

	public:
		UnionFindSet(const uint32_t& size)
			: _ufs(size, -1) 
		{}

		int findRoot(int index) {
			if (index >= 0 && index < _ufs.size()) {
				throw invalid_argument("下标越界");
			}

			int root = index;
			while (_ufs[root] >= 0) {
				root = _ufs[root];
			}

			// 路径压缩
			while (_ufs[index] >= 0) {
				int parent = _ufs[index];
				_ufs[index] = root;
				index = parent;
			}

			return root;
		}

		bool Union(const uint32_t& pos1, const uint32_t& pos2) {
			int root1 = findRoot(pos1);
			int root2 = findRoot(pos2);

			if (root1 == root2) { //如果pos1和pos2的根相同，说明他们已经在一个集合中，不需要也不能再进行合并，如果合并就会发生错误
				return false;
			}

			// 因为root1和root2都是根节点，它们所存的数据都是小于0的
			// 所以_ufs[root]数值越小的，代表它所拥有的节点越多
			// 我们这里让root1拥有的节点最多
			if (_ufs[root1] > _ufs[root2]) {
				swap(root1, root2);
			}

			// 让数据量小的向数据量大的去合并，因为被合并的那一方树的节点高度都会+1，所以尽量让数据量小的树高度+1
			_ufs[root1] += _ufs[root2];
			_ufs[root2] = root1;
			return true;
		}

		bool inSet(const uint32_t& pos1, const uint32_t& pos2) {
			return findRoot(pos1) == findRoot(pos2);
		}

		// 计算一共有多少个集合
		int setSize() {
			// 因为一个集合有相同的根，不相同的集合的根不同，且根于根之间没有关系。所以只需要找有多少个根就行，即只需要找有多少个小于0的数
			int size = 0;
			for (const auto& i : _ufs) {
				if (i < 0) {
					++size;
				}
			}
			return size;
		}
	};
}