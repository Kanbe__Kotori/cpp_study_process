#include <iostream>
#include "UnionFindSet.h"

using namespace simulation;

int main() {
	UnionFindSet ufs(10);
	ufs.Union(0, 1);
	ufs.Union(2, 3);
	ufs.Union(3, 4);
	ufs.Union(7, 8);
	ufs.Union(0, 7);
	ufs.Union(1, 7);

	return 0;
}