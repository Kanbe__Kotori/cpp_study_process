#pragma once
#include <utility>
#include <iostream>

using namespace std;

namespace simulation {

	template<class K, size_t M>
	struct BTreeNode {
		// BTree的节点中并不是简单的一个节点存储一个对应的key值，而是一个节点存储多个key值
		// 因为BTree是一棵多叉树，所以它需要存多个子节点的指针
		// 一个节点中应该存储K - 1个key值， K个子节点的指针(关键字的数量永远要比子节点指针的数量要少一个)，其中 ceil(M / 2) <= K <= M，ceil为向上取整
		// 在设计中，我们需要直接开辟满足最大容量的空间，以适应所有的情况
		// 且为了实现方便，存储key和指针的数组还需额外开辟一个空间，这样在节点分裂时更容易操作

		K _keys[M]; // 一个节点最多存储M - 1个关键字，这里多一个位置方便操作
		BTreeNode<K, M>* _subs[M + 1]; // 一个最多存储M个子节点指针，这里多一个位置方便操作
		BTreeNode<K, M>* _parent; // 记录父节点

		size_t _size; // 记录该节点中已经存储了多少个有效关键字

		BTreeNode()
			: _parent(nullptr)
			, _size(0)
		{
			for (int i = 0; i < M; ++i) {
				_keys[i] = K();
				_subs[i] = nullptr;
			}

			_subs[M] = nullptr;
		}
	};

	// 类型模板参数K指的是是存储的数据类型(如果数据存在磁盘上，K就是磁盘上数据的地址)，非类型模板参数M代表的是树的分叉个数，BTree是一棵M叉树而不是一棵二叉树，BTree的根节点至少是二叉的
	// BTree是一棵严格的平衡树，它的所有叶节点都在同一高度。
	// 因为它不像其他的树，其他的树在增加节点时总是增加子节点，这会导致某一棵子树树的高度增加，这样树整体上就不会平衡；
	// 而BTree在扩容分裂时，是增加兄弟节点(不会使树的高度增加)或者是增加根节点(会导致树的高度增加，但是每一棵子树的高度都会增加，不会出现单独某一棵子树高度增加的情况)，这就导致BTree一直使平衡的
	template<class K, size_t M>
	class BTree {
	private:
		using Node = BTreeNode<K, M>;

	private:
		Node* _root;

	public:
		/*
		* @return 如果找到了，Node*是Key所在节点的地址，int是在_keys数组中的下标；
		* @return 如果没找到，Node*就是在查找过程中经过的叶子节点的地址(方便后续的插入操作复用)，int是-1
		*/
		pair<Node*, int> find(const K& key) {
			Node* parent = nullptr;
			Node* cur = _root;
			// 在整棵树中找
			while (cur) {
				int index = 0;

				// 因为节点中使用数组来存储多个key值，我们在查找时需要遍历这些数组
				// 在数组中查找
				while (index < cur->_size) { // 如果找到了就直接返回
					if (key == cur->_keys[index]) {
						return make_pair(cur, index);
					}
					else if (key < cur->_keys[index]) { // 如果key < cur->_keys[index]，就要去_keys[index]位置的左孩子里去找
						break; // 这里退出数组循环，在外面一起处理在树中循环的更新
					}
					else { 
						// key > cur->_keys[index]，说明key既有可能在cur->_keys[index]的左孩子中，也有可能在该数组中index的右边位置，
						// 需要继续遍历，直到满足key < cur->_keys[index]去找左孩子
						// 或者是遍历完了还没找到(这说明有可能key值比该节点中最大的key值还要大)，这样就要去最后一个子节点指针指向的节点中去寻找

						++index;
					}
				}

				// 去孩子里找，如果是从第二个条件里break出来的就会去跳出循环的index的左孩子里找，如果是自然跳出的(第三个条件)就会去最右边(最大)的孩子里去找
				// _keyss[index]的左孩子的下标刚好等于index，而如果是是取最右边孩子，因为最后一次++index了，所以index == cur->_size(_keyss数组最后一个元素的右孩子下标)
				parent = cur;
				cur = cur->_subs[index];
			}

			// 如果树都遍历完了还没找到，说明没有要找的key值

			return make_pair(parent, -1);
		}

		bool insert(const K& key) {
			if (_root == nullptr) { // 没有根节点先创建根节点
				_root = new Node;
				_root->_keys[_root->_size++] = key;

				return true;
			}
			
			// 查找key是否已经存在，存在则返回false，不存在就拿到key值应插入叶子节点的地址
			pair<Node*, int> ret = find(key);
			if (ret.second != -1) {
				return false;
			}

			// 向cur插入child和new_key，不过第一次插入时Node* child = nullptr、K new_key = key
			// 这种插入方法主要是为了后面的不断向上分裂更新准备的
			Node* cur = ret.first;
			Node* child = nullptr;
			K new_key = key;

			// 因为节点在分裂完成后，需要将自己和兄弟连接到父节点上，并且给一个key给父节点。
			// 如果此时父节点刚好容量也满了，那么父节点也需要分裂，以此类推父节点的父节点...到根节点都有可能需要分裂更新，故给一个死循环
			// 在不断向上更新的过程中若遇见不需要继续分裂或者是根节点分裂的情况，就会在插入完成后退出循环，故不是真正的死循环
			while (true) {
				explicitInsert(cur, new_key, child);

				// 插入之后，如果parent满了就需要分裂，没满就结束插入

				if (cur->_size < M) { // 没满
					return true;
				}

				// 满了，需要分裂
				// 需要把一半的关键字和孩子都分给兄弟，且需要分一个关键字给父节点

				size_t mid = M / 2;
				Node* brother = new Node;

				size_t i = mid + 1, j = 0; // i = M / 2 + 1的效果与i = static_cast<size_t>(ceil(M * 1.0 / 2 + 1))的效果相似
				for (; i <= M - 1; ++i) {
					brother->_keys[j] = cur->_keys[i]; // 给关键字
					brother->_subs[j] = cur->_subs[i]; // 给对应关键字的左孩子
					if (brother->_subs[j]/*cur->_subs[i]*/ != nullptr) { // 说明左孩子存在，需要更改孩子的_parent
						brother->_subs[j]->_parent = brother;
					}

					++j;
				}

				// 更改节点的两个_size信息，这里采用消极删除的方式
				brother->_size = j;
				cur->_size -= (j + 1); // 这里的1是要转移给父节点关键字的数量，只是提前减掉了

				// 左孩子都转移完了，需要转移最后一个右孩子
				// 在出循环后i == M，刚好是右孩子的下标
				// 这里只转移孩子，不转移关键字，因为关键字已经转移完了且再进行访问会越界
				brother->_subs[j] = cur->_subs[i];
				if (brother->_subs[j]/*cur->_subs[i]*/ != nullptr) {
					brother->_subs[j]->_parent = brother;
				}

				// 分裂完成后，需要将一个关键字给父节点，并将兄弟与父节点连接起来
				K mid_key = cur->_keys[mid];

				if (cur == _root) { // 根节点分裂后就不再是根节点了，需要再产生一个根节点
					_root = new Node;
					_root->_keys[_root->_size++] = mid_key;
					_root->_subs[0] = cur;
					_root->_subs[1] = brother;

					cur->_parent = brother->_parent = _root;

					return true; // 根节点分裂后就不会再继续向上分裂了
				}

				// 继续向上分裂
				cur = cur->_parent;
				new_key = mid_key;
				child = brother;
			}
		}

		void inOrder() const {
			_inOrder(_root);
		}

	private:
		void explicitInsert(Node* aim, const K& key, Node* child) {
			// 实现类似于(单趟的插入排序)的查找来找到key值应插入的位置
			int end = aim->_size - 1;
			while (end >= 0) {
				// _keys中是从小到大排序的
				// 通过类插入排序的方法挪动数据并找到key插入的位置
				// 并且因为只有在第一次插入时才是在叶子节点插入(叶子节点的_subs中全为nullptr)，此时可以不挪动子节点指针
				// 但是后续的向上分裂插入的对象不再是叶子节点，其是有子节点指针的，为了不破坏key和指针之间的关系，还需挪动子节点指针
				if (key < aim->_keys[end]) {
					aim->_keys[end + 1] = aim->_keys[end]; // 挪动key值
					aim->_subs[end + 2] = aim->_subs[end + 1]; // 挪动key值对应的右孩子，如果是挪动左孩子的话，最右边的右孩子会被覆盖，数据应从最后一个开始挪动
					--end;
				}
				else { // key >= aim->_keys[end], 此时数据不需要挪动，直接在end的后面插入
					break; // 退出循环一起处理，因为循环结束的条件有两个，一个是从这里break，一个是不满足while (end >= 0)，这两种情况的处理方法是一致的
				}
			}

			aim->_keys[end + 1] = key;
			// 挪动右孩子的原因不止上面一个，还因为在对cur的父节点插入兄弟节点时，因为兄弟节点的key值都比cur的key值要大，且它们两个的父亲相同
			// 所以cur指针的位置一定要在brother指针的左边，所以brother指针一定不可能是在最左边的位置(最坏的情况是cur在最左边，brother在左边第二个)，所以移动右孩子
			aim->_subs[end + 2] = child; 

			if (child) {
				child->_parent = aim;
			}

			++aim->_size;
		}

		void _inOrder(Node* root) const {
			if (root == nullptr) {
				return;
			}

			int i = 0;
			for (; i < root->_size; ++i) {
				_inOrder(root->_subs[i]); // 先去遍历每个关键字的左子树，一个关键字后面的那个关键字的左子树就是该关键字的右子树，这样只会剩下最后一个右孩子(最右孩子)没被访问到
				cout << root->_keys[i] << " ";
			}

			_inOrder(root->_subs[i]); // 访问最右孩子
			cout << endl;
		}
	};
}