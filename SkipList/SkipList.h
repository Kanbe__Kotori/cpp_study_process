#pragma once
#include <vector>
#include <random>
#include <chrono>
using namespace std;


//LeetCode1206.设计跳表
namespace simulation {
    class Skiplist {
    private:
        struct SkiplistNode {
            int _val;
            vector<SkiplistNode*> _next_vec;

            SkiplistNode(const int& val, const int& level)
                : _val(val) // -1代表无效数据
                , _next_vec(level, nullptr)
            {}
        };

    private:
        using Node = SkiplistNode;

    public:
        Skiplist() {
            _head = new Node(-1, 1); // 从1层开始，有效节点的最大高度增加，头节点的高度才会增加到对应高度
        }

        bool search(int target) {
            int level = _head->_next_vec.size() - 1; // 记录层数的下标
            Node* cur = _head;
            while (level >= 0) {
                if (cur->_next_vec[level] != nullptr && target > cur->_next_vec[level]->_val) {
                    cur = cur->_next_vec[level];
                }
                else if (cur->_next_vec[level] == nullptr || target < cur->_next_vec[level]->_val) {
                    --level;
                }
                else {
                    return true;
                }
            }

            return false;
        }

        void add(int num) {
            vector<Node*> prev_node = getPrevNode(num);
            int level = getRandomLevel();

            if (level > _head->_next_vec.size()) { // 如果新增节点的层高比头节点的层高要高，就给头节点增高
                _head->_next_vec.resize(level, nullptr);
                // 除了要给头节点增高外，还因为prev_node的高度是根据扩容前的头节点的高度设定的，所以prev_node也要扩容
                // 否则new_node在连接多出来的高度的节点时会非法访问内存
                prev_node.resize(level, _head);
            }

            Node* new_node = new Node(num, level);

            for (int i = 0; i < level; ++i) {
                new_node->_next_vec[i] = prev_node[i]->_next_vec[i];
                prev_node[i]->_next_vec[i] = new_node;
            }
        }

        bool erase(int num) {
            vector<Node*> prev_node = getPrevNode(num);
            // 以num进行搜索，其第一层的前一个节点的next节点的val值如果不为num的话，就说明没搜索到num，就说明num不存在，返回false
            // 看第一层的原因是，每个节点至少会有一层
            if (prev_node[0]->_next_vec[0] == nullptr || prev_node[0]->_next_vec[0]->_val != num) {
                return false;
            }

            Node* delete_node = prev_node[0]->_next_vec[0];
            for (int i = 0; i < delete_node->_next_vec.size(); ++i) {
                prev_node[i]->_next_vec[i] = delete_node->_next_vec[i];
            }

            delete delete_node;

            // 检查头节点，看是否有因为删除节点而多出来的一层(next直接指向nullptr)
            int level = _head->_next_vec.size() - 1;
            while (level >= 0) {
                if (_head->_next_vec[level] != nullptr) {
                    break;
                }
                else {
                    --level;
                }
            }

            _head->_next_vec.resize(level + 1);
            return true;
        }


        ~Skiplist() {
            Node* cur = _head->_next_vec[0];
            while (cur) {
                Node* next = cur->_next_vec[0];
                delete cur;
                cur = next;
            }

            delete _head;
        }

    private:
        inline int getRandomLevel() {
            // 这里用static修饰是为了让这两个对象在整个进程中就只创建一次，避免额外开销
            static std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
            static std::uniform_real_distribution<double> distribution(0.0, 1.0);

            int level = 1;
            while (distribution(generator) <= _P && level < _Max_Level) {
                ++level;
            }

            return level;
        }

        inline vector<Node*> getPrevNode(const int& target) {
            int level = _head->_next_vec.size() - 1;
            Node* cur = _head;
            vector<Node*> prev_node(level + 1, _head); // 记录target节点各层的前一个节点，初始值设定为_head

            while (level >= 0) {
                if (cur->_next_vec[level] != nullptr && target > cur->_next_vec[level]->_val) {
                    cur = cur->_next_vec[level];
                }
                else { //cur->_next_vec[level] == nullptr || target < cur->_next_vec[level]->_val
                    prev_node[level] = cur;
                    --level;
                }
            }

            return prev_node;
        }

    private:
        Node* _head;
        const int _Max_Level = 32;
        const double _P = 0.25;
    };
}