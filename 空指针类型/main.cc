#include <iostream>
using namespace std;

void func(int) {
	cout << "func(int)" << endl;
}

void func(int*) {
	cout << "func(int*)" << endl;
}

void func(char) {
	cout << "func(char)" << endl;
}

int main() {
	func(0);
	func(NULL);
	func(nullptr);
	return 0;
}