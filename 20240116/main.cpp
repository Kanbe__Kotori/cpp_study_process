#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <list>
#include <algorithm>
using namespace std;

//int main()
//{
//	//构造空字符串
//	string s1;
//	cout << "s1的内容为%" << s1 << "%" << endl;
//
//	return 0;
//}

//int main()
//{
//	//用C语言的字符串来构建string
//	//可以是数组 用数组名构建
//	char charArray[] = "Hello C++ string!";
//	string s1(charArray);
//
//	//也可以用字符串常量构建
//	string s2("hello c++ string!");
//
//	cout << "s1的内容为" << s1 << endl;
//	cout << "s2的内容为" << s2 << endl;
//
//	return 0;
//}

//int main()
//{
//	//用n个相同字符构造字符串
//	string s1(10, 'a');
//	string s2(5, 'b');
//	cout << "s1的内容为" << s1 << endl;
//	cout << "s2的内容为" << s2 << endl;
//
//	return 0;
//}

//int main()
//{
//	//用C类型字符串构建string
//	string s1("Hello string!");
//
//	//拷贝构造
//	string s2(s1);
//	
//	cout << "s1的内容为" << s1 << endl;
//	cout << "s2的内容为" << s2 << endl;
//	return 0;
//}

//int main()
//{
//	//用字符串（C类型字符串或string类）的前n个字符构造
//	char charArray[] = "Hello World!";
//	string str("hello world");
//
//	//用C语言中字符串的前n个字符构建来字符串
//	string s1(charArray, 5);
//
//	//注意不能超过字符串的有效长度
//	//string s2(charArray, 114514);
//
//	//用string类型的字串来构建字符串  可以从模板的任意位置开始 拷贝任意个字符
//	//string(const string & str, size_t pos, size_t len = npos)
//	//str为作为模板的string字符串  pos为构建(拷贝)开始的位置  len为需要构建(拷贝的长度)  
//	//npos == -1 但因为size_t为无符号整数 所以表示为非常大的整数
//	//最多拷贝原字符串的长度个字符
//	string s3(str, 0, 5);
//	string s4(str, 6, 114514);
//
//	cout << "s1的内容为" << s1 << endl;
//	//cout << "s2的内容为" << s2 << endl;
//	cout << "s3的内容为" << s3 << endl;
//	cout << "s4的内容为" << s4 << endl;
//	return 0;
//}

//int main()
//{
//	string str("Hello World!");
//	char charArray[] = "Hello C++!";
//
//	//用重载后的赋值运算符构造字符串
//	//本质是调用了拷贝构造
//	string s1 = str;
//	string s2 = charArray;
//	string s3 = "Hello C!";
//
//	cout << "s1的内容为" << s1 << endl;
//	cout << "s2的内容为" << s2 << endl;
//	cout << "s3的内容为" << s3 << endl;
//
//	//这里是将s1的字符串拷贝给s2和s3
//	//将s2和s3原来的字符串替换
//	s2 = s1;
//	s3 = s1;
//	cout << endl;
//	cout << "s1的内容为" << s1 << endl;
//	cout << "s2的内容为" << s2 << endl;
//	cout << "s3的内容为" << s3 << endl;
//	return 0;
//}

//int main()
//{
//	//中文字符 一个占两个字节 这里的'！'也是中文的感叹号
//	string s1("你好！");
//	string s2("Hello and thanks!");
//
//	//size()和length()的效果相同 只不过size()是所有容器所共有的
//	//所以size()用的更多
//	cout << s1.size() << endl;
//	cout << s1.length() << endl;
//	cout << s2.size() << endl;
//	cout << s2.length() << endl;
//}

//int main()
//{
//	//创建空字符串
//	string s("");
//
//	//给空字符串扩容
//	while (s.size() <= 200)
//	{
//		cout << "s的长度为 " << s.size() << endl;
//		cout << "s的容量为 " << s.capacity() << endl;
//		cout << endl;
//
//		//用法见append()
//		s.append(50, 'a');
//	}
//	return 0;
//}

//int main()
//{
//	string s1 = "我不是空串！";
//	string s2;
//
//	//检查一个串是否为空串
//	cout << s1.empty() << endl;
//	cout << s2.empty() << endl;
//
//	return 0;
//}

//int main()
//{
//	//清空字符串的内容 让其成为空串
//
//	string s = "这里是字符串";
//	cout << "clear前s的内容为" << "#" << s << "#" << endl;
//	cout << "clear前s的长度为" << s.size() << endl;
//	cout << "clear前s的容量为" << s.capacity() << endl;
//
//	s.clear();
//	cout << endl;
//	cout << "clear后s的内容为" << "#" << s << "#" << endl;
//	cout << "clear后s的长度为" << s.size() << endl;
//	cout << "clear后s的容量为" << s.capacity() << endl;
//	return 0;
//}

//int main()
//{
//	//改变字符串的容量
//	string str("你猜猜我的capacity是多少");
//
//	//改变前
//	cout << "str的内容为 " << str << endl;
//	cout << "str的长度为" << str.size() << endl;
//	cout << "str的容量为" << str.capacity() << endl;
//	cout << endl;
//
//	//如果申请的容量比原容量要小 这个请求就不会被接受
//	str.reserve(4);
//	cout << "str的内容为 " << str << endl;
//	cout << "str的长度为" << str.size() << endl;
//	cout << "str的容量为" << str.capacity() << endl;
//	cout << endl;
//
//	//如果申请的容量比原容量大 那么容量就会按照设定的变化规律增大到指定容量(或更大)
//	//但是字符串的长度和内容并不会改变
//	str.reserve(200);
//	cout << "str的内容为 " << str << endl;
//	cout << "str的长度为" << str.size() << endl;
//	cout << "str的容量为" << str.capacity() << endl;
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	//改变字符串的长度
//	string str1 = "我怎么天天被改？";
//	string str2(str1);
//	string str3 = str2;
//
//	//缩小字符串的长度
//	cout << "修改前：" << endl;
//	cout << "str1的内容为 " << str1 << endl;
//	cout << "str1的长度为" << str1.size() << endl;
//	cout << "str1的容量为" << str1.capacity() << endl;
//	cout << endl;
//
//	str1.resize(6);
//	cout << "修改后：" << endl;
//	cout << "str1的内容为 " << str1 << endl;
//	cout << "str1的长度为" << str1.size() << endl;
//	cout << "str1的容量为" << str1.capacity() << endl;
//	cout << endl;
//
//	//扩大字符串的长度
//	cout << "修改前：" << endl;
//	cout << "str2的内容为 " << "#" << str2 << "#" << endl;
//	cout << "str2的长度为" << str2.size() << endl;
//	cout << "str2的容量为" << str2.capacity() << endl;
//	cout << endl;
//
//	//不指定字符c
//	str2.resize(32);
//	cout << "修改后：" << endl;
//	cout << "str2的内容为 " << "#" << str2 << "#" << endl;
//	cout << "str2的长度为" << str2.size() << endl;
//	cout << "str2的容量为" << str2.capacity() << endl;
//	cout << endl;
//
//	cout << "修改前：" << endl;
//	cout << "str3的内容为 " << "#" << str3 << "#" << endl;
//	cout << "str3的长度为" << str3.size() << endl;
//	cout << "str3的容量为" << str3.capacity() << endl;
//	cout << endl;
//
//	//指定字符c
//	str3.resize(32, 'x');
//	cout << "修改后：" << endl;
//	cout << "str3的内容为 " << "#" << str3 << "#" << endl;
//	cout << "str3的长度为" << str3.size() << endl;
//	cout << "str3的容量为" << str3.capacity() << endl;
//	cout << endl;
//	
//	return 0;
//}

//int main()
//{
//	//获取字符串的最大长度  没什么用......
//	string s1;
//	string s2 = "boring";
//
//	cout << s1.max_size() << endl;
//	cout << s2.max_size() << endl;
//	return 0;
//}

//int main()
//{
//	string s1("123456789");
//
//	//const修饰的字符串不能修改
//	const string s2(s1);
//
//	for (int i = 0; i < s1.size(); ++i)
//	{
//		s1[i] -= 1;
//	}
//
//	for (int i = 0; i < s1.size(); ++i)
//	{
//		cout << s1[i] << " ";
//	}
//	cout << endl;
//
//	for (int i = 0; i < s2.size(); ++i)
//	{
//		cout << s2[i] << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	//尾插字符
//	string s = "Hell";
//	cout << "s尾插前的内容为 " << s << endl;
//
//	s.push_back('o');
//	cout << "s尾插后的内容为 " << s << endl;
//
//	return 0;
//}

//int main()
//{
//	//尾插字符串
//
//	//string& append (const string& str) 尾插string类字符串
//	string s1("Hello ");
//	string s2("World!");
//	cout << s1.append(s2) << endl;
//
//	//string& append(const string & str, size_t subpos, size_t sublen) 尾插string类字符串的一部分
//	string s3("I have ");
//	string s4("There are 100 apples!");
//	cout << s3.append(s4, 10, string::npos) << endl;
//
//	//string& append (const char* s) 尾插C类型的字符串
//	string s5("I have ");
//	cout << s5.append("100 yuan!") << endl;
//
//	//string& append (const char* s, size_t n) 尾插C类型字符串的一部分
//	string s6("");
//	char charArray[] = "I love your money!";
//	cout << s6.append(charArray, 10) << endl;
//
//	//string& append (size_t n, char c) 尾插n个相同字符
//	string s7("???");
//	cout << s7.append(3, '!') << endl;
//
//	return 0;
//}

//int main()
//{
//	//+=重载
//	string s1("Hello ");
//	string s2(s1);
//	string s3(s1);
//	string s4("World!");
//	char chars[] = "World!";
//
//	cout << (s1 += s4) << endl;
//	cout << (s2 += chars) << endl;
//	cout << (s3 += '!') << endl;
//	return 0;
//}

//int main()
//{
//	//在字符串任意位置插入
//	string s1("World!");
//	string s2("Hello ");
//	cout << s1.insert(0, s2) << endl;
//	return 0;
//}

//int main()
//{
//	//在字符串任意位置删除
//	string s1 = "I love your money!";
//	string s2 = "I dislike you!";
//
//	//从指定位置开始全删
//	cout << s1.erase(10) << endl;
//
//	//从指定位置开始删n个 当n为1时 可以起到删一个字符的效果
//	cout << s2.erase(2, 3) << endl;
//	return 0;
//}

//int main()
//{
//	string s1 = "i am s1";
//	string s2 = "i am s2";
//
//	cout << "s1交换前为 " << s1 << endl;
//	cout << "s2交换前为 " << s2 << endl;
//	cout << endl;
//
//	s1.swap(s2);
//	cout << "s1交换后为 " << s1 << endl;
//	cout << "s2交换后为 " << s2 << endl;
//	return 0;
//}

//int main()
//{
//	string s("I am tired!");
//	s.pop_back();
//	cout << s << endl;
//	return 0;
//}

//int main()
//{
//	string s = "I wanna be a C-string!";
//	const char* chars = s.c_str();
//	printf("%s\n", chars);
//	return 0;
//}

//int main()
//{
//	string s1("Hello World!");
//	string s2("World");
//	string s3("world");
//
//	//find()是不忽略大小写的
//	cout << "s2在s1的" << s1.find(s2) << "处出现" << endl;
//	//如果没找到 find()会返回npos(-1) 但因为返回值类型为size_t 所以最后会变成一个非常大的整数
//	cout << "s3在s1的" << s1.find(s3) << "处出现" << endl;
//
//	//当查找一个字符时 字符即可以单引号也可以用双引号
//	size_t pos = s1.find("o");
//	cout << "s1中o第一次出现在" << pos << endl;
//	cout << "s1中o第二次出现在" << s1.find('o',pos + 1) << endl;
//
//	return 0;
//}

//int main()
//{
//	string s1("Hello World!");
//
//	size_t pos = s1.rfind("o");
//	cout << "s1中o倒数第一次出现在" << pos << endl;
//	cout << "s1中o倒数第二次出现在" << s1.rfind('o',pos - 1) << endl;
//	return 0;
//}

//int main()
//{
//	//截取字串 分割网址
//	string website("https://legacy.cplusplus.com/reference/string/string/substr/");
//
//	//分割协议
//	size_t pos1 = website.find(':');
//	string part1 = website.substr(0, pos1);
//
//	//分割域名
//	size_t pos2 = website.find('/', pos1 + 3);
//	string part2 = website.substr(pos1 + 3, pos2 - (pos1 + 3));
//
//	//分割路径
//	string part3 = website.substr(pos2 + 1, string::npos);
//
//	cout << part1 << endl;
//	cout << part2 << endl;
//	cout << part3 << endl;
//
//	return 0;
//}

//int main()
//{
//	string str("Hello World!");
//	char chars[20];
//
//	//拷贝所有内容
//	//第一个参数为拷贝到的目的地 第二个参数为拷贝的长度 第三个参数为从来源的哪个位置开始拷贝
//	size_t num = str.copy(chars, str.size(), 0);
//
//	//在拷贝后的字符串后补充'\0'
//	chars[num] = '\0';
//
//
//	cout << "str的长度为" << str.size() << endl;
//	cout << "一共拷贝了" << num << "个字符" << endl;
//	printf("拷贝后的内容为 %s\n", chars);
//	return 0;
//}

//int main()
//{
//	string str("Hello World!");
//	string::iterator hit = str.begin();
//	string::iterator bit = str.end();
//	auto prevBit = str.end() - 1;
//
//	//迭代器的使用很像指针
//	//但迭代器并不是指针!!!
//	cout << *hit << endl;
//	cout << *prevBit << endl;
//
//	return 0;
//}

//int main()
//{
//	string str("Hello World!");
//	string::reverse_iterator rbit = str.rbegin();
//
//	while (rbit != str.rend())
//	{
//		cout << *rbit << " ";
//
//		//注意：这里的rbegin想要到达rend依旧使用++而不是--！！！
//		++rbit;
//	}
//	return 0;
//}

//int main()
//{
//	const string cstr("Hello? Hello!");
//	string::const_iterator cbit = cstr.begin();
//	//string::iterator cbit = cstr.begin();
//	while (cbit != cstr.end())
//	{
//		cout << *cbit;
//		++cbit;
//	}
//	
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	return 0;
//}

//int main()
//{
//	//创建int类型的链表
//	list<int> ilist;
//
//	//尾插数据
//	ilist.push_back(1);
//	ilist.push_back(2);
//	ilist.push_back(3);
//	ilist.push_back(4);
//	ilist.push_back(5);
//
//	//遍历链表
//	list<int>::iterator it = ilist.begin();
//	while (it != ilist.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	string s("abcdedg");
//	cout << "s翻转之前的内容为 " << s << endl;
//
//	//翻转字符串
//	reverse(s.begin(), s.end());
//
//	cout << "s翻转之后的内容为 " << s << endl;
//
//	return 0;
//}

//int main()
//{
//	cout << string::npos << endl;
//	return 0;
//}

//int main()
//{
//	string s1("Hello ");
//	string s2("World!");
//	string s3 = s1 + s2;
//	cout << s3 << endl;
//	return 0;
//}

//int main()
//{
//	string s1 = "Hello World!";
//	string s2("hello World!");
//	cout << (s1 == "Hello World!") << endl;
//
//	//H的ASCII码为72 h的ASCII码为104
//	//所以s1 < s2
//	cout << (s1 > s2) << endl;
//
//	return 0;
//}

//int main()
//{
//	string str;
//
//	//注：cin的读取到token(空格或换行)结束！
//	cin >> str;
//	cout << str << endl;
//	return 0;
//}

//int main()
//{
//	string str;
//	getline(cin, str);
//	cout << str << endl;
//
//	return 0;
//}

//int main()
//{
//	string s1("1111122222");
//	string s2("520.1314");
//
//	int i = stoi(s1);
//	double d = stod(s2);
//	return 0;
//}

//int main()
//{
//	int i = 114514;
//	double d = 123.456;
//	string s1 = to_string(i);
//	string s2 = to_string(d);
//	return 0;
//}

//int main()
//{
//	string s("1234");
//	for (int i = 0; i < s.size(); ++i)
//	{
//		//这里的每一个元素的类型都为char类型
//		//所以每一项 *= 2并不会等于字面值的两倍 而是ASCII码 * 2
//		s[i] *= 2;
//	}
//
//	for (int i = 0; i < s.size(); ++i)
//	{
//		cout << s[i] << " ";
//	}
//
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	string s("abcde");
//	string::iterator it = s.begin();
//	while (it != s.end())
//	{
//		*it += 1;
//		cout << *it << " ";
//		++it;
//	}
//	return 0;
//}

int main()
{
	string s("abcdefg");
	for (auto& k : s)
	{
		++k;
	}

	for (auto& k : s)
	{
		cout << k << " ";
	}
	cout << endl;
	return 0;
}