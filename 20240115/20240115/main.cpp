#include <iostream>
using namespace std;

template <class T>
void Swap(T& x1, T& x2)
{
	T temp = x1;
	x1 = x2;
	x2 = temp;
}

template <class T>
T Add(const T& x1, const T& x2)
{
	return x1 + x2;
}

int main()
{
	int i1 = 1;
	int i2 = 2;
	double d1 = 1.1;
	double d2 = 2.2;
	char c1 = 'a';
	char c2 = 'b';
	//Swap(i1, i2);
	//Swap(d1, d2);
	//Swap(c1, c2);
	//cout << i1 << " " << i2 << endl;
	//cout << d1 << " " << d2 << endl;
	//cout << c1 << " " << c2 << endl;
	//Swap(i1, d2);
	Add<int>(i1, d2);
	return 0;
}