//TODO: LeetCode232 用栈实现队列

class MyQueue {
public:
    MyQueue() {}

    void push(int x) { _pushStack.push(x); }

    int pop() {
        if (_popStack.empty()) {
            while (!_pushStack.empty()) {
                int top = _pushStack.top();
                _popStack.push(top);
                _pushStack.pop();
            }
        }
        int top = _popStack.top();
        _popStack.pop();
        return top;
    }

    int peek() {
        if (_popStack.empty()) {
            while (!_pushStack.empty()) {
                int top = _pushStack.top();
                _popStack.push(top);
                _pushStack.pop();
            }
        }
        return _popStack.top();
    }

    bool empty() {
        return _popStack.empty() && _pushStack.empty();
    }

private:
    stack<int> _pushStack;
    stack<int> _popStack;
};