#pragma once
namespace simulation {

	//仿函数
	template<class T>
	struct less {
		bool operator() (const T& val1, const T& val2) const
		{
			return val1 < val2;
		}
	};

	template<class T>
	struct greater {
		bool operator() (const T& val1, const T& val2) const
		{
			return val1 > val2;
		}
	};

	template<class T, class Container = vector<T>, class Compare = simulation::less<T>>
	//默认建大堆
	class priority_queue {
	private:
		Container _con;
	public:
		priority_queue() {}

		template<class InputIterator>
		priority_queue(InputIterator first, InputIterator last)
			: _con(first, last) //调用了_con对应容器的迭代器构造
		{
			//建堆
			for (int i = (_con.size() - 2) / 2; i >= 0; --i)
			{
				adjustDown(i);
			}
		}

		bool empty() const
		{
			return _con.empty();
		}

		size_t size() const
		{
			return _con.size();
		}

		void push(const T& val)
		{
			_con.push_back(val);
			adjustUp(_con.size() - 1);
		}

		void pop()
		{
			std::swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			adjustDown(0);
		}

		const T& top() const
		{
			return _con[0];
		}
	private:
		void adjustUp(size_t child)
		{
			while (child > 0)
			{
				size_t parent = (child - 1) / 2;
				if (Compare()(_con[parent], _con[child]))
				{
					std::swap(_con[parent], _con[child]);
					child = parent;
				}
				else
				{
					break;
				}
			}
		}

		void adjustDown(size_t parent)
		{
			size_t aimChild = parent * 2 + 1;
			while (aimChild < _con.size())
			{
				aimChild = ((aimChild + 1 < _con.size()) && (Compare()(_con[aimChild], _con[aimChild + 1])))
					? aimChild + 1
					: aimChild;
				if (Compare()(_con[parent], _con[aimChild]))
				{
					std::swap(_con[parent], _con[aimChild]);
					parent = aimChild;
					aimChild = parent * 2 + 1;
				}
				else
				{
					break;
				}
			}
		}
	};
}