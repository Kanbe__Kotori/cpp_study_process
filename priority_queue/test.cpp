#include <vector>
#include <iostream>
using namespace std;

#include "priority_queue.h"
using namespace simulation;

int main()
{
	priority_queue<int, vector<int>, simulation::greater<int>> heap;
	heap.push(1);
	heap.push(4);
	heap.push(6);
	heap.push(7);
	heap.push(1);
	heap.push(3);
	heap.push(0);
	heap.push(-1);
	heap.push(323);
	heap.push(5);
	heap.push(98);
	heap.push(65);
	heap.push(123);

	while (!heap.empty())
	{
		cout << heap.top() << " ";
		heap.pop();
	}
	cout << endl;
	return 0;
}