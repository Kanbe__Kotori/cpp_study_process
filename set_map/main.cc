#include "map.hpp"

using namespace simulation;

int main()
{
	int array[] = { 24,64,744,85,93,974,643,53,98,34 };
	map<int, int> m;

	for (const auto& i : array)
	{
		m.insert(std::make_pair(i, i));
	}

	m.inOrder();
	return 0;
}