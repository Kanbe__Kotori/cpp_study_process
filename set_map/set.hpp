#pragma once

#include "RBTree.hpp"

namespace simulation {
	template<class K>
	class set {
	private:
		struct SetKeyOfData {
			const K& operator() (const K& key) const
			{
				return key;
			}
		};

		RBTree<K, K, SetKeyOfData> _RBTree;

	public:
		typedef typename RBTree<K, K, SetKeyOfData>::iterator iterator;
		typedef typename RBTree<K, K, SetKeyOfData>::const_iterator const_iterator;
		
		std::pair<iterator, bool> insert(const K& key)
		{
			return _RBTree.insert(key);
		}

		iterator begin()
		{
			return _RBTree.begin();
		}

		const_iterator begin() const
		{
			return _RBTree.begin();
		}

		iterator end()
		{
			return _RBTree.end();
		}

		const_iterator end() const
		{
			return _RBTree.end();
		}
	};
}