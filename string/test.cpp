#include "string.h"
using namespace simulation;
using std::cout;
using std::endl;
using std::cin;

//int main()
//{
//	size_t n = -1;
//	std::cout << n + 1 << std::endl;
//	return 0;
//}

//int main()
//{
//	string s1("Hello world");
//	cout << s1 << endl;
//	cin >> s1;
//	cout << s1 << endl;
//	return 0;
//}

void test1()
{
	string s1("Hello world");
	cout << s1 << endl;
	cin >> s1;
	cout << s1 << endl;
}

void test2()
{
	string s1;
	s1 += "Hello";
	s1.append(" World");
	s1.push_back('!');
	for (auto& i : s1)
	{
		cout << i;
	}

	cout << endl;
}

void test3()
{
	string s1;
	s1.append("Hello");
	s1.erase(0, 3);
	s1.push_back('?');
	cout << s1 << endl;
}

void test4()
{
	string s1;
	string s2("Hello ");
	s1.append("World!");
	s2.append(s1);
	cout << s1 << endl << s2 << endl;
	cout << s2.c_str() << endl;
}

void test5()
{
	string s1("Hello");
	string s2("Hello");
	string s3("Hello");
	s1.insert(s1.size(), 5, 'c');
	s2.insert(s2.size(), "World!");
	s3.insert(s3.size(), s2);
	string s4;
	s4.insert(0, "abcdefg");
	cout << s1 << endl << s2 << endl << s3 << endl << s4 << endl;
}

void test6()
{
	string s1("Hello World!");
	string s2(s1);
	s1.erase(0, string::npos);
	s2.erase(0, 5);
	cout << s1 << endl << s2 << endl;
}

void test7()
{
	string s("hello world!");
	cout << s.substr(0, string::npos) << endl << s.substr(6, 5) << endl;
}

void test8()
{
	string s1("Hello");
	string s2(s1);
	string s3(s1);
	s1.resize(2);
	s2.resize(8, 'a');
	s3.resize(10);
	cout << s1 << endl << s2 << endl << s3 << endl;
}

void test9()
{
	string s1;
	string s2;
	cin >> s1;
	getline(cin, s2);
	cout << s1 << endl << s2 << endl;
	cout << s1.empty() << endl;
}

//int main()
//{
//	//test3();
//	//test4();
//	//test5();
//	//test6();
//	//test7();
//	//test8();
//	test9();
//	return 0;
//}

//int main()
//{
//	cout << std::string::npos << endl;
//	cout << simulation::string::npos << endl;
//	return 0;
//}

//int main()
//{
//	string s1("abcd");
//	s1.pop_back();
//	cout << s1 << endl;
//	return 0;
//}