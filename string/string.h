#pragma once
#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <algorithm>
#include <string>
#include <cassert>

namespace simulation {
	class string {
	public:
		/*
		* 构造函数
		*/

		//默认构造
		string();
		//C类型字符串构造
		string(const char* s);
		//深度拷贝构造
		string(const string& str);

		/*
		* 析构函数
		*/

		~string();

		/*
		* 迭代器
		*/

		typedef char* iterator;
		typedef const char* const_iterator;

		//可读可写
		iterator begin();
		iterator end();
		//可读不可写
		const_iterator begin() const;
		const_iterator end() const;

		/*
		* 容量操作
		*/

		size_t size() const;
		size_t length() const;
		size_t capacity() const;
		size_t max_size() const;

		//清空字符串有效字符
		void clear();

		//判断字符串是否为空
		bool empty() const;

		//申请扩容 只会改字符串容量而不会改内容和长度
		void reserve(size_t n = 0);

		//改变字符串的长度 一般不会改变字符串的容量
		void resize(size_t n, char c = '\0');



		/*
		* 元素访问
		*/

		char& operator[](size_t pos);
		const char& operator[](size_t pos) const;

		/*
		* 修改操作
		*/

		void push_back(char c);
		string& append(const string& str);
		string& append(const char* s);
		string& operator+=(const string& str);
		string& operator+=(const char* s);
		string& operator+=(char c);
		void swap(string& str);
		void pop_back();
		string& insert(size_t pos, const string& str);
		string& insert(size_t pos, const char* s);
		string& insert(size_t pos, size_t n, char c);
		string& erase(size_t pos, size_t len = string::npos);
		string& operator=(const string& str);


		/*
		* 字符串操作
		*/

		const char* c_str() const;
		size_t find(char c, size_t pos = 0) const;
		size_t find(const string& str, size_t pos = 0) const;
		size_t find(const char* s, size_t pos = 0) const;
		string substr(size_t pos = 0, size_t len = string::npos) const;

		/*
		* 非成员函数重载
		* 这里我写错了 我写成了成员函数
		*/
		bool operator==(const string& str) const;
		bool operator!=(const string& str) const;
		bool operator>(const string& str) const;
		bool operator>=(const string& str) const;
		bool operator<=(const string& str) const;
		bool operator<(const string& str) const;


		/*
		* 成员常量
		*/

		//只有const修饰的静态成员变量才能直接赋初值
		//否则是要在类外面赋值的 因为类里面只是声明 外面才是定义
		const static size_t npos = -1;

	private:
		//为了契合C-style-string 所以会在字符串后面跟一个'\0' 但是这个'\0'并不会计入size和capacity
		//而是偷偷的开一个额外空间来存储'\0'
		char*  _str;
		size_t _size;
		size_t _capacity;
	};
}

std::ostream& operator<<(std::ostream& os, const simulation::string& str);
std::istream& operator>>(std::istream& in, simulation::string& str);
std::istream& getline(std::istream& in, simulation::string& str);