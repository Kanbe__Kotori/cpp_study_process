#include "string.h"
using namespace simulation;

const char* string::c_str() const
{
	return _str;
}

size_t string::size() const
{
	return _size;
}

size_t string::length() const
{
	return _size;
}

size_t string::capacity() const
{
	return _capacity;
}

bool string::empty() const
{
	//当字符串为空时 _size == 0 否则不为0
	return _size == 0;
}

string::iterator string::begin()
{
	//begin指向字符串的第一个字符 而_str也指向字符串的第一个字符
	return _str;
}

string::const_iterator string::begin() const
{
	return _str;
}

string::iterator string::end()
{
	//end指向字符串最后一个字符的后面一个位置 最后一个位置的下标的size - 1 所以end的下标为size
	//所以end可以看作&_str[size] -> &*(_str + size) -> _str + size
	return _str + _size;
}

string::const_iterator string::end() const
{
	return _str + _size;
}

string::string()
	: _str(new char[1])
	, _size(0)
	, _capacity(0)
{
	_str[0] = '\0';
}

string::string(const char* s)
	//strlen只会计算C类型字符串的有效长度(不计入'\0')
	//额外开一个空间给'\0'
	: _str(new char[std::strlen(s) + 1])

	//'\0'的空间并不计入_size和_capacity中 所以就按照s的长度来初始化
	, _size(strlen(s))
	, _capacity(_size)
{
	//s是C类型字符串 自带'\0'
	//strcpy会先自行拷贝一个'\0'然后再结束拷贝
	std::strcpy(_str, s);
}

//深拷贝的传统写法
//string::string(const string& str)
//	//注意这里是str._capacity + 1 而不是str._size + 1 因为size是字符串的当前长度 而capacity是字符串的最大长度
//	//我们要开够字符串的极限长度 并且预留一个'\0'的空间
//	: _str(new char[str._capacity + 1])
//	, _size(str._size)
//	, _capacity(str._capacity)
//{
//	assert(str._str);
//
//	std::strcpy(_str, str._str);
//}

//深拷贝的现代写法
string::string(const string& str)
	: _str(nullptr)
	, _size(0)
	, _capacity(0)
{
	string temp(str._str);
	swap(temp);
}

string::~string()
{
	//在释放一连串的空间时需要用 delete[] 而不能用 delete
	delete[] _str;
	_str = nullptr;
	_size = _capacity = 0;
}

void string::clear()
{
	//只需要把第一个字符换成'\0' 然后把_size标为0 就能达到清空一个字符串的效果
	//清空字符串只是清空字符串的内容和有效长度 容量并不会清空
	_str[0] = '\0';
	_size = 0;
}

char& string::operator[](size_t pos)
{
	assert(pos >= 0 && pos <= _size - 1);

	return _str[pos];
}

const char& string::operator[](size_t pos) const
{
	assert(pos >= 0 && pos <= _size - 1);

	return _str[pos];
}

void string::reserve(size_t n)
{
	//只有申请的容量大于字符串的容量才会扩容
	//即不会出现容量缩小的情况
	if (n > _capacity)
	{
		//开辟一块大小为n + 1的新空间存放原来空间的数据并预留空间
		//额外开的一个空间是为了存放'\0' 因为这个'\0'并不归属于字符串 不计入_size 和 _capacity
		//所以需要我们自己偷偷开空间自行管理
		char* temp = new char[n + 1];
		std::strcpy(temp, _str);

		//一定要先释放原来的空间 不然原来的空间在_str的指向被替换后就找不到了
		//这样会造成内存泄漏
		delete[] _str;

		//修改_str指向和展示给用户的容量大小
		_str = temp;
		_capacity = n;
	}
}

void string::resize(size_t n, char c)
{
	//resize分为3种情况
	//一、n < _size 只保留n个字符 其他全部丢弃
	//二、n > _size && n =< _capacity 用字符c来延伸n - _size个字符 不需要扩容
	//三、n > _size && n > _capacity 同二但需要扩容

	if (n < _size)
	{
		//情况一
		//第n个字符的下标为n - 1 我们只需将下标为n的位置改为'\0'就能达到删除效果
		_str[n] = '\0';
		_size = n;
	}
	else
	{
		//第二、第三种情况一起处理
		//reserve只有当n > _capacity时才会进行扩容 所以当n = _capacity时并不会起任何作用
		reserve(n);
		std::memset(_str + _size, c, n - _size);
		_size = n;
		_str[_size] = '\0';
	}
}

void string::push_back(char c)
{
	if (_size == _capacity)
	{
		//当字符串为空串时 _capacity == 0 为了应对对空串插入的情况要进行判断 当_capacity为0时就对_capacity赋初值
		//reserve在申请扩容时会对_capacity进行修改 所以后续不再需要修改_capacity
		reserve(_capacity == 0 ? 5 : _capacity * 2);
	}
	_str[_size++] = c;

	//要在字符串末尾补'\0'
	//因为在申请空间时每次都会多申请一个空间 且这个空间没有记录在_capacity中
	//所以不存在 在_size++后因为_size == _capacity而没有位置存放'\0'的情况
	_str[_size] = '\0';
}

string& string::append(const string& str)
{
	assert(str._str);

	if (str._size + _size > _capacity)
	{
		reserve(str._size + _size);
	}

	//str自带'\0' 不需要我们自行补充
	//strcat会找到_str的'\0' 然后从这个位置开始把str._str拷贝过去 
	//拷贝完str._str的'\0'结束
	std::strcat(_str, str._str);
	_size += str._size;

	return *this;
}

string& string::append(const char* s)
{
	assert(s);
	size_t len = std::strlen(s);

	if (_size + len > _capacity)
	{
		reserve(_size + len);
	}

	std::strcat(_str, s);
	_size += len;

	return *this;
}

string& string::operator+=(const string& str)
{
	assert(str._str);

	append(str);

	return *this;
}

string& string::operator+=(const char* s)
{
	assert(s);

	append(s);

	return *this;
}

string& string::operator+=(char c)
{
	push_back(c);

	return *this;
}

void string::swap(string& str)
{
	std::swap(_str, str._str);
	std::swap(_size, str._size);
	std::swap(_capacity, str._capacity);
}

size_t string::max_size() const
{
	return string::npos;
}

void string::pop_back()
{
	if (_size > 0)
	{
		_str[--_size] = '\0';
	}
}

string& string::insert(size_t pos, const string& str)
{
	//当pos == _size就相当于尾插
	assert(pos >= 0 && pos <= _size);
	assert(str._str);

	//如果插入之后字符串的有效长度大于容量就需要扩容
	if (_size + str._size > _capacity)
	{
		//至少扩大到有效长度
		reserve(_size + str._size);
	}

	size_t end = _size;
	//将插入位置后的数据后移 包括末尾的'\0'
	while (end >= pos && end != npos)
	{
		_str[end + str._size] = _str[end];
		--end;
	}

	//这里拷贝了str._size个字符 也就是_str的全部字符但不包括末尾的'\0'
	std::memcpy(_str + pos, str._str, str._size);
	_size += str._size;

	return *this;
}

string& string::insert(size_t pos, const char* s)
{
	assert(pos >= 0 && pos <= _size);
	assert(s);
	
	size_t len = std::strlen(s);
	if (_size + len > _capacity)
	{
		reserve(_size + len);
	}

	size_t end = _size;
	while (end >= pos && end != string::npos)
	{
		_str[end + len] = _str[end];
		--end;
	}

	std::memcpy(_str + pos, s, len);
	_size += len;

	return *this;
}

string& string::insert(size_t pos, size_t n, char c)
{
	//pos == _size时相当于尾插
	assert(pos >= 0 && pos <= _size);
	
	if (_size + n > _capacity)
	{
		reserve(_size + n);
	}

	size_t end = _size;
	while (end >= pos && end != string::npos)
	{
		_str[end + n] = _str[end];
		--end;
	}

	//因为前面后移时把'\0'也后移了 所以不需要补充'\0'
	std::memset(_str + pos, c, n);
	_size += n;

	return *this;
}

string& string::erase(size_t pos, size_t len)
{
	//删除有两种情况 一种是把pos后面只删了一部分 另一种是把pos后面的内容全删了
	assert(pos >= 0 && pos <= _size - 1);

	if (len == string::npos || pos + len >= _size)
	{
		//第一种情况
		_size = pos;
		_str[_size] = '\0';
	}
	else
	{
		//第二种情况
		std::strcpy(_str + pos, _str + pos + len);
		_size -= len;
	}

	return *this;
}

size_t string::find(char c, size_t pos) const
{
	assert(pos >= 0 && pos <= _size - 1);

	for (size_t i = pos; i <= _size - 1; ++i)
	{
		if (_str[i] == c)
		{
			return i;
		}
	}

	return string::npos;
}

size_t string::find(const string& str, size_t pos) const
{
	assert(str._str);
	assert(pos >= 0 && pos <= _size - 1);

	const char* p = std::strstr(_str + pos, str._str);

	return p == nullptr ? string::npos : p - _str;
}

size_t string::find(const char* s, size_t pos) const
{
	assert(s);
	assert(pos >= 0 && pos <= _size - 1);

	const char* p = std::strstr(_str + pos, s);

	return p == nullptr ? string::npos : p - _str;
}

std::ostream& operator<<(std::ostream& os, const string& str)
{
	assert(str.c_str());

	//有迭代器就支持范围for循环
	for (auto& ch : str)
	{
		os << ch;
	}

	return os;
}

//string string::substr(size_t pos = 0, size_t len = string::npos) const
//{
//	assert(pos >= 0 && pos <= _size - 1);
//	if (len == string::npos || pos + len >= _size)
//	{
//		//将pos后面的位置全部复制
//		char* temp = new char[_size - pos + 1];//留一个位置放'\0'
//		for (size_t i = 0, j = pos; j <= _size; ++i, ++j)
//		{
//			temp[i] = _str[j];
//		}
//
//		string ans(temp);
//		delete[] temp;
//		return ans;
//	}
//
//	//将pos后面的位置部分复制
//	char* temp = new char[len + 1];
//	for (size_t i = 0, j = pos; j < pos + len; ++i, ++j)
//	{
//		temp[i] = _str[j];
//	}
//
//	temp[len] = '\0';
//
//	string ans(temp);
//	delete[] temp;
//
//	return ans;
//}

string string::substr(size_t pos, size_t len) const
{
	assert(pos >= 0 && pos <= _size - 1);

	//ans为局部变量 在出函数时会自行调用析构函数
	string ans;

	//if (len == string::npos || pos + len >= _size)
	//{
	//	//将pos后面的位置全部复制
	//	ans.reserve(_size - pos);
	//	//for (size_t i = 0, j = pos; j <= _size; ++i, ++j)
	//	//{
	//	//	ans[i] = _str[j];
	//	//}
	//}
	//else
	//{
	//	//将pos后面的位置部分复制
	//	ans.reserve(len);
	//	//for (size_t i = 0, j = pos; j < pos + len; ++i, ++j)
	//	//{
	//	//	ans[i] = _str[j];
	//	//}
	//}

	//当len == string::npos || pos + len >= _size == true时为第一种情况：将pos后面的位置全部复制
	//反之为第二种情况：将pos后面的位置部分复制
	(len == string::npos || pos + len >= _size) ? ans.reserve(_size - pos) : ans.reserve(len);

	//两种情况不同的地方只有开的空间大小不同 截取字串都可以通过+=来完成 所以一起完成
	for (size_t i = pos; i <= _size; ++i)
	{
		ans += _str[i];
	}

	return ans;
}

bool string::operator==(const string& str) const
{
	return _size == str._size
		&& std::strcmp(_str, str._str) == 0;
}

bool string::operator>(const string& str) const
{
	return std::strcmp(_str, str._str) > 0;
}

bool string::operator>=(const string& str) const
{
	return _str > str._str || _str == str._str;
}

bool string::operator!=(const string& str) const
{
	return !(_str == str._str);
}

bool string::operator<(const string& str) const
{
	return !(_str >= str._str);
}

bool string::operator<=(const string& str) const
{
	return !(_str > str._str);
}

std::istream& operator>>(std::istream& in, simulation::string& str)
{
	//如果对已经有内容的字符串进行输入的话 会先清空内容再重新赋值
	str.clear();

	char ch = in.get();

	//处理有效字符前的空白字符
	//如果输入的只有空白字符 当把这些空白字符读取完毕的时候 get会返回EOF 此时就不会进入循环了
	while (ch == ' ' || ch == '\n')
	{
		ch = in.get();
	}

	//创建一个缓冲区 避免每插入一个字符就扩一次容造成性能损失
	char buffer[128] = { 0 };
	size_t i = 0;
	//cin读取到' '(空格) '\n'就会停止读取
	while (ch != ' ' && ch != '\n')
	{
		buffer[i++] = ch;

		//为'\0'预留一个空间
		if (i == 127)
		{
			buffer[i] = '\0';
			str += buffer;
			i = 0;
		}

		ch = in.get();
	}

	//当i != 0时 意味着缓冲区内还有数据 要把这些数据也输入进字符串
	if (i)
	{
		buffer[i] = '\0';
		str += buffer;
	}

	return in;
}

std::istream& getline(std::istream& in, simulation::string& str)
{
	//getline可以读取一行

	str.clear();

	char buffer[128] = { 0 };
	size_t i = 0;

	char ch = in.get();
	while (ch != '\n')
	{
		buffer[i++] = ch;

		//为'\0'预留一个空间
		if (i == 127)
		{
			buffer[i] = '\0';
			str += buffer;
			i = 0;
		}

		ch = in.get();
	}
	if (i)
	{
		buffer[i] = '\0';
		str += buffer;
	}

	return in;
}

//传统写法
//string& operator=(const string& str)
//{
//	//如果等号两边为同一个字符串 就不进行赋值操作
//	if (this != &str)
//	{
//		delete[] _str;
//		_str = new char[str._capacity + 1];
//		std::strcpy(_str, str._str);
//
//		_size = str._size;
//		_capacity = str._capacity;
//	}
//
//	return *this;
//}

//现代写法
string& string::operator=(const string& str)
{
	if (this != &str)
	{
		string temp(str);
		
		swap(temp);//this->swap(temp);
	}

	return *this;
}

//现代写法变形
//因为变形的参数并不是引用 所以在传参时就会调用一次拷贝构造
//相当于把现代写法函数体内的拷贝构造移到了传参过程中
//string& operator=(string temp)
//{
//	swap(temp);
//	return *this;
//}