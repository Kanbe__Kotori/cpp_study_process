#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

//int main()
//{
//	std::string s1("hello");
//	std::cout << s1.capacity() << std::endl;
//	std::cout << s1.size() << std::endl;
//	std::cout << s1[15] << std::endl;
//	return 0;
//}

//LeetCode 917.仅仅反转字母
//class Solution {
//public:
//    string reverseOnlyLetters(string s) {
//        int begin = 0;
//        int end = s.size() - 1;
//
//        while (begin < end)
//        {
//            while (!isalpha(s[begin]) && begin < end)
//            {
//                ++begin;
//            }
//            while (!isalpha(s[end]) && begin < end)
//            {
//                --end;
//            }
//
//            //交换完了别别忘了begin++和end-- 
//            //如果忘记了的话就会一直交换begin和end 陷入死循环
//            swap(s[begin++], s[end--]);
//        }
//
//        return s;
//    }
//};
//
//int main()
//{
//    string s = Solution().reverseOnlyLetters("ab-cd");
//    cout << s << endl;
//    return 0;
//}

//LeetCode 387.字符串中的第一个唯一字符
//class Solution {
//public:
//    int firstUniqChar(string s)
//    {
//        //数组用来记录每个字母出现的次数 一共26个字母 所以大小为26
//        int count[26] = { 0 };
//        for (auto& k : s)
//        {
//            //映射 a在0位置 z在25位置
//            ++count[k - 'a'];
//        }
//
//        //从头开始循环遍历字符串
//        for (int i = 0; i < s.size(); ++i)
//        {
//            //第一个在数组映射位置大小为1的字母就是题目要求的字母
//            if (count[s[i] - 'a'] == 1)
//            {
//                return i;
//            }
//        }
//        return -1;
//    }
//};