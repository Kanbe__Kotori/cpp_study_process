﻿#include <iostream>
#include <vector>

using namespace std;

//#include <string>
//int main()
//{
//	vector<int> vi;
//	vector<string> vs;
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi;
//	return 0;
//}

//int main()
//{
//	vector<char> vc(3);
//	for (auto& k : vc)
//	{
//		cout << k << endl;
//	}
//	return 0;
//}
//#include <iomanip>
//int main()
//{
//	//用4个16个vi进行初始化
//	vector<int> vi(4, 16);
//
//	//用4个16个vi进行初始化
//	vector<double> vd(2, 520.1314);
//
//	//用3个'x'给vc进行初始化
//	vector<char> vc(3, 'x');
//
//	//输出vi
//	for (auto& K : vi)
//	{
//		cout << K << " ";
//	}
//	cout << endl;
//
//	//输出vd
//	for (auto& K : vd)
//	{
//		//默认情况下，cout显示数据的最大位数（包括小数点之前和⼩数点之后）是6位
//		//我们可以通过<iomanip>库中的setprecision(n)来控制cout输出的精度
//		//若指定的位数大于自身位数，则按自身位数全部输出(注意小数末尾的0不输出)
//		//注：设置的setprecision(n)会⼀直存在，直到更改设置
//		cout << setprecision(8) <<K << " ";
//	}
//
//	//将精度修改为默认设置，并换行
//	cout << setprecision(6) <<endl;
//
//	//输出vc
//	for (auto& K : vc)
//	{
//		cout << K << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	//构造存储了两个10的vi
//	vector<int> vi(2, 10);
//
//	//拷贝构造
//	vector<int> vi_copy(vi);
//
//	cout << "vi: ";
//	for (auto k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	cout << "vi_copy: ";
//	for (auto k : vi_copy)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi;
//
//	//对vi进行尾插
//	vi.push_back(5);
//	vi.push_back(2);
//	vi.push_back(0);
//	vi.push_back(1);
//	vi.push_back(4);
//
//	//截取vi的前3个数据来构造vii
//	vector<int> vii(vi.begin(), vi.begin() + 3);
//
//	for (auto& k : vii)
//	{
//		cout << k << " ";
//	}
//
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	int array[] = { 1,1,4,5,1,4 };
//
//	//array为数组名，是数组首元素的地址
//	//sizeof(array) / sizeof(array[0]用于求数组的长度
//	vector<int> vi(array, array + sizeof(array) / sizeof(array[0]));
//
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi(10, 3);
//	vector<int> vii = vi;
//
//	for (auto& k : vii)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//#include <string>
//int main()
//{
//	vector<int> vi{ 1, 2, 3, 4, 5, 6 };
//	vector<double> vd{ 1.1, 2.2, 3.3 };
//	vector<string> vs{ "Hello", "vector", "!" };
//
//	cout << "vi: ";
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	cout << "vd: ";
//	for (auto& k : vd)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	cout << "vs: ";
//	for (auto& k : vs)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	int array[] = { 1,2,3,4 };
//	vector<int> vi(array);
//	return 0;
//}

//int main()
//{
//	vector<int> vi{ 1,2,3,4,5,6,7,8,9 };
//
//	//vi的类型是vector<int> 而不是vector
//	//所以是vector<int>::iterator 而不是vector::iterator
//	vector<int>::iterator bit = vi.begin();
//	vector<int>::iterator eit = vi.end();
//
//	cout << "vi的第一个元素为：" << *bit << endl;
//	cout << "vi的最后一个元素为：" << *(eit - 1) << endl;
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi{ 1,2,3,4,5,6,7,8,9 };
//	vector<int>::reverse_iterator rbit = vi.rbegin();
//	vector<int>::reverse_iterator reit = vi.rend();
//
//	//倒着打印vi
//	while (rbit != reit)
//	{
//		cout << *rbit << " ";
//
//		//虽然rbit在reit的后面
//		//但是rbegin到rend任然是++而不是--
//		++rbit;
//	}
//
//	cout << endl;
//	return 0;
//}

//#include <string>
//int main()
//{
//	vector<int> vi{ 1, 2, 3, 4, 5, 6 };
//
//	//vs中有2个字符串，所以vs的有效长度为2
//	vector<string> vs{ "Hello", "vector" };
//	
//	cout << vi.size() << endl;
//	cout << vs.size() << endl;
//	return 0;
//}

//int main()
//{
//	vector<int> vi{ 1,2,3,4,5 };
//	cout << vi.capacity() << endl;
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi;
//	cout << vi.empty() << endl;
//
//	vi.push_back(1);
//	cout << vi.empty() << endl;
//
//	return 0;
//}

//int main()
//{
//	//情况一: n < size
//	vector<int> vi1{ 1,2,3,4,5,6,7,8,9 };
//	cout << "vi1 resize前的长度为 " << vi1.size() << endl;
//	cout << "vi1 resize前的容量为 " << vi1.capacity() << endl;
//	vi1.resize(4);
//	cout << "vi1 resize后的长度为 " << vi1.size() << endl;
//	cout << "vi1 resize后的容量为 " << vi1.capacity() << endl;
//
//	cout << "vi1: " << endl;
//	for (auto& k : vi1)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//
//	//情况二: n > size
//	vector<int> vi2{ 1,2,3,4,5 };
//	vector<int> vi3(vi2);
//	cout << "vi2 resize前的长度为 " << vi2.size() << endl;
//	cout << "vi2 resize前的容量为 " << vi2.capacity() << endl;
//	cout << "vi3 resize前的长度为 " << vi3.size() << endl;
//	cout << "vi3 resize前的容量为 " << vi3.capacity() << endl;
//
//	//指定val
//	vi2.resize(11, 11);
//
//	//不指定val
//	vi3.resize(11);
//
//	cout << "vi2 resize后的长度为 " << vi2.size() << endl;
//	cout << "vi2 resize后的容量为 " << vi2.capacity() << endl;
//	cout << "vi3 resize后的长度为 " << vi3.size() << endl;
//	cout << "vi3 resize后的容量为 " << vi3.capacity() << endl;
//
//	cout << "vi2: " << endl;
//	for (auto& k : vi2)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	cout << "vi3: " << endl;
//	for (auto& k : vi3)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi;
//	//记录vi的capacity
//	size_t capacityRecord = vi.capacity();
//	cout << "容量为：" << capacityRecord << endl;
//	//尾插100个数据
//	for (int i = 0; i < 100; ++i)
//	{
//		vi.push_back(i);
//
//		//如果容量发生了改变 就把新容量的值打印出来
//		if (capacityRecord != vi.capacity())
//		{
//			capacityRecord = vi.capacity();
//			cout << "容量改变为：" << capacityRecord << endl;
//		}
//
//	}
//
//	return 0;
//}

//int main()
//{
//	//用100个0来构建一个vector<int>
//	vector<int> vi;
//	vi.reserve(100);
//	size_t capacityRecord = vi.capacity();
//	cout << "容量为：" << capacityRecord << endl;
//
//	//尾插150个数据
//	for (int i = 0; i < 150; ++i)
//	{
//		vi.push_back(i);
//
//		//如果容量发生了改变 就把新容量的值打印出来
//		if (capacityRecord != vi.capacity())
//		{
//			capacityRecord = vi.capacity();
//			cout << "容量改变为：" << capacityRecord << endl;
//		}
//	}
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi(3, 10);
//
//	for (int i = 0; i < vi.size(); ++i)
//	{
//		cout << vi[i] << " ";
//	}
//
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	vector<int> vi;
//	vi.push_back(1);
//	vi.push_back(2);
//	vi.push_back(3);
//	vi.push_back(4);
//	vi.push_back(5);
//
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	vector<int> vi(3, 100);
//	vi.pop_back();
//	vi.pop_back();
//
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//插入单个数据
//int main()
//{
//	vector<int> vi{ 1,2,3,4,5,6,7,8,9 };
//
//	//头插
//	vi.insert(vi.begin(), 0);
//
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//填充插入 插入多个相同数据
//int main()
//{
//	vector<int> vi{ 0,1,2,3,4,5,7,8,9 };
//
//	//在5的后面插入10个6
//	vi.insert(vi.begin() + 6, 10, 6);
//
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//
//	cout << endl;
//	return 0;
//}

//插入一个范围的数据
//int main()
//{
//	vector<int> vi{ 1,5,6,7,8,9 };
//	int array[] = { 1,2,3,4,5,6,7,8,9 };
//
//	//vi.begin() + 1 为插入位置
//	//array + 1为要插入数据的开头
//	//array + 4为要插入数据的结尾再后面一个位置 -》左闭右开
//	vi.insert(vi.begin() + 1, array + 1, array + 4);
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	 }
//
//	cout << endl;
//	return 0;
//}

//删除单个数据
//int main()
//{
//	vector<int> vi{ 1,2,3,4,4,5,6 };
//	vi.erase(vi.begin() + 3);
//
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi{ 1,2,3,4,5,6,7,8,9 };
//	vi.erase(vi.begin() + 1, vi.end() - 1);
//
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi1{ 1,2,3,4,5 };
//	vector<int> vi2{ 6,7,8,9,10 };
//	vi1.reserve(100);
//
//	cout << "交换前vi1的数据为：";
//	for (auto& k : vi1)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	cout << "交换前vi1的长度为：" << vi1.size() << endl;
//	cout << "交换前vi1的容量为：" << vi1.capacity() << endl << endl;
//
//	cout << "交换前vi2的数据为：";
//	for (auto& k : vi2)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	cout << "交换前vi2的长度为：" << vi2.size() << endl;
//	cout << "交换前vi2的容量为：" << vi2.capacity() << endl << endl;
//
//	vi1.swap(vi2);
//
//	cout << "交换后vi1的数据为：";
//	for (auto& k : vi1)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	cout << "交换后vi1的长度为：" << vi1.size() << endl;
//	cout << "交换后vi1的容量为：" << vi1.capacity() << endl << endl;
//
//	cout << "交换前vi2的数据为：";
//	for (auto& k : vi2)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	cout << "交换后vi2的长度为：" << vi2.size() << endl;
//	cout << "交换后vi2的容量为：" << vi2.capacity() << endl << endl;
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi{ 1,2,3,4,5 };
//
//	cout << "clear前vi的数据为：";
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	cout << "clear前vi的长度为：" << vi.size() << endl;
//	cout << "clear前vi的容量为：" << vi.capacity() << endl << endl;
//
//	vi.clear();
//
//	cout << "clear后vi的数据为：";
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//	cout << "clear后vi的长度为：" << vi.size() << endl;
//	cout << "clear后vi的容量为：" << vi.capacity() << endl << endl;
//	return 0;
//}
#include <algorithm>

//int main()
//{
//	vector<int> vi{ 1,2,3,4,5,6 };
//	
//	//vi.begin() 为查找的起始点
//	//vi.end() 为查找的终止点 -》 左闭右开
//	//2为要查找到值
//	vector<int>::iterator pos = find(vi.begin(), vi.end(), 2);
//
//	//如果找到了就会返回对应位置的迭代器
//	//如果没找到就会返回会find的第二个参数 这里为vi.end()
//	if (pos != vi.end())
//	{
//		cout << *pos << endl;
//	}
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi{ 1,4,6,2,4,7,9,44,62,24,11 };
//
//	//排升序用正向迭代器
//	sort(vi.begin(), vi.end());
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	//排降序用反向迭代器
//	sort(vi.rbegin(), vi.rend());
//	for (auto& k : vi)
//	{
//		cout << k << " ";
//	}
//	cout << endl;
//
//	return 0;
//}

int main()
{
	vector<int> vi{ 1,2,3,4,5,6,7,8,9 };

	reverse(vi.begin(), vi.end());
	for (auto& k : vi)
	{
		cout << k << " ";
	}
	cout << endl;

	return 0;
}