//LeetCode118.�������

//class Solution {
//public:
//    vector<vector<int>> generate(int numRows) {
//        vector<vector<int>> ans(numRows);
//        for (int i = 0; i < numRows; ++i)
//        {
//            ans[i].resize(i + 1, 0);
//            for (int k = 0; k <= i; ++k)
//            {
//                if (k == 0 || k == i)
//                {
//                    ans[i][k] = 1;
//                }
//                else
//                {
//                    ans[i][k] = ans[i - 1][k - 1] + ans[i - 1][k];
//                }
//            }
//        }
//
//        return ans;
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> generate(int numRows) {
//        vector<vector<int>> ans(numRows);
//
//        for (int i = 0; i < numRows; ++i)
//        {
//            ans[i].resize(i + 1);
//            ans[i][0] = ans[i][i] = 1;
//
//            for (int k = 1; k < i; ++k)
//            {
//                ans[i][k] = ans[i - 1][k - 1] + ans[i - 1][k];
//            }
//        }
//
//        return ans;
//    }
//};