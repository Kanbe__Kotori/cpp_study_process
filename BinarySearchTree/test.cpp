#include "BSTree.hpp"
using namespace K_simulation;

int main()
{
	BSTree<int> bst;
	int array[] = { 5,3,27,97,23,13,45,12,77 };
	for (auto& k : array)
	{
		bst.insert(k);
	}
	//bst.inOrder();
	//std::cout << bst.find(0) << std::endl;
	return 0;
}