#pragma once

#ifndef _ALL_H_
#define _ALL_H_

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <thread>
#include <functional>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <future>
#include <algorithm>
#include <ranges>
//#include "unistd.h"
using namespace std;

#endif 
