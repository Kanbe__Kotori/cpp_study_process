//TODO: LeetCode606. 根据二叉树创建字符串

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
// class Solution {
// public:
//     string tree2str(TreeNode* root) {
//         if (root == nullptr)
//         {
//             return "";
//         }

//         string str(to_string(root->val));//C++中string只能用字符串或者是字符来构造 不能用int等类型

//         if (root->left) //如果左子树不为空
//         {
//             str += '(';
//             str += tree2str(root->left);
//             str += ')';
//         }
//         else if (root->right) //如果左子树为空但右子树不为空
//         {
//             str += "()";
//         }

//         if (root->right) //如果右子树不为空
//         {
//             str += '(';
//             str += tree2str(root->right);
//             str += ')';
//         }

//         return str;
//     }
// };