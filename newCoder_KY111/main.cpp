//牛客KY111 日期差值
//有两个日期，求两个日期之间的天数，如果两个日期是连续的我们规定他们之间的天数为两天

#include <iostream>
#include <iomanip>//格式控制需要的头文件
#include <cstdio>
using namespace std;

class Solution {
  public:
    Solution(int year, int month, int day)
        : _year(year)
        , _month(month)
        , _day(day)
    {}

    Solution(const Solution& s)
    {
        _year = s._year;
        _month = s._month;
        _day = s._day;
    }

    [[nodiscard]] bool isLeapYear(const int year) const
    {
        if (year % 400 == 0 || (year % 4 == 0) && (year % 100 != 0))
        {
            return true;
        }

        return false;
    }

    [[nodiscard]] int getMonthDay(const int year, const int month) const
    {
        int monthDays[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if(isLeapYear(year) && month == 2)
        {
            return 29;
        }

        return monthDays[month];
    }

    bool operator>(const Solution& s) const
    {
        if (_year > s._year) {
            return true;
        }
        else if (_year == s._year && _month > s._month)
        {
            return true;
        }
        else if (_year == s._year && _month == s._month && _day > s._day)
        {
            return true;
        }

        return false;
    }

    bool operator==(const Solution& s) const
    {
        return _year == s._year 
            && _month == s._month 
            && _day == s._day;
    }

    bool operator>=(const Solution& s) const
    {
        return *this == s || *this > s;
    }

    bool operator<(const Solution& s) const
    {
        return !(*this >= s);
    }

    bool operator<=(const Solution& s) const
    {
        return !(*this > s);
    }

    bool operator!=(const Solution& s) const
    {
        return !(*this == s);
    }

    Solution& operator+=(const int days)
    {
        _day += days;
        while(_day > getMonthDay(_year, _month))
        {
            _day -= getMonthDay(_year, _month);
            ++_month;

            if(_month == 13)
            {
                ++_year;
                _month = 1;
            }
        }

        return *this;
    }

    Solution& operator++()
    {
        *this += 1;
        return *this;
    }

    int operator-(const Solution& s) const
    {
        Solution max(*this);
        Solution min(s);

        if (max < min)
        {
            max = s;
            min = *this;
        }

        int count = 0;
        while(min < max)
        {
            ++count;
            ++min;
        }

        return count + 1;
    }

    void test()
    {
        cout << _year << " " << _month << " " << _day << endl;
    }

private:
    int _year;
    int _month;
    int _day;

};

int main()
{
    int y1, m1, d1;
    int y2, m2, d2;

    //只有cout才能进行格式控制
    //cin >> setw(4) >> y1 >> setw(2) >> m1 >> setw(2) >> d1;
    //cin >> setw(4) >> y2 >> setw(2) >> m2 >> setw(2) >> d2;
    scanf("%4d%2d%2d", &y1, &m1, &d1);
    scanf("%4d%2d%2d", &y2, &m2, &d2);

    Solution s1(y1, m1, d1);
    Solution s2(y2, m2, d2);
    //s1.test();
    //s2.test();

    cout << (s1 - s2) << endl;
    return 0;
}