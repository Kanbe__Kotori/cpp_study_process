﻿#include "vector.h"
#include <string>
#include <vector>
//using namespace std;
//using namespace simulation;
using std::cout;
using std::endl;
using std::string;

//#include <iostream>
////#include <vector>
//#include <string>
////using namespace std;
//#include "vector.h"
//using namespace simulation;
//
//int main()
//{
//	std::string s("Hello World!");
//	vector<char> vc(s.begin(), s.end());
//
//	for (auto& k : vc)
//	{
//		std::cout << k << " ";
//	}
//	std::cout << std::endl;
//	return 0;
//}

#if 0
void test1()
{
	vector<int> vi;
	vi.reserve(100);
	cout << vi.capacity() << endl;
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);
	vi.reserve(200);
	cout << vi.capacity() << endl;
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);
	for (auto& k : vi)
	{
		cout << k << " ";
	}

	cout << endl;
}

void test2()
{
	vector<int> vi;
	vi.insert(vi.begin(), 1);
	vi.insert(vi.begin(), 1);
	vi.insert(vi.begin(), 1);
	vi.insert(vi.begin(), 1);
	vi.insert(vi.begin(), 1);
	vi.insert(vi.begin(), 1);
	vi.insert(vi.begin(), 1);
	vi.insert(vi.begin(), 1);
	vi.insert(vi.begin(), 1);
	vi.insert(vi.begin(), 1);
	vi.insert(vi.begin() + 3, 3);
	for (auto& k : vi)
	{
		cout << k << " ";
	}

	cout << endl;
}

void test3()
{
	vector<int> vi;
	vi.push_back(1);
	vi.push_back(2);
	vi.push_back(3);
	vi.push_back(4);
	vi.push_back(5);
	vi.push_back(6);
	vi.push_back(7);
	vi.erase(vi.begin() + 3);
	vi.erase(vi.begin());

	for (auto& k : vi)
	{
		cout << k << " ";
	}

	cout << endl;
}

void test4()
{
	vector<int> vi;
	vi.pop_back();
}

void test5()
{
	vector<int> vi;
	vi.resize(10, 4);
	for (auto& k : vi)
	{
		cout << k << " ";
	}
	cout << endl;

	vi.resize(10);
	for (auto& k : vi)
	{
		cout << k << " ";
	}
	cout << endl;
}

void test6()
{
	vector<int> vi;
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);

	vector<int> vii(vi);
	for (auto& k : vii)
	{
		cout << k << " ";
	}
	cout << endl;
}

void test7()
{
	vector<int> vi;
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);
	vector<int> vii = vi;
	vi = vi;
	for (auto& k : vii)
	{
		cout << k << " ";
	}
	cout << endl;
}

void test8()
{
	string s("Hello World!");
	vector<char> vc(s.begin(), s.end());
	for (auto& k : vc)
	{
		cout << k;
	}
	cout << endl;
	cout << vc.empty() << endl;
	vc.clear();
	cout << vc.empty() << endl;
	for (auto& k : vc)
	{
		cout << k;
	}

}

void test9()
{
	vector<int> vi;
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);
	vi.push_back(1);

	for (int i = 0; i < vi.size(); ++i)
	{
		cout << vi[i] << " ";
	}
	cout << endl;

	vi.pop_back();
	vi.pop_back();
	vi.pop_back();

	for (int i = 0; i < vi.size(); ++i)
	{
		cout << vi[i] << " ";
	}
	cout << endl;
}

#endif
//int main()
//{
//	int i1 = int(10);
//	int i2 = int();
//	int* pi = new int(20);
//
//	double d = double(22.2);
//	char c = char('x');
//
//	cout << i1 << endl << i2 << endl << *pi << endl;
//	cout << d << endl;
//	cout << c << endl;
//
//	delete pi;
//	return 0;
//}

//int main()
//{
//	std::string s("Hello vector!");
//	std::vector<char> vc(s.begin(), s.end());
//
//	for (const auto& k : vc)
//	{
//		cout << k;
//	}
//
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	vector<int> vi(2, 100);
//	return 0;
//}



//int main() 
//{
//	vector<Test> v;
//	Test t1, t2, t3, t4;
//	v.reserve(5);
//	cout << "-------------------" << endl;
//	v.push_back(t1);
//	v.push_back(t2);
//	v.push_back(t3);
//	v.push_back(t4);
//	cout << "----------------" << endl;
//	v.pop_back();
//	cout << "----------------------" << endl;
//	return 0;
//}

//int main() 
//{
//	std::vector<Test> v;
//	Test t1, t2, t3, t4;
//	v.reserve(5);
//	cout << "-------------------" << endl;
//	v.push_back(t1);
//	v.push_back(t2);
//	v.push_back(t3);
//	v.push_back(t4);
//	cout << "----------------" << endl;
//	v.pop_back();
//	cout << "----------------------" << endl;
//	return 0;
//}


// 指针++和指针=1是一样的效果
//int main()
//{
//	int* p1 = NULL;
//	//int i;
//	char* p2 = NULL;
//
//	//cout << (p1 + 1) << " " << ++p1 << endl;
//	//cout << (p2 + 1) << " " << ++p2 << endl;
//
//	printf("%p %p %p\n",p1, p1 + 1, p1++);
//	printf("%p %p %p\n",p2, p2 + 1, p2++);
//
//	return 0;
//}

//int main()
//{
//	Test* t = (Test*)malloc(sizeof(Test));
//	*t = Test();
//	return 0;
//}

//int main()
//{
//}

class Test {
public:
	Test() {
		cout << "Test()" << endl;
	}

	~Test() {
		cout << "~Test()" << endl;
	}

	Test(const Test&) {
		cout << "Test(const Test&)" << endl;
	}

	Test(Test&&) noexcept {
		cout << "Test(Test&&)" << endl;
	}

	Test& operator=(const Test&) {
		cout << "Test& operator=(const Test&)" << endl;
		return *this;
	}

	Test& operator=(Test&&) noexcept {
		cout << "Test& operator=(Test&&)" << endl;
		return *this;
	}
};

void test1()
{
	simulation::vector<Test> v;
	Test t1, t2, t3, t4;
	v.reserve(5);
	cout << "-------------------" << endl;
	v.push_back(t1);
	v.push_back(t2);
	v.push_back(t3);
	v.push_back(t4);
	cout << "----------------" << endl;
	v.pop_back();
	cout << "----------------------" << endl;
}

void test2()
{
	std::vector<Test> v;
	Test t1, t2, t3, t4;
	v.reserve(5);
	cout << "-------------------" << endl;
	v.push_back(t1);
	v.push_back(t2);
	v.push_back(t3);
	v.push_back(t4);
	cout << "----------------" << endl;
	v.pop_back();
	cout << "----------------------" << endl;
}

int main()
{
	test1();
	cout << endl;
	test2();

	return 0;
}