//牛客 剑指offer
//JZ64 求1+2+3+...+n，
//要求不能使用乘除法、for、while、if、else、switch、case等关键字及条件判断语句（A?B:C）

#include <iostream>
#include <cmath>
using namespace std;
//方法一
//用数学库函数pow来完成
//因为1+2+3+...+n == (1 + n) * n / 2
// class Solution {
// public:
//     int Sum_Solution(int n) {
//         return (int)(pow(n, 2) + n) >> 1;
//     }
// };

//方法二 运用&& 短路的特性 即当&&的左侧为假时 &&的右侧就不会被计算了
//用递归来代替了循环
class Solution {
public:
    int Sum_Solution(int n) {
        int ans = n;
        //&&左侧可写为 if(n > 0)
        //&&右侧一定为真 起到1 + ... + n的作用
        n > 0 && ((ans = ans + Sum_Solution(n - 1)) > 0);
        return ans;
    }
};

int main()
{
    Solution s;
    cout << s.Sum_Solution(5) << endl;
    return 0;
}