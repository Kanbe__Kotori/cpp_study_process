#pragma once
#include "UnionFindSet.h"
#include <bitset>
#include <climits>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <stdexcept>
#include <vector>

using namespace std;
using namespace simulation;

namespace adjacency_matrix {

	/*
	* @para V V->vertex(顶点)，标识顶点的类型，如int，string等
	* @para W W->weight(权重)，标识权重的类型，如int，double等
	* @para W_WAX 非类型模板参数，表示权重的默认值
	* @para Direction 如果为真就代表为有向，为假就是无向
	*/
	template<class V, class W = int, W W_MAX = INT_MAX, bool Direction = false>
	class Graph {
	private:
		vector<V> _vertexs; // 顶点集合，存储所有的顶点；可以通过下标来找顶点

		map<V, size_t> _vertex_index_map; // 记录顶点对应下标，可以通过顶点来找下标

		vector<vector<W>> _matrix; // 邻接矩阵，记录两点之间的关系(边)与权重

	private:
		using Self = Graph<V, W, W_MAX, Direction>;

	public:
		Graph() noexcept = default;

		Graph(const V* p_vertex, size_t size) {
			_vertexs.reserve(size);
			_matrix.resize(size);

			for (size_t i = 0; i < size; ++i) {
				_vertexs.push_back(p_vertex[i]); // 将点存入点集中
				_vertex_index_map[p_vertex[i]] = i; // 记录点与其下标的映射关系

				_matrix[i].resize(size, W_MAX); // 构建邻接矩阵
			}
		}

		size_t getVertexIndex(const V& vertex) const {
			auto it = _vertex_index_map.find(vertex);
			if (it == _vertex_index_map.end()) {
				throw invalid_argument("顶点不存在");
			}
			else {
				return it->second;
			}
		}

		void addEdge(const V& src, const V& dst, const W& weight) {
			size_t src_i = getVertexIndex(src);
			size_t dst_i = getVertexIndex(dst);

			//_matrix[src_i][dst_i] = weight;

			//if (Direction == false) { // 如果为无向图
			//	_matrix[dst_i][src_i] = weight;
			//}

			addEdgeByIndex(src_i, dst_i, weight);
		}

		void print() const {
			// 顶点
			for (size_t i = 0; i < _vertexs.size(); ++i) {
				cout << "[" << i << "]" << "->" << _vertexs[i] << endl;
			}
			cout << endl;

			// 矩阵
			// 横下标
			cout << "  ";
			for (size_t i = 0; i < _vertexs.size(); ++i) {
				//cout << i << " ";
				printf("%4d", i);
			}
			cout << endl;

			for (size_t i = 0; i < _matrix.size(); ++i)
			{
				cout << i << " "; // 竖下标
				for (size_t j = 0; j < _matrix[i].size(); ++j)
				{
					//cout << _matrix[i][j] << " ";
					if (_matrix[i][j] == W_MAX)
					{
						//cout << "* ";
						printf("%4c", '*');
					}
					else
					{
						//cout << _matrix[i][j] << " ";
						printf("%4d", _matrix[i][j]);
					}
				}
				cout << endl;
			}
			cout << endl;
		}

		// 广度优先遍历
		void BFS(const V& start) const {
			size_t start_i = getVertexIndex(start); // 开始遍历的位置的下标
			const size_t size = _vertexs.size();

			queue<size_t> que; // 用来存放需要访问的元素的下标
			bitset<size> visited; // 用来记录哪一些下标对应的元素已经访问过了，可以用来避免重复访问、成环；入队列就标记的话还可以避免同一下标重复入队列

			que.push(start_i);
			visited.set(start_i);

			while (!que.empty()) {
				size_t front_i = que.front();
				que.pop();
				cout << front_i << ": " << _vertexs[front_i] << endl;

				// 将(与 (front_i对应的数据) 相邻的数据)的下标放进队列中
				for (size_t i = 0; i < size; ++i) {
					if (_matrix[front_i][i] != W_MAX) { //i下标的数据与front_i下标的数据有关系
						if (!visited.test(i)) { // 如果i下标已经入过队列了，就不再入了，否则会造成成环问题
							que.push(i);
							visited.set(i);
						}
					}
				}
			}

			// 如果进行遍历的图不是连通图(有孤岛)，就要visited里去找没有遍历过的下标，再以其为起点再次进行遍历(BFS和DFS都要)
			// ......
		}

		// 访问start第level层的数据
		void BFSLevel(const V& start, size_t level) const {
			size_t start_i = getVertexIndex(start); // 开始遍历的位置的下标
			const size_t size = _vertexs.size();

			queue<size_t> que; // 用来存放需要访问的元素的下标
			bitset<size> visited; // 用来记录哪一些下标对应的元素已经访问过了，可以用来避免重复访问、成环；入队列就标记的话还可以避免同一下标重复入队列

			que.push(start_i);
			visited.set(start_i);

			size_t level_size = 1; // 从start开始，每一层节点(有关联的数据)的个数，第一层为1个

			while (!que.empty()) {
				if (level == 1) {
					for (size_t i = 0; i < level_size; ++i) {
						size_t front_i = que.front();
						que.pop();
						cout << front_i << ": " << _vertexs[front_i] << " ";
					}
					cout << endl;
					break;
				}

				// 一层一层的出队列
				for (size_t i = 0; i < level_size; ++i) {
					size_t front_i = que.front();
					que.pop();

					for (size_t j = 0; j < size; ++j) {
						if (_matrix[front_i][j] != W_MAX && visited.test(j) == false) {
							que.push(j);
							visited.set(j);
						}
					}
				}

				level_size = que.size();
				--level;
			}
		}

		void DFS(const V& start) const {
			const size_t size = _vertexs.size();
			size_t start_i = getVertexIndex(start);
			vector<bool> visited(size, false); // 位图不能做参数，不玩位图了

			_DFS(start_i, visited);
		}

		// min_tree为输出型参数，会把最后找到的最小生成树返回回去
		W Kruskal(Self& min_tree) {
			const size_t vt_size = _vertexs.size();

			min_tree._vertexs = _vertexs;
			min_tree._vertex_index_map = _vertex_index_map;
			min_tree._matrix.resize(vt_size);

			for (auto& v : min_tree._matrix) {
				v.resize(vt_size, W_MAX);
			}

			priority_queue<Edge, vector<Edge>, greater<Edge>> small_heap; // 要找权重和最小，建小堆
			for (size_t i = 0; i < vt_size; ++i) {
				for (size_t j = 0; j < vt_size; ++j) {
					if (i < j && _matrix[i][j] != W_MAX) { // 因为最小生成树是无向图的概念，_matrix[i][j]和_matrix[j][i]是同一条边，所以通过限制i < j达到一条边只存一次的效果(上三角)
						small_heap.push(Edge(i, j, _matrix[i][j]));
					}
				}
			}

			// 最小生成树要求通过n - 1条路径来联通n个点，且这n个点的权重和最小，这就要求n个点之间不能成环，成了环就一定会有孤岛
			// 这里用贪心算法来求出局部最优解，用并查集来达到不成环的效果

			size_t edge_size = 0; // 已找到的边的条数
			UnionFindSet ufs(static_cast<size_t>(vt_size)); // 用并查集来判断两个点是否在一个集合里，如果在一个集合里就不选它们两构成的边，否则会成环
			W w_sum = W(); // 最小生成树的权重和

			while (!small_heap.empty()) {
				Edge min = small_heap.top();
				small_heap.pop();

				if (!ufs.inSet(min._srci, min._dsti)) { // 如果min._srci和min._dsti就说明它们两个相连并不会成环
					ufs.Union(min._srci, min._dsti);
					min_tree.addEdgeByIndex(min._srci, min._dsti, min._weight);

					++edge_size;
					w_sum += min._weight;

					cout << _vertexs[min._srci] << "->" << _vertexs[min._dsti] << ": " << min._weight << endl;
				}

				if (edge_size == vt_size - 1) {
					return w_sum;
				}
			}

			return W();
		}

		W prim(Self& min_tree, const V& start) {
			const size_t vt_size = _vertexs.size();
			size_t start_i = getVertexIndex(start); // 因为prim是局部贪心，需要从一个点出发，所以需要一个起点

			min_tree._vertexs = _vertexs;
			min_tree._vertex_index_map = _vertex_index_map;
			min_tree._matrix.resize(vt_size);

			for (auto& v : min_tree._matrix) {
				v.resize(vt_size, W_MAX);
			}

			// prim算法采取局部贪心，找出对某一个点而言的最优解
			// prim将顶点分为两类，一类是已经与其他点联通的，一类是没有与其他点联通的
			// 这里采用一个数组来表示两类，因为不满足一种情况就一定满足另一种情况

			vector<bool> connected(vt_size, false); // true代表已经与其他人有联系了，不能以其引出一条边；false为没有产生过联系，可以引边
			connected[start_i] = true;

			priority_queue<Edge, vector<Edge>, greater<Edge>> min_heap; // 建小堆，找局部权重最小的位置。

			// 因为目前只找到了一个起始点，所以目前不会有任何路径，因此将从起始点延伸出来的路径全部入堆
			for (size_t i = 0; i < vt_size; ++i) {
				if (_matrix[start_i][i] != W_MAX) {
					min_heap.push(Edge(start_i, i, _matrix[start_i][i]));
				}
			}

			// 开始选出n - 1条边
			size_t edge_size = 0;
			W weight_sum = W();
			while (!min_heap.empty()) {
				Edge min = min_heap.top();
				min_heap.pop();

				if (connected[min._dsti] == false) {
					connected[min._dsti] = true;
					min_tree.addEdgeByIndex(min._srci, min._dsti, min._weight);

					++edge_size;
					weight_sum += min._weight;

					cout << _vertexs[min._srci] << "->" << _vertexs[min._dsti] << ": " << min._weight << endl;

					if (edge_size == vt_size - 1) {
						return weight_sum;
					}

					for (size_t i = 0; i < vt_size; ++i) {
						if (_matrix[min._dsti][i] != W_MAX && connected[i] == false) {
							min_heap.push(Edge(min._dsti, i, _matrix[min._dsti][i]));
						}
					}

				}
			}

			return W();
		}

		/*
		* 最短路径，使用局部贪心算法，不能处理带负权的情况
		* 因为使用局部贪心，所以也需要给一个起点
		* 空间复杂度O(N) 时间复杂度O(N^2)
		*
		* @para distance 输出型参数 代表起始点到某一目标点的距离。这里用数组来存储，数组的下标代表对应点的编号，可用_vertex_index_map查询
		* @para parent_path 输出型参数 存储某一个目标点最短路径上的上一个点的下标是什么，可以看作邻接表的入表(只不过这个入表只有一个点，而且存储的是这个点的下标)
		*/
		void Dijkstra(const V& start, vector<W>& distance, vector<size_t> parent_path) {
			size_t start_i = getVertexIndex(start);
			const size_t vt_size = _vertexs.size();

			distance.resize(vt_size, W_MAX); // 目标点距离原点W_MAX说明还没有联通两个点的路径
			parent_path.resize(vt_size, -1); // 某一点的上一个点的下标为-1代表这个点还没有被联通

			distance[start_i] = W()/* 0 */; // 起始点与自己的距离为0
			parent_path[start_i] = start_i; //起始点的上一个点就是自己

			vector<bool> specific_vertex(vt_size, false); // 用来记录哪一些点的最短路径已经被确认了

			// 包括起始点在内，一共有n个点需要确认出与起始点的最短路径
			// 第一次循环是为了确定起始点到起始点的最短路径
			// 第二次及以后才是利用贪心算法来确认其他点的与起始点的最短路径
			size_t remain = vt_size;
			while (remain > 0) {
				size_t closest_index = 0; // 除了已经确认的点，与起始点最近的那个点的下标
				W min_weight = W_MAX; // 与最近的那个点的权重之和。因为除了联通的情况，没有任何权重会大于W_MAX，所以拿W_MAX当初始最小值

				for (size_t i = 0; i < vt_size; ++i) {
					if (specific_vertex[i] == false && distance[i] < min_weight) { // 因为第一遍遍历时，起始点的最短距离没有被确认，且起始点到起始点的距离为0，所以选出来的一定是起始点
						closest_index = i;
						min_weight = distance[i];
					}
				}

				specific_vertex[closest_index] = true; // 确认最短距离

				// 从被确认点开始，找到新的联通点或更新与还没确认距离的点的权重，计算并更新距离起始点的最短距离
				// 因为新联通点原本的权重为W_MAX，所以新联通点的权重一定会被更新
				// 起始点到目标点的新的权重为 start_i -> next_i = start_i -> closest_index + closest_index -> next_i
				// 如果新权重比原权重小，就进行更新，反之则不变
				for (size_t next_i = 0; next_i < vt_size; ++next_i) {
					if (specific_vertex[next_i] == false // 还未被确认最短距离
						&& _matrix[closest_index][next_i] != W_MAX // 与新确认点联通
						&& distance[closest_index] + _matrix[closest_index][next_i] < distance[next_i] /*新权重小于旧权重*/) {

						distance[next_i] = distance[closest_index] + _matrix[closest_index][next_i];
						parent_path[next_i] = closest_index; // 更新目标点最短路径上的上一个点的坐标
					}
				}
			}
		}

		// BellmanFord算法可以解决负权重问题，但是它不能像Dijkstra算法一样求任意一个起始点到其余各点的最短距离，只能求特定的某个点(一般为下标为0的点)到其余各点的距离
		// 空间复杂度O(N) 时间复杂度O(N^3)
		// 返回值代表是否找到了最短路径，因为如果进行有向图中存在负权回路的话就会一直更新最小值导致死循环，这样就找不出最短路径
		bool BellmanFord(vector<W>& distance, vector<size_t> parent_path) {
			const size_t start_i = 0; // 一般不做修改，如果做了修改，算法的循环体逻辑全部就要更改
			const size_t vt_size = _vertexs.size();

			distance.resize(vt_size, W_MAX); // 目标点距离原点W_MAX说明还没有联通两个点的路径
			parent_path.resize(vt_size, -1); // 某一点的上一个点的下标为-1代表这个点还没有被联通

			distance[start_i] = W()/* 0 */; // 起始点与自己的距离为0
			parent_path[start_i] = start_i; //起始点的上一个点就是自己

			// 因为BellmanFord算法采用暴力求解，对每一个点引出的每一条边都进行计算，所以不需要像Dijkstra算法一样用true和false来标定哪一个点的最短距离已经被求出了

			// 因为路径带有负权，所以所每次一条路径因为负权而更新最短距离时，就需要对所有路径(主要是原本利用该路劲构建最短路径的路径)再进行一次检查，看是否有其他路径会因为新更新的路径而更新
			// 可以用优先队列优化(SPFA)使对每一条路径都进行检查更新改为对只会对被影响的路径进行检查更新
			size_t remain_update_turn = vt_size; // 因为有n个点，所以最多需要检查n次
			while (remain_update_turn > 0) {
				bool update = false; // 记录本次循环是否有路径被更新，如果没有则退出循环，如果有的话旧继续更新。就像冒泡排序一样

				for (size_t i = 0; i < vt_size; ++i) {
					for (size_t j = 0; j < vt_size; ++j) {
						// 如果BellmanFord算法不选取下标为0的点为起始点就会在这里出问题
						// 因为如果不先对起始点更新出其他临近点的distance的话，就会造成第一轮的distance[i]除了起始点本身外，其他点全为W_MAX，distance[i]随便加一个大于0的值都会数据溢出变成一个极小值，这样 < W_MAX就会恒成立了
						if (_matrix[i][j] != W_MAX && distance[i] + _matrix[i][j] < distance[j]) {
							distance[j] = distance[i] + _matrix[i][j];
							parent_path[j] = i;

							update = true;

							cout << _vertexs[i] << "->" << _vertexs[j] << ": " << _matrix[i][j] << endl;
						}
					}
				}

				if (update == false) {
					return true;
				}

			}

			// 如果还能进行更新的话就说明存在负值回路，这样就找不到最短路径了
			for (size_t i = 0; i < vt_size; ++i) {
				for (size_t j = 0; j < vt_size; ++j) {
					if (_matrix[i][j] != W_MAX && distance[i] + _matrix[i][j] < distance[j]) {
						return false;
					}
				}
			}

			return true;
		}

		void PrintShortPath(const V& src, const vector<W>& dist, const vector<int>& pPath) {
			size_t srci = GetVertexIndex(src);
			size_t n = _vertexs.size();
			for (size_t i = 0; i < n; ++i) {
				if (i != srci) {
					// 找出i顶点的路径
					vector<int> path;
					size_t parenti = i;
					while (parenti != srci) {
						path.push_back(parenti);
						parenti = pPath[parenti];
					}
					path.push_back(srci);
					reverse(path.begin(), path.end());

					for (auto index : path) {
						cout << _vertexs[index] << "->";
					}
					cout << dist[i] << endl;
				}
			}
		}

		/*
		* FloydWarshall算法可以求任意两个点(双源)之间的最短路径(正负权重都可)，使用动态规划的思想求解
		* @para vv_distance 与distance相同，不过变成了二维数组，功能从存储任意一点到起始点的距离变成了任意一点到任意一点当前的最短距离(在完成算法的所有流程后当前就会变成最终)
		* @para vv_distance 本质上与_matrix的功能别无二异，主要是为了不修改_matrix的值才不直接在用_matrix
		* 
		* @para vv_parent_path 与parent_path相同，不过变成了二维，记录一条最短路径的终点的前一个点
		*/
		void FloydWarshall(vector<vector<W>>& vv_distance, vector<vector<int>>& vv_parent_path) {
			const size_t vt_size = _vertexs.size();
			vv_distance.resize(vt_size);
			vv_parent_path.resize(vt_size);

			for (size_t i = 0; i < vt_size; ++i) {
				vv_distance[i].resize(vt_size, W_MAX);
				vv_parent_path[i].resize(vt_size, -1);
			}

			// 将直接相邻的两个点构成的边最为最初的两个点之间的最短距离
			for (size_t i = 0; i < vt_size; ++i) {
				for (size_t j = 0; j < vt_size; ++j) {
					if (_matrix[i][j] != W_MAX) {
						vv_distance[i][j] = _matrix[i][j];
						vv_parent_path[i][j] = i;
					}

					// 因为在_matrix中当i == j时，存储的值为W_MAX，所以可以进这个判断
					// 在vv_distance[i][j]中不像_matrix[i][j]中一样存储W_MAX是为了方便以后的计算
					// 当计算 i -> i + i -> k = i -> k 时就直接能相加，不需要其他处理，非常方便
					if (i == j) {
						vv_distance[i][j] = W();
						vv_parent_path[i][j] = i;
					}
				}
			}

			// 这里的动态规划是这样思考的，假设计算i -> j的最短距离
			// 我们需要比较distance[i][j] 和 distance[i][k] + distance[k][j]的大小，选取最小的那一个
			// 其中，这里的k是一个集合，包括了除了i和j之外的所有顶点
			// 表面上因为只有n个点，去除了i和j两个点之后k集合中最多只有n - 2个点。但是因为i和j只是是一个代号，其可以是任意点，可能前一秒i是a点、j是b点，后一秒i就是c点，j就是f点了
			// 所以一共需要循环n轮，因为k集合里n个点都有机会被包含进去
			// 这里的k既能当作循环的计数器，还能作为集合中任意一个点的下标，一举两得
			for (size_t k = 0; k < vt_size; ++k) {
				for (size_t i = 0; i < vt_size; ++i) {
					for (size_t j = 0; j < vt_size; ++j) {
						if (vv_distance[i][k] != W_MAX && vv_distance[k][j] != W_MAX // 要保证ik和kj之间要有(当前最短)路径
							&& vv_distance[i][k] + vv_distance[k][j] < vv_distance[i][j]) {
							vv_distance[i][k] + vv_distance[k][j] = vv_distance[i][j];

							// 新的i到j的最短路径经过了k点，所以i到j最短路劲上终点j的前一个点是k到j最短路径上的前一个点
							vv_parent_path[i][j] = vv_parent_path[k][j];
						}
					}
				}
			}

			// 打印权值和路径矩阵观察数据
			for (size_t i = 0; i < vt_size; ++i) {
				for (size_t j = 0; j < vt_size; ++j) {
					if (vv_distance[i][j] == W_MAX) {
						printf("%3c", '*');
					}
					else {
						printf("%3d", vv_distance[i][j]);
					}
				}
				cout << endl;
			}
			cout << endl;

			for (size_t i = 0; i < vt_size; ++i) {
				for (size_t j = 0; j < vt_size; ++j) {
					printf("%3d", vv_parent_path[i][j]);
				}
				cout << endl;
			}
			cout << "=================================" << endl;
		}

	private:
		void addEdgeByIndex(const size_t& src_i, const size_t dst_i, const W& weight) {
			_matrix[src_i][dst_i] = weight;

			if (Direction == false) {
				_matrix[dst_i][src_i] = weight;
			}
		}

		void _DFS(const size_t& start_i, vector<bool>& visited) {
			visited[start_i] = true;
			cout << start_i << ": " << _vertexs[start_i] << " ";

			for (size_t i = 0; i < _vertexs.size(); ++i) {
				if (_matrix[start_i][i] != W_MAX && visited[i] == false) {
					_DFS(i, visited);
				}
			}
		}

		struct Edge {
			size_t _srci;
			size_t _dsti;
			W& _weight;

			Edge(const size_t& srci, const size_t& dsti, const W& weight)
				: _srci(srci)
				, _dsti(dsti)
				, _weight(weight)
			{}

			bool operator>(const Edge& e) const {
				return _weight > e._weight; // 用权重来判断大小
			}
		};
	};
}

namespace adjacency_table {
	template<class W>
	struct Edge { // 通过Edge可以实现邻接表的入表和出表，但一般只实现出表就足够了
		size_t _src_i;
		size_t _dst_i;
		W _weight;

		Edge<W>* _next;

		Edge(const size_t& src_i, const size_t& dst_i, const W& weight)
			: _src_i(src_i)
			, _dst_i(dst_i)
			, _weight(weight)
			, _next(nullptr)
		{}
	};

	template<class V, class W = int, W W_MAX = INT_MIN, bool Direction = false>
	class Graph {
	private:
		using Edge = Edge<W>;

	private:

		vector<V> _vertexs; // 顶点集合，存储所有的顶点

		map<V, size_t> _vertex_index_map; // 记录顶点对应下标

		vector<Edge*> _table; // 邻接表

	public:
		Graph() noexcept = default;

		Graph(const V* p_vertex, size_t size) {
			_vertexs.reserve(size);
			_table.resize(size, nullptr);

			for (size_t i = 0; i < size; ++i) {
				_vertexs.push_back(p_vertex[i]); // 将点存入点集中
				_vertex_index_map[p_vertex[i]] = i + 1; // 记录点与其下标的映射关系
			}
		}

		~Graph() {
			clear();
		}

		size_t getVertexIndex(const V& vertex) const {
			auto it = _vertex_index_map.find(vertex);
			if (it == _vertex_index_map.end()) {
				throw invalid_argument("顶点不存在");
			}
			else {
				return it->second;
			}
		}

		void addEdge(const V& src, const V& dst, const W& weight) {
			size_t src_i = getVertexIndex(src);
			size_t dst_i = getVertexIndex(dst);

			Edge* edge = new Edge(src_i, dst_i, weight);
			// 头插，类似于unordered_map
			edge->_next = _table[src_i];
			_table[src_i] = edge;

			if (Direction == false) {
				Edge* edge = new Edge(dst_i, src_i, weight);
				edge->_next = _table[dst_i];
				_table[dst_i] = edge;
			}
		}

		void Print() {
			// 顶点
			for (size_t i = 0; i < _vertexs.size(); ++i) {
				cout << "[" << i << "]" << "->" << _vertexs[i] << endl;
			}
			cout << endl;

			for (size_t i = 0; i < _table.size(); ++i) {
				cout << _vertexs[i] << "[" << i << "]->";
				Edge* cur = _table[i];
				while (cur)
				{
					cout << "[" << _vertexs[cur->_dsti] << ":" << cur->_dsti << ":" << cur->_w << "]->";
					cur = cur->_next;
				}
				cout << "nullptr" << endl;
			}
		}

	private:
		void clear() {
			for (auto& bucket : _table) {
				Edge* cur = bucket;
				while (cur) {
					Edge* next = cur->_next;
					delete cur;
					cur = next;
				}

				bucket = nullptr;
			}
		}
	};
}