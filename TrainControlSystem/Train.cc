#include "Train.h"

namespace train_system {
    Train::Train(const std::string& id, const std::string& type, 
        const std::string& departure, const std::string& destination, 
        const std::string& departure_time, const std::string& arrival_time, 
        const unsigned int& max_capacity, double& price) 
        : _id(id), _type(type), _departure(departure), _destination(destination), 
        _departure_time(departure_time), _arrival_time(arrival_time), 
        _max_capacity(max_capacity), _price(price), _current_capacity(0) {}

    Train::Train(std::string&& id, std::string&& type, 
        std::string&& departure, std::string&& destination, 
        std::string&& departure_time, std::string&& arrival_time, 
        const unsigned int& max_capacity, const double& price) 
        : _id(std::move(id)), _type(std::move(type)), 
        _departure(std::move(departure)), _destination(std::move(destination)), 
        _departure_time(std::move(departure_time)), _arrival_time(std::move(arrival_time)), 
        _max_capacity(max_capacity), _price(price), _current_capacity(0) {}

    const std::string& Train::getId() const {
        return _id;
    }

    const std::string& Train::getType() const {
        return _type;
    }

    const std::string& Train::getDeparture() const {
        return _departure;
    }

    const std::string& Train::getDestination() const {
        return _destination;
    }

    const std::string& Train::getDepartureTime() const {
        return _departure_time;
    }

    const std::string& Train::getArrivalTime() const {
        return _arrival_time;
    }

    const unsigned int& Train::getMaxCapacity() const {
        return _max_capacity;
    }

    const double& Train::getPrice() const {
        return _price;
    }

    const unsigned int& Train::getCurrentCapacity() const {
        return _current_capacity;
    }

    void Train::setId(const std::string& number) {
        _id = number;
    }

    void Train::setType(const std::string& type) {
        _type = type;
    }

    void Train::setDeparture(const std::string& departure) {
        _departure = departure;
    }

    void Train::setDestination(const std::string& destination) {
        _destination = destination;
    }

    void Train::setDepartureTime(const std::string& departure_time) {
        _departure_time = departure_time;
    }

    void Train::setArrivalTime(const std::string& arrival_time) {
        _arrival_time = arrival_time;
    }

    void Train::setPrice(const double& price) {
        _price = price;
    }

    void Train::setCurrentCapacity(const unsigned int& current_capacity) {
        _current_capacity = current_capacity;
    }

    void Train::setMaxCapacity(const unsigned int& max_capacity) {
        _max_capacity = max_capacity;
    }
} //@end namespace train_system