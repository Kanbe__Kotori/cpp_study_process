#include "TrainSystem.h"
#include <iostream>
#include <string>
#include <windows.h>
#include <memory>
using namespace std;
using namespace train_system;


int main() {
    auto train_system = make_unique<TrainSystem>();
    train_system->init();
    train_system->run();
    train_system->exit();
    
    return 0;
}
