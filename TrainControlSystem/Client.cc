#include "Client.h"

namespace train_system {
    Client::Client(const std::string& name, const std::string& password, 
        const std::string& id, const std::string& phone) 
        : _real_name(name), _password(password), _nickname(id), _phone_number(phone) {}

    Client::Client(std::string&& name, std::string&& password, 
        std::string&& id, std::string&& phone) 
        : _real_name(std::move(name)), _password(std::move(password)), 
        _nickname(std::move(id)), _phone_number(std::move(phone)) {}

    const std::string& Client::getRealName() const {
        return _real_name;
    }

    const std::string& Client::getPassword() const {
        return _password;
    }

    const std::string& Client::getNickname() const {
        return _nickname;
    }

    const std::string& Client::getPhoneNumber() const {
        return _phone_number;
    }

    const std::vector<Train::ticket>& Client::getTickets() const {
        return _tickets;
    }

    void Client::setRealName(const std::string& name) {
        _real_name = name;
    }

    void Client::setPassword(const std::string& password) {
        _password = password;
    }

    void Client::setNickname(const std::string& id) {
        _nickname = id;
    }

    void Client::setPhoneNumber(const std::string& phone) {
        _phone_number = phone;
    }

    void Client::addTicket(const Train::ticket& ticket) {
        _tickets.push_back(ticket);
    }

    void Client::removeTicket(const std::string& train_id) {
        for (auto it = _tickets.begin(); it != _tickets.end(); ++it) {
            if (it->first == train_id) {
                _tickets.erase(it);
                break;
            }
        }
    }
} //@end namespace train_system