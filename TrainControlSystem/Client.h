#pragma once
#include "Train.h"
#include <string>
#include <vector>

namespace train_system {
    class Client {
    private:
        std::string _real_name; // 客户姓名, 用于找回密码等
        std::string _password; // 客户密码
        std::string _nickname; // 客户名, 作为客户的唯一标识
        std::string _phone_number; // 客户电话号码， 用于找回密码
        std::vector<Train::ticket> _tickets; // 车票列表

    public:
        Client() = default;
        Client(const Client& client) = default;
        Client(Client&& client) = default;
        Client& operator=(const Client& client) = default;
        Client& operator=(Client&& client) = default;
        ~Client() = default;

        Client(const std::string& real_name, const std::string& password, const std::string& nickname, const std::string& phone_number);
        Client(std::string&& real_name, std::string&& password, std::string&& nickname, std::string&& phone_number);

    public:
        const std::string& getRealName() const;
        const std::string& getPassword() const;
        const std::string& getNickname() const;
        const std::string& getPhoneNumber() const;
        const std::vector<Train::ticket>& getTickets() const;

        void setRealName(const std::string& real_name);
        void setPassword(const std::string& password);
        void setNickname(const std::string& nickname);
        void setPhoneNumber(const std::string& phone_number);
        void addTicket(const Train::ticket& ticket);
        void removeTicket(const std::string& train_id);
    }; //@end class Client
} //@end namespace train_system