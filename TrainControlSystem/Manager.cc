#include "Manager.h"

namespace train_system {
    Manager::Manager(const unsigned int& id, const std::string& name, const std::string& password)
        : _id(id)
        , _name(name)
        , _password(password)
    {}

    Manager::Manager(const unsigned int& id, std::string&& name, std::string&& password)
        : _id(id)
        , _name(std::move(name))
        , _password(std::move(password))
    {}

    bool Manager::operator==(const Manager& manager) const {
        return _id == manager._id && _name == manager._name && _password == manager._password;
    }

    const unsigned int& Manager::get_id() const {
        return _id;
    }

    const std::string& Manager::get_name() const {
        return _name;
    }

    const std::string& Manager::get_password() const {
        return _password;
    }
} //@end namespace train_system