#include "TrainSystem.h"

namespace train_system {
    void TrainSystem::init() {
        SetConsoleCP(65001); // 设置控制台输入编码为UTF-8
        SetConsoleOutputCP(65001); // 设置控制台输出编码为UTF-8

        loadManager();
        loadClient();
        loadTrain();
    }

    void TrainSystem::exit() {
        saveClient();
        saveManager();
        saveTrain();
        clear();
        ::exit(EXIT_SUCCESS);
    }

    void TrainSystem::pause() {
        std::cout << "按任意键继续..." << std::endl;
        getchar();
    }

    void TrainSystem::saveClient() {
        std::ofstream ofs(ClientConfigPath);
        if (!ofs.is_open()) {
            std::cerr << "无法打开客户数据文件!" << ClientConfigPath << std::endl;
            return;
        }

        for (const auto& [nickname, client] : _clients_hashmap) {
            int size = client->getTickets().size();
            ofs << nickname << " " << client->getRealName() << " " << client->getPassword() << " " << client->getPhoneNumber() << " " << size << " ";
            for (const auto& [train_id, seat_number] : client->getTickets()) {
                ofs << train_id << " " << seat_number << " ";
            }

            ofs << std::endl;
        }

        ofs.close();
    }

    void TrainSystem::saveManager() {
        std::ofstream ofs(ManagerConfigPath);
        if (!ofs.is_open()) {
            std::cerr << "无法打开管理员数据文件!" << ManagerConfigPath << std::endl;
            return;
        }

        for (const auto& [id /* 管理员id */, manager] : _managers) {
            ofs << id << " " << manager->get_name() << " " << manager->get_password() << std::endl;
        }

        ofs.close();
    }

    void TrainSystem::saveTrain() {
        std::ofstream ofs(TrainConfigPath);
        if (!ofs.is_open()) {
            std::cerr << "无法打开车次数据文件!" << TrainConfigPath << std::endl;
            return;
        }

        for (const auto& [id /* 车次id */, train] : _trains_hashmap) {
            ofs << id << " " << train->getType() << " " 
            << train->getDeparture() << " " << train->getDestination() << " " 
            << train->getDepartureTime() << " " << train->getArrivalTime() << " " 
            << train->getMaxCapacity() << " " << train->getPrice() << " " 
            << train->getCurrentCapacity() << std::endl;
        }

        ofs.close();
    }

    void TrainSystem::loadClient() {
        std::ifstream ifs(ClientConfigPath);
        if (!ifs.is_open()) {
            std::cerr << "无法打开客户数据文件!" << ClientConfigPath << std::endl;
            return;
        }

        std::string nickname;
        std::string real_name;
        std::string password;
        std::string phone_number;
        int size;
        while (ifs >> nickname >> real_name >> password >> phone_number >> size) {
            Client* client = new Client(real_name, password, nickname, phone_number);
            for (int i = 0; i < size; ++i) {
                std::string train_id;
                unsigned int seat_number;
                ifs >> train_id >> seat_number;
                client->addTicket({train_id, seat_number});
            }

            _clients_hashmap[nickname] = client;
        }

        ifs.close();
    }

    void TrainSystem::loadManager() {
        std::ifstream ifs(ManagerConfigPath);
        if (!ifs.is_open()) {
            std::cerr << "无法打开管理员数据文件!" << ManagerConfigPath << std::endl;
            return;
        }

        unsigned int id;
        std::string name;
        std::string password;
        while (ifs >> id >> name >> password) {
            _managers[id] = new Manager(id, name, password);
        }
    }
    
    void TrainSystem::loadTrain() {
        std::ifstream ifs(TrainConfigPath);
        if (!ifs.is_open()) {
            std::cerr << "无法打开车次数据文件!" << TrainConfigPath << std::endl;
            return;
        }

        std::string id;
        std::string type;
        std::string departure;
        std::string destination;
        std::string departure_time;
        std::string arrival_time;
        unsigned int max_capacity;
        double price;
        unsigned int current_capacity;
        while (ifs >> id >> type >> departure >> destination >> departure_time >> arrival_time >> max_capacity >> price >> current_capacity) {
            Train* train = new Train(id, type, departure, destination, departure_time, arrival_time, max_capacity, price);
            train->setCurrentCapacity(current_capacity);
            _trains_hashmap[id] = train;
            _trains.push_back(train);
        }

        ifs.close();
    }

    void TrainSystem::clear() {
        for (auto& [id, manager] : _managers) {
            delete manager;
        }

        for (auto& [id, client] : _clients_hashmap) {
            delete client;
        }

        for (auto& [id, train] : _trains_hashmap) {
            delete train;
        }
    }

    void TrainSystem::clearScreen() {
        system("cls");
    }

    void TrainSystem::loginMenu() {
        std::printf("\033[31m%s\033[31m\n\n", "温馨提示, 本系统暂不支持中文输入, 请使用英文输入法登录系统!");
        std::printf("\033[36m%s\033[36m\n", "欢迎使用车次信息管理系统!");
        std::printf("\033[36m%s\033[36m\n\n", "请选择您的身份登录系统:");
        std::printf("\033[0m");

        std::cout << "****************车次信息管理系统******************" << std::endl;
        std::cout << "********        1. 管理员登录             ********" << std::endl;
        std::cout << "********        2. 用户登录               ********"<< std::endl;
        std::cout << "********        3. 用户注册               ********" << std::endl;
        std::cout << "********        4. 用户找回               ********" << std::endl;
        std::cout << "********        5. 退出系统               ********" << std::endl;
        std::cout << "**************************************************" << std::endl;
    }

    bool TrainSystem::managerLogin() {
        unsigned int id;
        std::string name;
        std::string password;

        std::cout << "请输入管理员id: ";
        std::cin >> id;
        std::cout << "请输入管理员姓名: ";
        std::cin >> name;
        std::cout << "请输入管理员密码: ";
        std::cin >> password;

        auto iter = _managers.find(id);
        if (iter == _managers.end()) {
            std::cerr << "管理员id不存在!" << std::endl;
            return false;
        }

        if (iter->second->get_name() != name || iter->second->get_password() != password) {
            std::cerr << "管理员姓名或密码错误!" << std::endl;
            return false;
        }

        return true;
    }

    bool TrainSystem::clientLogin(std::string* _id) {
        std::string id;
        std::string password;

        std::cout << "请输入用户账号: ";
        std::cin >> id;
        std::cout << "请输入用户密码: ";
        std::cin >> password;

        auto iter = _clients_hashmap.find(id);
        if (iter == _clients_hashmap.end()) {
            std::cerr << "用户账号不存在" << std::endl;
            return false;
        }

        if (iter->second->getPassword() != password) {
            std::cerr << "用户密码错误!" << std::endl;
            return false;
        }

        *_id = id;

        return true;
    }

    bool TrainSystem::clientRegister() {
        std::string name;
        std::string password;
        std::string id;
        std::string phone_number;

        std::cout << "请输入您的账号名: ";
        std::cin >> id;
        std::cout << "请输入您的密码: ";
        std::cin >> password;

        std::cout << "以下信息将用于找回密码:\n" << std::endl;

        std::cout << "请输入您的真实姓名姓名:";
        std::cin >> name;
        std::cout << "请输入您的电话号码:";
        std::cin >> phone_number;

        if (_clients_hashmap.find(id) != _clients_hashmap.end()) {
            std::cerr << "您的账号名已存在!" << std::endl;
            return false;
        }

        _clients_hashmap[id] = new Client(name, password, id, phone_number);
        return true;
    }

    bool TrainSystem::clientRetrievePassword() {
        std::string name;
        std::string phone_number;

        std::cout << "请输入您的真实姓名: ";
        std::cin >> name;
        std::cout << "请输入您的电话号码: ";
        std::cin >> phone_number;

        for (const auto& [id, client] : _clients_hashmap) {
            if (client->getRealName() == name && client->getPhoneNumber() == phone_number) {
                std::cout << "您的账号是: " << id << std::endl;
                std::cout << "您的密码是: " << client->getPassword() << std::endl;

                std::cout << "请问您是否要修改密码? (y/n): ";
                char choice;
                std::cin >> choice;
                if (choice == 'y') {
                    std::string new_password;
                    std::cout << "请输入新密码: ";
                    std::cin >> new_password;
                    client->setPassword(new_password);
                }
                return true;
            }
        }

        std::cerr << "找不到您的账号信息!" << std::endl;
        return false;
    }

    void TrainSystem::run() {
        while (true) {
            clearScreen();
            loginMenu();

            int choice;
            std::cout << "请输入您的选择: ";
            std::cin >> choice;

            switch (choice) {
                case 1: {
                    if (managerLogin()) {
                        managerRun();
                    }
                    break;
                }
                case 2: {
                    std::string id;
                    if (clientLogin(&id)) {
                        clientRun(id);
                    }
                    break;
                }
                case 3: {
                    if (clientRegister()) {
                        std::cout << "注册成功!" << std::endl;
                    }
                    break;
                }
                case 4: {
                    if (clientRetrievePassword()) {
                        std::cout << "找回密码成功!" << std::endl;
                    }
                    break;
                }
                case 5: {
                    std::cout << "感谢您的使用!" << std::endl;
                    pause();
                    exit();
                    return;
                }
                default: {
                    std::cerr << "无效的选择!" << std::endl;
                    break;
                }
            }

            clearInBuffer();
            pause();
        }
    }

    void TrainSystem::managerMenu() {
        clearScreen();
        std::cout << "****************车次信息管理系统******************" << std::endl;
        std::cout << "********        1. 添加车次信息           ********" << std::endl;
        std::cout << "********        2. 删除车次信息           ********" << std::endl;
        std::cout << "********        3. 修改车次信息           ********" << std::endl;
        std::cout << "********        4. 查询车次信息           ********" << std::endl;
        std::cout << "********        5. 显示全部车次信息       ********" << std::endl;
        std::cout << "********        6. 查询客户信息           ********" << std::endl;
        std::cout << "********        7. 展示全部客户信息       ********" << std::endl;
        std::cout << "********        8. 退出登录               ********" << std::endl;
        std::cout << "********        9. 退出系统               ********" << std::endl;
        std::cout << "**************************************************" << std::endl;
    }

    void TrainSystem::managerRun() {
        while (true) {
            clearScreen();
            managerMenu();

            int choice;
            std::cout << "请输入您的选择: ";
            std::cin >> choice;

            switch(choice) {
                case 1: {
                    addTrain();
                    break;
                }
                case 2: {
                    removeTrain();
                    break;
                }
                case 3: {
                    modifyTrain();
                    break;
                }
                case 4: {
                    queryTrain();
                    break;
                }
                case 5: {
                    queryAllTrain();
                    break;
                }
                case 6: {
                    queryClient();
                    break;
                }
                case 7: {
                    queryAllClient();
                    break;
                }
                case 8: {
                    return;
                }
                case 9: {
                    std::cout << "感谢您的使用!" << std::endl;
                    pause();
                    exit();
                    return;
                }
                default: {
                    std::cerr << "无效的选择!" << std::endl;
                    break;
                }
            }

            clearInBuffer();
            pause();
        }
    }

    bool TrainSystem::addTrain() {
        std::string id;
        std::string type;
        std::string departure;
        std::string destination;
        std::string departure_time;
        std::string arrival_time;
        unsigned int max_capacity;
        double price;

        std::cout << "请输入车次编号: ";
        std::cin >> id;
        std::cout << "请输入车次类型: ";
        std::cin >> type;
        std::cout << "请输入车次出发地: ";
        std::cin >> departure;
        std::cout << "请输入车次目的地: ";
        std::cin >> destination;
        std::cout << "请输入车次出发时间: ";
        std::cin >> departure_time;
        std::cout << "请输入车次到达时间: ";
        std::cin >> arrival_time;
        std::cout << "请输入车次最大容量: ";
        std::cin >> max_capacity;
        std::cout << "请输入车次票价: ";
        std::cin >> price;

        if (_trains_hashmap.find(id) != _trains_hashmap.end()) {
            std::cerr << "车次编号已存在!" << std::endl;
            return false;
        }

        _trains_hashmap[id] = new Train(id, type, departure, destination, departure_time, arrival_time, max_capacity, price);
        _trains.push_back(_trains_hashmap[id]);

        std::cout << "添加车次成功!" << std::endl;
        return true;
    }

    bool TrainSystem::removeTrain() {
        std::string id;
        std::cout << "请输入车次id: ";
        std::cin >> id;

        auto iter = _trains_hashmap.find(id);
        if (iter == _trains_hashmap.end()) {
            std::cerr << "车次id不存在!" << std::endl;
            return false;
        }

        std::cout <<"您要删除的车次信息如下:" << std::endl;
        queryTrain(iter);

        std::cout << "请问您是否要删除该车次? (y/n): ";
        char choice;
        std::cin >> choice;
        if (choice == 'n') {
            return false;
        }

        delete iter->second;
        _trains_hashmap.erase(iter);
        _trains.erase(std::remove(_trains.begin(), _trains.end(), iter->second), _trains.end());

        std::cout << "删除车次成功!" << std::endl;
        return true;
    }

    bool TrainSystem::modifyTrain() {
        std::string id;
        std::cout << "请输入车次id: ";
        std::cin >> id;

        auto iter = _trains_hashmap.find(id);
        if (iter == _trains_hashmap.end()) {
            std::cerr << "车次id不存在!" << std::endl;
            return false;
        }

        std::string type;
        std::string departure;
        std::string destination;
        std::string departure_time;
        std::string arrival_time;
        unsigned int max_capacity;
        double price;

        std::cout << "请输入更改后车次类型: ";
        std::cin >> type;
        std::cout << "请输入更改后车次出发地: ";
        std::cin >> departure;
        std::cout << "请输入更改后车次目的地: ";
        std::cin >> destination;
        std::cout << "请输入更改后车次出发时间: ";
        std::cin >> departure_time;
        std::cout << "请输入更改后车次到达时间: ";
        std::cin >> arrival_time;
        std::cout << "请输入更改后车次最大容量: ";
        std::cin >> max_capacity;
        std::cout << "请输入更改后车次票价: ";
        std::cin >> price;

        iter->second->setType(type);
        iter->second->setDeparture(departure);
        iter->second->setDestination(destination);
        iter->second->setDepartureTime(departure_time);
        iter->second->setArrivalTime(arrival_time);
        iter->second->setMaxCapacity(max_capacity);
        iter->second->setPrice(price);


        std::cout << "修改车次成功!" << std::endl;
        return true;
    }

    bool TrainSystem::queryTrain() {
        std::string id;
        std::cout << "请输入车次编号: ";
        std::cin >> id;

        auto iter = _trains_hashmap.find(id);
        if (iter == _trains_hashmap.end()) {
            std::cerr << "车次不存在!" << std::endl;
            return false;
        }

        std::cout << "\n车次信息如下:\n" << std::endl;

        std::cout << "车次编号: " << iter->second->getId() << std::endl;
        std::cout << "车次类型: " << iter->second->getType() << std::endl;
        std::cout << "车次出发地: " << iter->second->getDeparture() << std::endl;
        std::cout << "车次目的地: " << iter->second->getDestination() << std::endl;
        std::cout << "车次出发时间: " << iter->second->getDepartureTime() << std::endl;
        std::cout << "车次到达时间: " << iter->second->getArrivalTime() << std::endl;
        std::cout << "车次最大容量: " << iter->second->getMaxCapacity() << std::endl;
        std::cout << "车次票价: " << iter->second->getPrice() << std::endl;
        std::cout << "车次当前容量: " << iter->second->getCurrentCapacity() << "\n" << std::endl;

        return true;
    }

    void TrainSystem::queryAllTrain() {

        std::cout << "您想按照什么顺序展示车次信息?" << std::endl;
        std::cout << "1. 按照车次编号排序" << std::endl;
        std::cout << "2. 按照车次类型排序" << std::endl;
        std::cout << "3. 按照车次出发地排序" << std::endl;
        std::cout << "4. 按照车次目的地排序" << std::endl;
        std::cout << "5. 按照车次出发时间排序" << std::endl;
        std::cout << "6. 按照车次到达时间排序" << std::endl;
        std::cout << "7. 按照车次最大容量排序" << std::endl;
        std::cout << "8. 按照车次票价排序" << std::endl;
        std::cout << "9. 按照车次当前容量排序" << std::endl;

        int choice;
        std::cin >> choice;

        if (choice < 1 || choice > 9) {
            std::cerr << "无效的选择!" << std::endl;
            return;
        }

        std::cerr << "sort begin" << std::endl;
        sortTrain(choice);
        std::cerr << "sort end" << std::endl;
        clearScreen();

        std::cout << "车次信息如下:\n" << std::endl;
        for (const auto& train : _trains) {
            std::cout << "车次id: " << train->getId() << std::endl;
            std::cout << "车次类型: " << train->getType() << std::endl;
            std::cout << "车次出发地: " << train->getDeparture() << std::endl;
            std::cout << "车次目的地: " << train->getDestination() << std::endl;
            std::cout << "车次出发时间: " << train->getDepartureTime() << std::endl;
            std::cout << "车次到达时间: " << train->getArrivalTime() << std::endl;
            std::cout << "车次最大容量: " << train->getMaxCapacity() << std::endl;
            std::cout << "车次票价: " << train->getPrice() << std::endl;
            std::cout << "车次当前容量: " << train->getCurrentCapacity() << std::endl;
            std::cout << std::endl;
        }
    }

    bool TrainSystem::queryClient() {
        std::string id;
        std::cout << "请输入用户id: ";
        std::cin >> id;

        auto iter = _clients_hashmap.find(id);
        if (iter == _clients_hashmap.end()) {
            std::cerr << "用户id不存在!" << std::endl;
            return false;
        }

        std::cout << "用户信息如下:" << std::endl;
        std::cout << "用户id: " << iter->second->getNickname() << std::endl;
        std::cout << "用户姓名: " << iter->second->getRealName() << std::endl;
        std::cout << "用户密码: " << iter->second->getPassword() << std::endl;
        std::cout << "用户电话号码: " << iter->second->getPhoneNumber() << std::endl;
        std::cout << "用户购票信息如下:" << std::endl;
        for (const auto& [train_id, seat_number] : iter->second->getTickets()) {
            std::cout << "车次id: " << train_id << " 座位号: " << seat_number << std::endl;
        }
        std::cout << std::endl;

        return true;
    }

    void TrainSystem::queryAllClient() {
        std::cout << "用户信息如下:" << std::endl;
        for (const auto& [id, client] : _clients_hashmap) {
            std::cout << "用户id: " << client->getNickname() << std::endl;
            std::cout << "用户姓名: " << client->getRealName() << std::endl;
            std::cout << "用户密码: " << client->getPassword() << std::endl;
            std::cout << "用户电话号码: " << client->getPhoneNumber() << std::endl;
            std::cout << "用户购票信息如下:" << std::endl;
            for (const auto& [train_id, seat_number] : client->getTickets()) {
                std::cout << "车次id: " << train_id << " 座位号: " << seat_number << std::endl;
            }
            std::cout << std::endl;
        }
    }

    void TrainSystem::sortTrain(const int& mode) {
        switch (mode) {
            case 1: {
                std::sort(_trains.begin(), _trains.end(), [](const Train* lhs, const Train* rhs) {
                    return lhs->getId() < rhs->getId();
                });
                break;
            }
            case 2: {
                std::sort(_trains.begin(), _trains.end(), [](const Train* lhs, const Train* rhs) {
                    return lhs->getType() < rhs->getType();
                });
                break;
            }
            case 3: {
                std::sort(_trains.begin(), _trains.end(), [](const Train* lhs, const Train* rhs) {
                    return lhs->getDeparture() < rhs->getDeparture();
                });
                break;
            }
            case 4: {
                std::sort(_trains.begin(), _trains.end(), [](const Train* lhs, const Train* rhs) {
                    return lhs->getDestination() < rhs->getDestination();
                });
                break;
            }
            case 5: {
                std::sort(_trains.begin(), _trains.end(), [](const Train* lhs, const Train* rhs) {
                    return lhs->getDepartureTime() < rhs->getDepartureTime();
                });
                break;
            }
            case 6: {
                std::sort(_trains.begin(), _trains.end(), [](const Train* lhs, const Train* rhs) {
                    return lhs->getArrivalTime() < rhs->getArrivalTime();
                });
                break;
            }
            case 7: {
                std::sort(_trains.begin(), _trains.end(), [](const Train* lhs, const Train* rhs) {
                    return lhs->getMaxCapacity() < rhs->getMaxCapacity();
                });
                break;
            }
            case 8: {
                std::sort(_trains.begin(), _trains.end(), [](const Train* lhs, const Train* rhs) {
                    return lhs->getPrice() < rhs->getPrice();
                });
                break;
            }
            case 9: {
                std::sort(_trains.begin(), _trains.end(), [](const Train* lhs, const Train* rhs) {
                    return lhs->getCurrentCapacity() < rhs->getCurrentCapacity();
                });
                break;
            }
            default: {
                std::cerr << "无效的选择!" << std::endl;
                break;
            }
        }
    }

    void TrainSystem::clientMenu() {
        clearScreen();
        std::cout << "****************车次信息管理系统******************" << std::endl;
        std::cout << "********        1. 查询车次信息           ********" << std::endl;
        std::cout << "********        2. 查询所有车次信息       ********" << std::endl;
        std::cout << "********        3. 购买车票               ********" << std::endl;
        std::cout << "********        4. 退票                   ********" << std::endl;
        std::cout << "********        5. 查询持有车票           ********" << std::endl;
        std::cout << "********        6. 修改信息               ********" << std::endl;
        std::cout << "********        7. 退出登录               ********" << std::endl;
        std::cout << "********        8. 退出系统               ********" << std::endl;
        std::cout << "**************************************************" << std::endl;
    }

    void TrainSystem::clientRun(const std::string& id) {
        while (true) {
            clearScreen();
            clientMenu();

            int choice;
            std::cout << "请输入您的选择: ";
            std::cin >> choice;

            switch(choice) {
                case 1: {
                    queryTrain();
                    break;
                }
                case 2: {
                    queryAllTrain();
                    break;
                }
                case 3: {
                    buyTicket(id);
                    break;
                }
                case 4: {
                    refundTicket(id);
                    break;
                }
                case 5: {
                    queryTicket(id);
                    break;
                }
                case 6: {
                    modifyClient(id);
                    break;
                }
                case 7: {
                    return;
                }
                case 8: {
                    std::cout << "感谢您的使用!" << std::endl;
                    pause();
                    exit();
                    return;
                }
                default: {
                    std::cerr << "无效的选择!" << std::endl;
                    break;
                }
            }

            clearInBuffer();
            pause();
        }
    }

    bool TrainSystem::buyTicket(const std::string& id) {
        std::string train_id;
        std::cout << "请输入车次id: ";
        std::cin >> train_id;

        auto iter = _trains_hashmap.find(train_id);
        if (iter == _trains_hashmap.end()) {
            std::cerr << "车次id不存在!" << std::endl;
            return false;
        }

        if (iter->second->getCurrentCapacity() >= iter->second->getMaxCapacity()) {
            std::cerr << "车次已满!" << std::endl;
            return false;
        }

        std::cout << "您购买的车次信息如下:" << std::endl;
        queryTrain(iter);

        std::cout << "请问您是否要购买该车次? (y/n): ";
        char choice;
        std::cin >> choice;
        if (choice == 'n') {
            return false;
        }

        unsigned int seat_number = iter->second->getCurrentCapacity() + 1;

        auto client = _clients_hashmap[id];
        client->addTicket({train_id, seat_number});
        iter->second->setCurrentCapacity(iter->second->getCurrentCapacity() + 1);

        std::cout << "购票成功!" << std::endl;
        return true;
    }

    bool TrainSystem::refundTicket(const std::string& id) {
        std::string train_id;
        std::cout << "请输入车次id: ";
        std::cin >> train_id;

        if (_trains_hashmap.find(train_id) == _trains_hashmap.end()) {
            std::cerr << "车次id不存在!" << std::endl;
            return false;
        }
        
        const std::vector<Train::ticket>& tickets = _clients_hashmap[id]->getTickets();
        for(auto begin = tickets.begin(); begin != tickets.end(); ++begin) {
            if (begin->first == train_id) {

                std::cout << "您的退票信息如下:" << std::endl;
                queryTrain(train_id);
                std::cout << "请问您是否要退票? (y/n): ";
                char choice;
                std::cin >> choice;
                if (choice == 'n') {
                    return false;
                }

                _clients_hashmap[id]->removeTicket(train_id);
                _trains_hashmap[train_id]->setCurrentCapacity(_trains_hashmap[train_id]->getCurrentCapacity() - 1);
                std::cout << "退票成功!" << std::endl;
                return true;
            }
        }

        std::cerr << "您没有购买该车次的车票!" << std::endl;
        return false;
    }

    void TrainSystem::queryTrain(const std::string& id) {
        auto iter = _trains_hashmap.find(id);

        std::cout << "车次信息如下:" << std::endl;

        std::cout << "车次编号: " << iter->second->getId() << std::endl;
        std::cout << "车次类型: " << iter->second->getType() << std::endl;
        std::cout << "车次出发地: " << iter->second->getDeparture() << std::endl;
        std::cout << "车次目的地: " << iter->second->getDestination() << std::endl;
        std::cout << "车次出发时间: " << iter->second->getDepartureTime() << std::endl;
        std::cout << "车次到达时间: " << iter->second->getArrivalTime() << std::endl;
        std::cout << "车次最大容量: " << iter->second->getMaxCapacity() << std::endl;
        std::cout << "车次票价: " << iter->second->getPrice() << std::endl;
        std::cout << "车次当前容量: " << iter->second->getCurrentCapacity() << std::endl;
    }

    void TrainSystem::queryTicket(const std::string& id) {
        const std::vector<Train::ticket>& tickets = _clients_hashmap[id]->getTickets();
        std::cout << "您的购票信息如下:\n" << std::endl;
        for(auto begin = tickets.begin(); begin != tickets.end(); ++begin) {
            std::cout << "车次id: " << begin->first << " 座位号: " << begin->second << std::endl;
            queryTrain(begin->first);
            std::cout << std::endl;
        }
    }

    bool TrainSystem::modifyClient(const std::string& id) {
        std::string name;
        std::string password;
        std::string phone_number;

        std::cout << "请输入您修改后的真实姓名: ";
        std::cin >> name;
        std::cout << "请输入您修改后的密码: ";
        std::cin >> password;
        std::cout << "请输入您修改后的电话号码: ";
        std::cin >> phone_number;

        auto iter = _clients_hashmap.find(id);
        if (iter == _clients_hashmap.end()) {
            std::cerr << "用户id不存在!" << std::endl;
            return false;
        }

        iter->second->setRealName(name);
        iter->second->setPassword(password);
        iter->second->setPhoneNumber(phone_number);
        std::cout << "修改成功!" << std::endl;

        return true;
    }

    void TrainSystem::addManager(unsigned int id, const std::string& name, const std::string& password) {
        _managers[id] = new Manager(id, name, password);
    }

    void TrainSystem::clearInBuffer() {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    void TrainSystem::queryTrain(const std::unordered_map<std::string, Train*>::iterator& it) {
        std::cout << "车次id: " << it->second->getId() << std::endl;
        std::cout << "车次类型: " << it->second->getType() << std::endl;
        std::cout << "车次出发地: " << it->second->getDeparture() << std::endl;
        std::cout << "车次目的地: " << it->second->getDestination() << std::endl;
        std::cout << "车次出发时间: " << it->second->getDepartureTime() << std::endl;
        std::cout << "车次到达时间: " << it->second->getArrivalTime() << std::endl;
        std::cout << "车次最大容量: " << it->second->getMaxCapacity() << std::endl;
        std::cout << "车次票价: " << it->second->getPrice() << std::endl;
        std::cout << "车次当前容量: " << it->second->getCurrentCapacity() << std::endl;
    }

}; //@end namespace train_system