#pragma once
#include <string>

namespace train_system {
    class Manager {
    private:
        unsigned int _id; // 管理员id, 作为管理员的唯一标识
        std::string _name; // 管理员姓名
        std::string _password; // 管理员密码

    public:
        Manager() = default;
        Manager(const Manager& manager) = default;
        Manager(Manager&& manager) = default;
        Manager& operator=(const Manager& manager) = default;
        Manager& operator=(Manager&& manager) = default;
        ~Manager() = default;

        Manager(const unsigned int& id, const std::string& name, const std::string& password);
        Manager(const unsigned int& id, std::string&& name, std::string&& password);

    public:
        bool operator==(const Manager& manager) const; // 判断两个管理员是否相等

    public:
        const unsigned int& get_id() const;
        const std::string& get_name() const;
        const std::string& get_password() const;

    }; //@end class Manager
} //@end namespace train_system