#pragma once
#include <string>
#include <utility>

namespace train_system {
    class Train {
    public:
        using ticket = std::pair<std::string, unsigned int>; // 车次编号和座位号

    private:
        std::string _id; // 车次编号, 作为车次的唯一标识
        std::string _type; // 车次类型
        std::string _departure; // 出发地
        std::string _destination; // 目的地
        std::string _departure_time; // 出发时间
        std::string _arrival_time; // 到达时间

        unsigned int _max_capacity; // 最大容量
        double _price; // 票价
        unsigned int _current_capacity; // 当前容量

    public:
        Train() = default;
        Train(const Train& train) = default;
        Train(Train&& train) = default;
        Train& operator=(const Train& train) = default;
        Train& operator=(Train&& train) = default;
        ~Train() = default;

        Train(const std::string& id, const std::string& type, 
            const std::string& departure, const std::string& destination, 
            const std::string& departure_time, const std::string& arrival_time, 
            const unsigned int& _max_capacity, double& price);

        Train(std::string&& id, std::string&& type, 
            std::string&& departure, std::string&& destination, 
            std::string&& departure_time, std::string&& arrival_time, 
            const unsigned int& max_capacity, const double& price);

    public:
        const std::string& getId() const;
        const std::string& getType() const;
        const std::string& getDeparture() const;
        const std::string& getDestination() const;
        const std::string& getDepartureTime() const;
        const std::string& getArrivalTime() const;
        const unsigned int& getMaxCapacity() const;
        const double& getPrice() const;
        const unsigned int& getCurrentCapacity() const;

        void setId(const std::string& number);
        void setType(const std::string& type);
        void setDeparture(const std::string& departure);
        void setDestination(const std::string& destination);
        void setDepartureTime(const std::string& departure_time);
        void setArrivalTime(const std::string& arrival_time);
        void setPrice(const double& price);
        void setCurrentCapacity(const unsigned int& current_capacity);
        void setMaxCapacity(const unsigned int& max_capacity);
        
    }; //@end class Train
} //@end namespace train_system