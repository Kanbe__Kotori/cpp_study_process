#pragma once
#include "Manager.h"
#include "Client.h"
#include "Train.h"
#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>
#include <windows.h>
#include <fstream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>

namespace train_system {
    class TrainSystem {
    private: // 数据文件路径
        static constexpr const char* ManagerConfigPath = "manager.txt"; // 管理员数据文件路径
        static constexpr const char* ClientConfigPath = "client.txt"; // 客户数据文件路径
        static constexpr const char* TrainConfigPath = "train.txt"; // 车次数据文件路径

    private:
        std::unordered_map<unsigned int, Manager*> _managers; // 管理员列表
        std::unordered_map<std::string, Client*> _clients_hashmap; // 客户列表, 用于查询
        std::unordered_map<std::string, Train*> _trains_hashmap; // 车次列表, 用于查询
        std::vector<Train*> _trains; // 车次列表, 用于遍历等

    public:
        TrainSystem() = default;
        TrainSystem(const TrainSystem& train_system) = delete;
        TrainSystem(TrainSystem&& train_system) = default;
        TrainSystem& operator=(const TrainSystem& train_system) = delete;
        TrainSystem& operator=(TrainSystem&& train_system) = default;
        ~TrainSystem() = default;

    public:
        void init(); // 初始化系统
        void run(); // 运行系统
        void exit(); // 退出系统


    private:

        void clear(); // 清空系统
        void clearScreen(); // 清空屏幕
        void pause(); // 暂停系统
        bool queryTrain(); // 查询车次
        void queryAllTrain(); // 查询所有车次
        void sortTrain(const int& mode); // 排序车次
        void addManager(unsigned int id, const std::string& name, const std::string& password); // 添加管理员
        void clearInBuffer(); // 清空输入缓冲区
        void queryTrain(const std::unordered_map<std::string, Train*>::iterator& it); // 查询车次

    private:
        void saveManager(); // 保存管理员数据
        void saveClient(); // 保存客户数据
        void saveTrain(); // 保存车次数据
        void loadManager(); // 加载管理员数据
        void loadClient(); // 加载客户数据
        void loadTrain(); // 加载车次数据
        
    private:
        void loginMenu(); // 显示登录菜单
        bool managerLogin(); // 管理员登录
        bool clientLogin(std::string* id); // 客户登录
        bool clientRegister(); // 客户注册
        bool clientRetrievePassword(); // 客户找回密码

    private:
        void managerMenu(); // 显示管理员菜单
        void managerRun(); // 执行管理员流程
        bool addTrain(); // 添加车次
        bool removeTrain(); // 删除车次
        bool modifyTrain(); // 修改车次
        bool queryClient(); // 查询客户
        void queryAllClient(); // 查询所有客户

    private:
        void clientMenu(); // 显示客户菜单
        void clientRun(const std::string& id); // 执行客户流程
        bool buyTicket(const std::string& id); // 购买车票
        bool refundTicket(const std::string& id); // 退票
        void queryTicket(const std::string& id); // 查询车票
        bool modifyClient(const std::string& id); // 修改客户信息
        void queryTrain(const std::string& id); // 查询车次
        
    }; //@end class TrainSystem
} //@end namespace train_system