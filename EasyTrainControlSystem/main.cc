#include "TrainSystem.h"
#include <iostream>
#include <string>
#include <memory>

using namespace std;
using namespace train_system;

int main() {
    unique_ptr<TrainSystem> train_system(new TrainSystem());
    train_system->initSystem(); // 初始化系统

    int count = 3;
    if (!train_system->login(count)) {
        cout << "登录失败，系统即将关闭。" << endl;
        train_system->pauseSystem();
        train_system->closeSystem();
        return 0;
    }

    train_system->run(); // 运行系统

    train_system->closeSystem(); // 关闭系统

    return 0;
}