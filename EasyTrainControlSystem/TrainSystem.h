#pragma once

#include "Train.h"
#include "Manager.h"
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <clocale>
#include <windows.h>

namespace train_system {
    using namespace std;

    class TrainSystem {
    private:
        unordered_map<string, Train> _trains;
        unordered_map<string, Manager> _managers;
        static const string TrainFileName;
        static const string ManagerFileName;

    public:
        TrainSystem() = default;
        TrainSystem(const TrainSystem& train_system) = delete;
        TrainSystem(TrainSystem&& train_system) = delete;
        TrainSystem& operator=(const TrainSystem& train_system) = delete;
        TrainSystem& operator=(TrainSystem&& train_system) = delete;
        ~TrainSystem() = default;

    public:
        void initSystem();
        bool login(int count);
        void run();
        void closeSystem();
        void pauseSystem();
        
    private:
        bool addTrain(const string& number, const string& departure_station, const string& arrival_station, const string& departure_time);
        bool addTrain(string&& number, string&& departure_station, string&& arrival_station, string&& departure_time);

        bool deleteTrain(const string& number);

        bool updateTrain(const string& number, const string& departure_station, const string& arrival_station, const string& departure_time);
        bool updateTrain(string&& number, string&& departure_station, string&& arrival_station, string&& departure_time);

        void showTrain(const string& number) const;
        void showAllTrain() const;

    private:
        void menu();
        void clear();
        bool saveTrain(const string& file_name) const;
        bool loadTrain(const string& file_name);
        bool saveManager(const string& file_name) const;
        bool loadManager(const string& file_name);
        bool login(const string& id, const string& password);
        void addManager(const string& id, const string& password);
    }; //@end class TrainSystem
} //@end namespace train_system