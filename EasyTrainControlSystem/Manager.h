#pragma once

#include <string>

namespace train_system {
    using namespace std;

    class Manager {
    private:
        string _id;
        string _password;

    public:
        Manager() = default;
        Manager(const Manager& manager) = default;
        Manager(Manager&& manager) = default;
        Manager& operator=(const Manager& manager) = default;
        Manager& operator=(Manager&& manager) = default;
        ~Manager() = default;

        Manager(const string& id, const string& password)
            : _id(id)
            , _password(password)
        {}

        Manager(string&& id, string&& password)
            : _id(std::move(id))
            , _password(std::move(password))
        {}

    public:
        bool operator==(const Manager& manager) const {
            return _id == manager._id && _password == manager._password;
        }

    public:
        const string& getId() const {
            return _id;
        }

        const string& getPassword() const {
            return _password;
        }
    };
} //@end namespace train_system