#pragma once

#include <string>

namespace train_system {
    using namespace std;

    class Train {
    private:
        string _number;
        string _departure_station;
        string _arrival_station;
        string _departure_time;

    public:
        Train() = default;
        Train(const Train& train) = default;
        Train(Train&& train) = default;
        Train& operator=(const Train& train) = default;
        Train& operator=(Train&& train) = default;
        ~Train() = default;

        Train(const string& number, const string& departure_station, const string& arrival_station, const string& departure_time)
            : _number(number)
            , _departure_station(departure_station)
            , _arrival_station(arrival_station)
            , _departure_time(departure_time)
        {}

        Train(string&& number, string&& departure_station, string&& arrival_station, string&& departure_time)
            : _number(std::move(number))
            , _departure_station(std::move(departure_station))
            , _arrival_station(std::move(arrival_station))
            , _departure_time(std::move(departure_time))
        {}

    public:
        const string& getNumber() const {
            return _number;
        }

        const string& getDepartureStation() const {
            return _departure_station;
        }

        const string& getArrivalStation() const {
            return _arrival_station;
        }

        const string& getDepartureTime() const {
            return _departure_time;
        }

        void setNumber(const string& number) {
            _number = number;
        }

        void setDepartureStation(const string& departure_station) {
            _departure_station = departure_station;
        }

        void setArrivalStation(const string& arrival_station) {
            _arrival_station = arrival_station;
        }

        void setDepartureTime(const string& departure_time) {
            _departure_time = departure_time;
        }
    }; //@end class Train
} //@end namespace train_system