#include <iostream>
#include <cstdio>
#include <typeinfo>
using namespace std;

//引用做参数
void swap_cpp(int& a, int& b)
{
    int tmp = a;
    a = b;
    b = tmp;
}

int* static_test()
{
    static int a = 0;
    ++a;
    return &a;
}

int& quote_test()
{
    static int a = 0;
    ++a;
    return a;
}

//int& Add(int a, int b)
//{
//    int c = a + b;
//    return c;
//}

int main()
{
    ////printf("%p\n",&1);
    //const int& tmp = 1;
    //printf("%p\n", &tmp);
    
//    int a = 10;
//    int b = 20;
//    swap_cpp(a, b);
//    cout << "a = " << a << " b = " << b << endl;

    //cout << *static_test() << endl;
    //(*static_test()) += 10;
    //static_test();
    //static_test();
    //static_test();
    //static_test();
    //cout << *static_test() << endl;

    //cout << "-------------------------------------" << endl;

    //int& b = quote_test();
    //cout << b << endl;
    //b += 20;
    //quote_test();
    //quote_test();
    //quote_test();
    //quote_test();
    //cout << b << endl;
    //return 0;
    
    int a = 10;
    double d = 20.0;
    float f = 11.11F;
    unsigned short us = 5;

    cout << typeid(a).name() << endl;
    cout << typeid(d).name() << endl;
    cout << typeid(f).name() << endl;
    cout << typeid(us).name() << endl;


}
