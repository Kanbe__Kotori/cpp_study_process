//牛客HJ73 计算日期到天数转换

#include <iostream>
using namespace std;

class Solution{
public:
    Solution(int year, int month, int day)
    :_year(year)
    ,_month(month)
    ,_day(day)
    {}

    [[nodiscard]] bool isLeapYear(const int year) const
    {
        if (year % 400 == 0 || (year % 4 == 0) && (year % 100 != 0))
        {
            return true;
        }

        return false;
    }

    [[nodiscard]] int getMonthDay(const int year, const int month) const
    {
        int monthDays[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if(isLeapYear(year) && month == 2)
        {
            return 29;
        }

        return monthDays[month];
    }

    void datePrint() const
    {
        int n = 0;
        for (int i = 1; i < _month; ++i)
        {
            n += getMonthDay(_year, i);
        }

        n += _day;

        cout << n << endl;
    }

    void test()
    {
        cout << _year << " " << _month << " " << _day << endl;
    }
private:
    int _year;
    int _month;
    int _day;
};

int main()
{
    int year, month, day;
    cin >> year >> month >>day;
    Solution s(year, month, day);
    //s.test();
    s.datePrint();
    return 0;
}