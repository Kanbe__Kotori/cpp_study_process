// #include <Windows.h>
// #include <iostream>
// using namespace std;

// int main()
// {
//     SYSTEMTIME st;
//     GetLocalTime(&st);
//     cout << st.wYear << "-" << st.wMonth << "-" << st.wDay << " " << st.wHour << ":" << st.wMinute << ":" << st.wSecond << endl;
//     return 0;
// }

// #include <iostream>
// using namespace std;

// void hanoi(int n, char from, char aux, char to) {
//     if (n == 1) {
//         cout << "Move disk 1 from rod " << from << " to rod " << to << endl;
//         return;
//     }
//     hanoi(n - 1, from, to, aux);
//     cout << "Move disk " << n << " from rod " << from << " to rod " << to << endl;
//     hanoi(n - 1, aux, from, to);
// }

// int main() {
//     int n = 3; // Number of disks
//     hanoi(n, 'A', 'B', 'C');  // A, B and C are names of rods
//     return 0;
// }

// #include <iostream>
// #include <vector>
// #include <string>

// using namespace std;

// struct Train {
//     string number;
//     string departure;
//     string destination;
// };

// vector<Train> trains;

// void addTrain() {
//     Train train;
//     cout << "Enter train number: ";
//     cin >> train.number;
//     cout << "Enter departure station: ";
//     cin >> train.departure;
//     cout << "Enter destination station: ";
//     cin >> train.destination;
//     trains.push_back(train);
// }

// void viewTrains() {
//     for (Train train : trains) {
//         cout << "Train Number: " << train.number << ", Departure: " << train.departure << ", Destination: " << train.destination << endl;
//     }
// }

// void findTrain() {
//     string number;
//     cout << "Enter train number: ";
//     cin >> number;
//     for (Train train : trains) {
//         if (train.number == number) {
//             cout << "Train Number: " << train.number << ", Departure: " << train.departure << ", Destination: " << train.destination << endl;
//             return;
//         }
//     }
//     cout << "Train not found." << endl;
// }

// void deleteTrain() {
//     string number;
//     cout << "Enter train number to delete: ";
//     cin >> number;
//     for (int i = 0; i < trains.size(); i++) {
//         if (trains[i].number == number) {
//             trains.erase(trains.begin() + i);
//             cout << "Train deleted." << endl;
//             return;
//         }
//     }
//     cout << "Train not found." << endl;
// }

// int main() {
//     int choice;
//     while (true) {
//         cout << "1. Add Train\n2. View Trains\n3. Find Train\n4. Exit\nEnter your choice: ";
//         cin >> choice;
//         switch (choice) {
//             case 1:
//                 addTrain();
//                 break;
//             case 2:
//                 viewTrains();
//                 break;
//             case 3:
//                 findTrain();
//                 break;
//             case 4:
//                 return 0;
//             default:
//                 cout << "Invalid choice." << endl;
//         }
//     }
// }