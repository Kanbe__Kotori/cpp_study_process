#define _CRT_SECURE_NO_WARNINGS 1
////#pragma warning(disable:6031)
//
////#include <stdio.h>
////
////int main()
////{
////	printf("%d\n", -10 % -3);
////	return 0;
////}
//
////#include <iostream>
////using namespace std;
////
////int main()
////{
////	//cout << 1 / 0 << endl;
////	//cout << -10 % -3 << endl;
////	//int a = 10;
////	//double d = 12.5;
////	////a += d;
////	//cout << a + d << endl;
////
////	//int a = 10;
////	//a = a++;
////	//cout << a << endl;
////	//a = ++a;
////	//cout << a << endl;
////
////	//cout << "Hello"" ""World!" << endl;
////	//double a = 10.0;
////	//switch (a)
////	//{
////	//case 1:
////	//	cout << 1 << endl;
////	//	break;
////	//case 2:
////	//	cout << 2 << endl;
////	//	break;
////	//default:
////	//	cout << "no one" << endl;
////	//	break;	//}
////	//int a = 0;
////	//switch (a)
////	//{
////	//case 1:
////	//	break;
////	////case 1:
////	////	break;
////
////	//default:
////	//	break;
////	//}
////	return 0;
////}
//
//#include <stdio.h>
//
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//
//	int count = 0;
//
//	int i = 0;
//	//因为num的数据类型为int 大小为4个字节 共32个比特位 所以最多要移动31个比特位 故i < =31
//	//如果num的数据类型位long long 则i <= 63
//	for (i = 0; i <= 31; ++i)
//	{
//		if ((num >> i & 1) == 1)
//		{
//			++count;
//		}
//	}
//
//	printf("%d\n", count);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//
//	int count = 0;
//
//	while (num != 0)
//	{
//		num = num & (num - 1);
//		++count;
//	}
//
//	printf("%d\n", count);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	printf("%p %p\n", &a, &b);
//	printf("%Id\n", &a - &b);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int i;
//	for (i = 1; i <= 10; i++)
//		printf("%d ", i);
//	return 0;
//}

//#include <stdio.h>
//#include <Windows.h>
//
//int main()
//{
//	int i = 0;
//	for (i = 10; i >= 0; --i)
//	{
//		printf("%-2d\r", i);
//		Sleep(1000);
//	}
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	char arr1[] = "abc";
//	char arr2[] = {'a', 'b','c'};
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int arr[10] = { 0 };
//	printf("%p\n", arr);
//	printf("%p\n", &arr[0]);
//	printf("%p\n", &arr);
//
//	return 0;
//}

//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int a = 0;
//	int& aa = a;
//	++aa;
//	cout << a << endl;
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int arr[][3] = { {0}, {0}, {0} };
//	printf("%zu", sizeof(arr) / sizeof(arr[0][0]));
//	return 0;
//}

//#include <Windows.h>
//#include <stdio.h>
//
//int main()
//{
//	printf("1\n");
//	Sleep(1000);
//	printf("2\n");
//	return 0;
//}

//#include <stdio.h>
//#include <Windows.h>
//
//int main()
//{
//	for (int i = 10; i >= 1; --i)
//	{
//		printf("%-2d\r", i);
//		fflush(stdout);
//		Sleep(1000);
//	}
//	return 0;
//}

//#include <stdio.h>
//#include <time.h>
//
//int fib(int n)
//{
//	if (n == 1 || n == 2)
//	{
//		return 1;
//	}
//
//	return fib(n - 1) + fib(n - 2);
//}
//
//int main()
//{
//	int begin10 = clock();
//	fib(40);
//	int end10 = clock();
//	printf("%d\n", end10 - begin10);
//
//	int begin20 = clock();
//	fib(45);
//	int end20 = clock();
//	printf("%d\n", end20 - begin20);
//
//	int begin30 = clock();
//	fib(50);
//	int end30 = clock();
//	printf("%d\n", end30 - begin30);
//
//	return 0;
//}

#include <iostream>

void print(int n = 0)
{
	std::cout << n << std::endl;
}

int main()
{
	print(1);
	print();
	return 0;
}
