//牛客 HJ1 字符串最后一个单词的长度
//#include <iostream>
//#include <string>
//using namespace std;
//
//int main()
//{
//    string words;
//
//    //cin读取到token结束 类似于java的 .next()
//    //getline读取完一行 类似于java的 .nextLine()
//    getline(cin, words);
//
//    size_t pos = words.rfind(" ");
//
//    //有可能字符串就只有一个单词 字符串最后一个单词的长度就是字符串的长度
//    //此时 是找不到空格的 这样的话 pos == npos
//    //所以 就不做出一下判断
//    //if (pos != string::npos)
//    //{
//    //    cout << (words.size() - (pos + 1)) << endl;
//    //}
//
//    //string类型size()的值 相当于字符串最后一个字母的后面一个位置
//    //类似于\0的位置 但string不以\0结尾
//    //而pos是最后一个单词前空格的位置 因此pos + 1就是最后一个单词第一个字母的位置
//    //最后一个单词的长度就为 size - (pos - 1)
//    cout << (words.size() - (pos + 1)) << endl;
//    return 0;
//}

//LeetCode125.验证回文串
//class Solution {
//public:
//    bool isPalindrome(string s) {
//        int left = 0;
//        int right = s.size() - 1;
//
//        while (left < right)
//        {
//            //isalnum()用于判断一个字符是否属于数字或字母(大小写都可)
//            while (left < right && !isalnum(s[left]))
//            {
//                ++left;
//            }
//            while (left < right && !isalnum(s[right]))
//            {
//                --right;
//            }
//
//            if (tolower(s[left]) != tolower(s[right]))
//            {
//                return false;
//            }
//            ++left;
//            --right;
//        }
//
//        return true;
//    }
//};

//暴力求解
//LeetCode415.字符串相加
//#include <iostream>
//#include <string>
//using namespace std;
//
//class Solution {
//public:
//    string addStrings(string num1, string num2) {
//        //找到较长的字符串
//        string& longerStr = num1.size() > num2.size() ? num1 : num2;
//        int end1 = num1.size() - 1, end2 = num2.size() - 1, longerEnd = longerStr.size() - 1;
//
//        //表示进位
//        int flag = 0;
//
//        //要求两个字符串都不遍历结束
//        while (end1 >= 0 && end2 >= 0)
//        {
//            int ret = num1[end1] + num2[end2] - 2 * '0' + flag;
//
//            //结果大于等于10就需要进位
//            flag = ret >= 10 ? 1 : 0;
//            ret %= 10;
//            longerStr[longerEnd] = ret + '0';
//            --end1;
//            --end2;
//            --longerEnd;
//        }
//
//        //在短的结束后再判断长的某一位加上进位后需不需要再进位
//        //如 1 + 99999
//        while (flag != 0 && longerEnd >= 0)
//        {
//            int ret = longerStr[longerEnd] - '0' + flag;
//            flag = ret >= 10 ? 1 : 0;
//            ret %= 10;
//            longerStr[longerEnd] = ret + '0';
//            --longerEnd;
//        }
//
//        //如 1 + 99999 虽然两个字符串都遍历结束了 但仍有一位需要进位
//        //此时就头插一个
//        if (flag != 0)
//        {
//            longerStr.insert(longerStr.begin(), '1');
//        }
//
//        return longerStr;
//    }
//};
//
//int main()
//{
//    cout << Solution().addStrings("456", "77") << endl;
//    return 0;
//}

//class Solution {
//public:
//    string addStrings(string num1, string num2) {
//        //end1和end2指向两个字符串的尾部
//        int end1 = num1.size() - 1, end2 = num2.size() - 1;
//        int flag = 0;
//
//        //创建空字符串来存储答案
//        string ans("");
//
//        //只要两个字符串中有一个没有遍历完就继续
//        //只要有进位就继续
//        while (end1 >= 0 || end2 >= 0 || flag != 0)
//        {
//            //当继续相加时 如果一个字符串已经遍历完了(对应的end < 0) 此时进行访问就会报错
//            //这样就把已经遍历完的字符串的不存在的位用0来填充 因为任何数加0都等于它本身
//            int ret1 = end1 >= 0 ? num1[end1] - '0' : 0;
//            int ret2 = end2 >= 0 ? num2[end2] - '0' : 0;
//            int ret = ret1 + ret2 + flag;
//
//            //ret % 10的结果是数字 我们要加'0'把它转成字符
//            ans += (ret % 10 + '0');
//            flag = ret / 10;
//            --end1;
//            --end2;
//        }
//
//        //因为我们是在记录答案的字符串尾插的(因为字符串是用顺序表实现的 头插的效率不高)
//        //所以最后的答案应该要翻转过来
//        reverse(ans.begin(), ans.end());
//        return ans;
//    }
//};