#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <iostream>
using namespace std;

class Date
{
public:
	Date(int year = 0, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	bool operator==(const Date& date)
	{
		return _year == date._year
			&& _month == date._month
			&& _day == date._day;
	}

	bool operator>(const Date& date)
	{
		//return _year > date._year
		//	&& _month > date._month
		//	&& _day > date._day;

		//return _year > date._year
		//	|| _month > date._month
		//	|| _day > date._day;

		//if (_year > date._year)
		//{
		//	return true;
		//}
		//else if (_year < date._year)
		//{
		//	return false;
		//}
		//else
		//{
		//	if (_month > date._month)
		//	{
		//		return true;
		//	}
		//	else if (_month < date._month)
		//	{
		//		return false;
		//	}
		//	else
		//	{
		//		if (_day > date._day)
		//		{
		//			return true;
		//		}
		//		else
		//		{
		//			return false;
		//		}
		//	}
		//}

		if (_year > date._year) 
		{
			return true;
		} 
		else if (_year == date._year && _month > date._month) 
		{
			return true;
		}
		else if (_year == date._year && _month == date._month && _day > date._day) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1(2024, 1, 7);
	Date d2(2025, 1, 3);

	//bool result = d1 == d2;
	//cout << result << endl;
	//cout << (d1 == d2) << endl;
	//cout << d1.operator==(d2) << endl;

	//cout << (d1 > d2) << endl;
	//cout << " " + 1 << endl;
	return 0;
}