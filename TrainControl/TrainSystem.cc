#include "TrainSystem.h"

TrainSystem::TrainSystem() noexcept {
	srand(static_cast<unsigned int>(time(nullptr)) ^ 0X1ade213);

	// todo...
}

void TrainSystem::startMenu() noexcept {
	//cout << "欢迎来到列车管理系统" << endl;
	//cout << "0. 退出系统" << endl;
	//cout << "1. 用户身份登录" << endl;
	//cout << "2. 管理员身份登录" << endl;
	//cout << "3. 注册用户身份" << endl;

	cout << "*******************************************" << endl;
	cout << "********    欢迎来到列车管理系统   ********" << endl;
	cout << "********    0.   退出系统          ********" << endl;
	cout << "********    1.  注册用户身份       ********" << endl;
	cout << "********    2.  用户身份登录       ********" << endl;
	cout << "********    3. 管理员身份登录      ********" << endl;
	cout << "*******************************************" << endl;
}

void TrainSystem::exitSystem() noexcept {
	cout << "系统退出... 欢迎您的再次使用！" << endl;
	// todo...
	exit(0);
}

//// 生成前四位是字母，后一位是数字的五位数验证码
//string TrainSystem::makeAuthCode() noexcept {
//	static pair<const string, const string> auth_code_graphs = getAuthCodeGraph();
//	static const string nums = move(auth_code_graphs.first);
//	static const string alphas = move(auth_code_graphs.second);
//
//	string auth_code;
//	for (int i = 0; i < 4; ++i) {
//		auth_code += alphas[rand() % alphas.size()];
//	}
//
//	auth_code += nums[rand() % nums.size()];
//
//	return auth_code;
//}

//// 生成验证码表
//pair<const string, const string> TrainSystem::getAuthCodeGraph() noexcept {
//	string nums, alphas;
//	for (int i = 0; i <= 9; ++i) {
//		nums += static_cast<char>(i + '0');
//	}
//	for (int i = 'A'; i <= 'Z'; ++i) {
//		alphas += static_cast<char>(i);
//		alphas += static_cast<char>(i + 32);
//	}
//
//	return make_pair(nums, alphas);
//}

// 生成一个n位的验证码
string TrainSystem::makeAuthCode(const int& code_length) noexcept {
	static const vector<char> auth_code_graph = getAuthCodeGraph();
	static const size_t auth_code_graph_length = auth_code_graph.size();
	string auth_code;

	//const int code_length = 5;

	for (int i = 0; i < code_length; ++i) {
		auth_code += auth_code_graph[rand() % auth_code_graph_length];
	}

	return auth_code;
}

// 获取验证码表
vector<char> TrainSystem::getAuthCodeGraph() noexcept {
	vector<char> auth_code_graph;
	// 记住这里要reserve而不是resize！！！
	auth_code_graph.reserve(10 + 26 * 2); // 0 - 10 && 'a' - 'z' && 'A' - 'Z')

	for (int i = '0'; i <= '9'; ++i) {
		auth_code_graph.push_back(i);
	}

	for (int i = 'A'; i <= 'Z'; ++i) {
		auth_code_graph.push_back(i);
		auth_code_graph.push_back(i + 32);
	}

	return auth_code_graph;
}

bool TrainSystem::clientRegister() noexcept {
	string user_name, password, ID_number, telephone;

	cout << "请输入用户名，用户名中不能包含空格" << endl;
	cin >> user_name;

	cout << "请输入密码，密码中应包含字母和数字" << endl;
	while (true) {
		cin >> password;

		if (Person::isPasswordInvalid(password)) {
			cout << "您的密码不符合要求，请重新输入！" << endl;
		}
		else {
			break;
		}
	}

	//todo ID_number, telephone的读取和验证
}