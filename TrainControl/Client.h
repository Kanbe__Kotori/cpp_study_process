#pragma once
#include "Person.h"
#include <string>

class Client final : public Person {
private:
	string _ID_code; // 身份证号
	string _telephone; // 电话号码

public:
	Client() noexcept = default;

	Client(const string& user_name, const string& password, const string& ID_code, const string& telephone)
		: Person(user_name, password)
		, _ID_code(ID_code)
		, _telephone(telephone)
	{}

	Client(string&& user_name, string&& password, string&& ID_code, string&& telephone)
		: Person(user_name, password)
		, _ID_code(ID_code)
		, _telephone(telephone)
	{}

	Client(const Client& p) = delete;
	Client& operator=(const Client& p) = delete;

	Client(Client&& p) noexcept = default;
	Client& operator=(Client&& p) noexcept = default;

	virtual ~Client() noexcept = default;

public:
	virtual void userMenu() noexcept;
	static bool isIdCodeInvalid(const string& ID_code) noexcept;
};