#pragma once
#include <string>
#include <utility>
using namespace std;

class Train {
private:
	string _train_number; // 火车编号
	string _departure_place; // 出发地
	string _destination; // 目的地
	string _departure_time; // 出发时间
	int _capacity; // 火车容量
	int _size; // 目前已容纳多少人(有多少人买了票)

public:
	Train() noexcept = default;
	Train(const string& train_number, const string& departure_place, const string& destination, const string& departure_time, const int& capacity) noexcept
		: _train_number(train_number)
		, _departure_place(departure_place)
		, _destination(destination)
		, _departure_time(departure_time)
		, _capacity(capacity)
		, _size(0)
	{}

public:

};