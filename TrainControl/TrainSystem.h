#pragma once
#include "Client.h"
#include "Manager.h"
#include "Person.h"
#include "Train.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <Windows.h>
using namespace std;

enum class StartMenuChoice {
	EXIT,
	CLIENT_REGISTER,
	CLIENT_LOGIN,
	MANAGER_LOGIN,
};

class TrainSystem {
private:
	unordered_map<string, Train> _train_hash_map; // 存储列车信息
	unordered_map<string, Manager> _manager_hash_map; // 存储管理员信息
	unordered_map<string, Client> _client_hash_map; // 存储用户信息

public:
	TrainSystem() noexcept;
	TrainSystem(const TrainSystem& ts) = delete;
	TrainSystem& operator=(const TrainSystem& ts) = delete;
	TrainSystem(TrainSystem&& ts) noexcept = default;
	TrainSystem& operator=(TrainSystem&& ts) noexcept = default;
	~TrainSystem() noexcept = default;

public:
	void startMenu() noexcept;
	bool clientRegister() noexcept;
	Person* login(const StartMenuChoice& choice) noexcept;
	void exitSystem() noexcept;

private:
	static string makeAuthCode(const int& code_length = 5) noexcept;
//	static pair<const string, const string> getAuthCodeGraph() noexcept;
	static vector<char> getAuthCodeGraph() noexcept;
};