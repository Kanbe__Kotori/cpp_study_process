#include "Client.h"

// 判断身份证号是否合法
bool Client::isIdCodeInvalid(const string& ID_code) noexcept {
	if (ID_code.size() != 18) return false;

	// 身份证校验码算法 https://mbd.baidu.com/newspage/data/dtlandingsuper?nid=dt_4490467478012473602
	int coefficient[17] = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 }; // 身份证前17位数字的系数
	char checkout[17] = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' }; // 经过处理后第18位校验码

	int sum = 0;
	for (int i = 0; i < 17; ++i) {
		sum += (ID_code[i] - '0') * coefficient[i];
	}

	return ID_code[17] != checkout[sum % 11];
}
