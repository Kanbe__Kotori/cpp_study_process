#pragma once
#include "Person.h"

class Manager final : public Person {
private:
	// 没有什么要额外添加的成员变量

public:
	Manager() noexcept = default;

	Manager(const string& user_name, const string& password) noexcept
		: Person(user_name, password)
	{}

	Manager(string&& user_name, string&& password) noexcept
		: Person(user_name, password)
	{}

	Manager(const Manager& p) = delete;
	Manager& operator=(const Manager& p) = delete;

	Manager(Manager&& p) noexcept = default;
	Manager& operator=(Manager&& p) noexcept = default;

	virtual ~Manager() noexcept = default;

public:
	virtual void userMenu() noexcept;
};