#pragma once
#include <string>
#include <cctype>
using namespace std;

class Person {
protected:
	string _user_name; // 用户账号
	string _password; // 用户密码

public:
	Person() noexcept = default;

	Person(const string& user_name, const string& password) noexcept
		: _user_name(user_name)
		, _password(password)
	{}

	Person(string&& user_name, string&& password) noexcept
		: _user_name(user_name)
		, _password(password)
	{}

	Person(const Person& p) = delete;
	Person& operator=(const Person& p) = delete;

	Person(Person&& p) noexcept = default;
	Person& operator=(Person&& p) noexcept = default;

	virtual ~Person() noexcept = default;

public:
	virtual void userMenu() noexcept = 0;

	// 有效的密码应包括字母和数字
	static bool isPasswordInvalid(string password) noexcept {
		//return !hasAlpha(password) || !hasDigit(password);
		return !(hasAlpha(password) && hasDigit(password));
	}

private:
	static bool hasAlpha(const string& str) noexcept {
		for (const auto& ch : str) {
			if (isalpha(ch)) {
				return true;
			}
		}

		return false;
	}

	static bool hasDigit(const string& str) noexcept {
		for (const auto& ch : str) {
			if (isdigit(ch)) {
				return true;
			}
		}

		return false;
	}
};