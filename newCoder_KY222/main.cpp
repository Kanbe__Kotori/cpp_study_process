//牛客KY222 打印日期
//给出年分m和一年中的第n天，算出第n天是几月几号。
//这些题目都是cout的格式控制难 哪有难题

#include <iostream>
#include <iomanip>
using namespace std;

class Solution{
public:
    Solution(const int year, const int num)
        : _year(year)
        , _num(num)
    {}

    [[nodiscard]] bool isLeapYear(const int year) const
    {
        if (year % 400 == 0 || (year % 4 == 0) && (year % 100 != 0))
        {
            return true;
        }

        return false;
    }

    [[nodiscard]] int getMonthDay(const int year, const int month) const
    {
        int monthDays[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if(isLeapYear(year) && month == 2)
        {
            return 29;
        }

        return monthDays[month];
    }

    void dateCal()
    {
        int month = 1;
        while (_num > getMonthDay(_year, month))
        {
            _num -= getMonthDay(_year, month);
            ++month;
        }

        cout << _year << "-" << setw(2) << setfill('0') << month << "-" << setw(2)
        << setfill('0')<< _num << endl;
    }

private:
    int _year;
    int _num;
};

int main()
{
    int year, num;
    while (scanf("%d %d", &year, &num) != EOF)
    {
        Solution s(year, num);
        s.dateCal();
    }
    return 0;
}