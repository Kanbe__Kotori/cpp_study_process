//todo: LeetCode58. 最后一个单词的长度
//* ver1
class Solution {
public:
    int lengthOfLastWord(string s) {
        size_t end = s.size() - 1;
        while (s[end] == ' ') {
            s.pop_back();
            --end;
        }
        size_t begin = s.find_last_of(" ");
        if (begin == string::npos) {
            return end + 1;
        }

        return end - begin;
    }
};

class Solution {
public:
    int lengthOfLastWord(string s) {
        int len = 0;
        //因为i为size_t类型，所以当字符串只有一个单词时，i最后会减到-1（非常大的整数）即npos
        for (size_t i = s.size() - 1; i >= 0 && i != string::npos; --i) {
            if (s[i] == ' ') {
                if (len != 0) { //消除末尾的空格带来的影响，只有遇见非空格了才会让长度++
                    return len;
                }
            }
            else {
                ++len;
            }
        }

        return len; //当字符串只有一个单词时在这里返回
    }
};